<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */

global $consal_opt;

get_header();

if(isset($consal_opt)){
	$bloglayout = 'nosidebar';
} else {
	$bloglayout = 'sidebar';
}
if(isset($consal_opt['blog_layout']) && $consal_opt['blog_layout']!=''){
	$bloglayout = $consal_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$bloglayout = $_GET['layout'];
}
$blogsidebar = 'right';
if(isset($consal_opt['sidebarblog_pos']) && $consal_opt['sidebarblog_pos']!=''){
	$blogsidebar = $consal_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$blogsidebar = $_GET['sidebar'];
}
switch($bloglayout) {
	case 'sidebar':
		$blogclass = 'blog-sidebar';
		$blogcolclass = 9;
		$consal_postthumb = 'consal-category-thumb'; //750x510px
		break;
	case 'fullwidth':
		$blogclass = 'blog-fullwidth';
		$blogcolclass = 12;
		$blogsidebar = 'none';
		$consal_postthumb = 'consal-category-full'; //1144x510px
		break;
	default:
		$blogclass = 'blog-nosidebar';
		$blogcolclass = 12;
		$blogsidebar = 'none';
		$consal_postthumb = 'consal-post-thumb'; //500x500px
} consal_pages_breadcrumb(); ?>

	<section class="single_blog_bla">
		<div class="container">
			<div class="row">
				<?php if($blogsidebar=='left') : ?>
					<?php get_sidebar(); ?>
				<?php endif; ?>
				<div class="col-xs-12 <?php echo 'col-md-'.$blogcolclass; ?>">
					<div id="primary" class="content-area">
						<div id="main" class="site-main">
							<?php
							while ( have_posts() ) : the_post();

								get_template_part( 'template-parts/content-single', get_post_format() );

								// If comments are open or we have at least one comment, load up the comment template.
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;

							endwhile; // End of the loop.
							?>
						</div><!-- #main -->
					</div><!-- #primary -->
				</div>
				<!-- left sidebar -->
				<?php if( $blogsidebar=='right') : ?>
					<?php get_sidebar(); ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php
get_footer();
