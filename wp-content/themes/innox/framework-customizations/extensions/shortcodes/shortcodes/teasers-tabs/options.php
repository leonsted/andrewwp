<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$teaser = fw_ext( 'shortcodes' )->get_shortcode( 'teaser' );

$options = array(
	'tabs'       => array(
		'type'          => 'addable-popup',
		'label'         => esc_html__( 'Tabs', 'innox' ),
		'popup-title'   => esc_html__( 'Add/Edit Tabs', 'innox' ),
		'desc'          => esc_html__( 'Create your tabs', 'innox' ),
		'template'      => '{{=tab_title}}',
		'popup-options' => array(
			'tab_title'           => array(
				'type'  => 'text',
				'label' => esc_html__( 'Title', 'innox' )
			),
			'tab_columns_width'   => array(
				'type'    => 'select',
				'label'   => esc_html__( 'Column width in tab content', 'innox' ),
				'value'   => 'col-sm-4',
				'desc'    => esc_html__( 'Choose teaser width inside tab content', 'innox' ),
				'choices' => array(
					'col-sm-12' => esc_html__( '1/1', 'innox' ),
					'col-sm-6'  => esc_html__( '1/2', 'innox' ),
					'col-sm-4'  => esc_html__( '1/3', 'innox' ),
					'col-sm-3'  => esc_html__( '1/4', 'innox' ),
				),
			),
			'tab_columns_padding' => array(
				'type'    => 'select',
				'value'   => 'columns_padding_15',
				'label'   => esc_html__( 'Column paddings', 'innox' ),
				'desc'    => esc_html__( 'Choose columns horizontal paddings value', 'innox' ),
				'choices' => array(
					'columns_padding_0'  => esc_html__( '0', 'innox' ),
					'columns_padding_1'  => esc_html__( '1 px', 'innox' ),
					'columns_padding_2'  => esc_html__( '2 px', 'innox' ),
					'columns_padding_5'  => esc_html__( '5 px', 'innox' ),
					'columns_padding_15' => esc_html__( '15 px - default', 'innox' ),
					'columns_padding_25' => esc_html__( '25 px', 'innox' ),
				),
			),
			'tab_teasers'         => array(
				'type'          => 'addable-popup',
				'label'         => esc_html__( 'Teasers in tabs', 'innox' ),
				'popup-title'   => esc_html__( 'Add/Edit Teasers in tabs', 'innox' ),
				'desc'          => esc_html__( 'Create your teasers in tabs', 'innox' ),
				'template'      => '{{=title}}',
				'popup-options' => $teaser->get_options(),

			),
		),

	),
	'top_border' => array(
		'type'         => 'switch',
		'value'        => '',
		'label'        => esc_html__( 'Top color border', 'innox' ),
		'desc'         => esc_html__( 'Add top color border to tab content', 'innox' ),
		'left-choice'  => array(
			'value' => '',
			'label' => esc_html__( 'No top border', 'innox' ),
		),
		'right-choice' => array(
			'value' => 'top-color-border',
			'label' => esc_html__( 'Color top border', 'innox' ),
		),
	),
	'id'         => array( 'type' => 'unique' ),
);