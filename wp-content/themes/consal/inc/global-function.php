<?php

// Display header top bar
function consal_header_top_show(){
	global $consal_opt;
	return $consal_opt['show_header_top_bar'];
}

// Phone number
function consal_top_bar_phone(){
	global $consal_opt;
	return $consal_opt['top_bar_phone'];
}

// Email Address
function consal_top_bar_email(){
	global $consal_opt;
	return $consal_opt['top_bar_email'];
}

// Topbar Social link
function consal_social_icons(){
	global $consal_opt;
	if(isset($consal_opt['topbar_social_icons'])) {?>							
		<?php echo '<ul class="social-icons">';
			foreach($consal_opt['topbar_social_icons'] as $key=>$value ) {
				if($value!=''){
					if($key=='vimeo'){
						echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-vimeo"></i></a></li>';
					} else {
						echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-'.esc_attr($key).'"></i></a></li>';
					}
				}
			}
			echo '</ul>';
		?>							
	<?php };
}

// consal hearder top
function consal_header_top(){
	if( consal_header_top_show() == true) : ?>
		<!-- tiny header starts -->
		<div class="tiny_header">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="contact_email">
							<ul>
								<li><span>
									<?php global $consal_opt; 
										if( ! empty ($consal_opt['top_bar_phone'])) { ?>
											<i class="fa fa-phone"></i>
											<?php esc_html_e('Call:', 'consal'); 
										} 
									?>
									</span>
									<a href="tel:<?php echo esc_attr(consal_top_bar_phone()); ?>">
										<?php echo esc_html(consal_top_bar_phone()); ?>
									</a>
								</li>
								<li><span>
									<?php global $consal_opt; 
										if( ! empty ($consal_opt['top_bar_email'])) { ?>
											<i class="fa fa-envelope"></i>
											<?php esc_html_e('Email:', 'consal'); 
										} 
									?>
									</span>
									<a href="mailto:<?php echo esc_attr(consal_top_bar_email()); ?>">
										<?php echo esc_html(consal_top_bar_email()); ?>
									</a>
								</li>
							</ul>
						</div>
						<div class="social_icons">
							<?php echo consal_social_icons(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- tiny header ends -->
	<?php endif;
} 

// display logo
function consal_logo(){
	global $consal_opt;
	if(is_ssl()){
		$consal_opt['logo_main']['url'] = str_replace('http:', 'https:', $consal_opt['logo_main']['url']);
	}
	
	if( !empty($consal_opt['logo_main']['url']) ){ ?>
			<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<img class="svg" src="<?php echo esc_url($consal_opt['logo_main']['url']); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
			</a>
		<?php
		} else { ?>
			<h1 class="navbar-blog-info"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php
		} ?>
	<?php
}

// Main Menu
function consal_main_menu(){
	wp_nav_menu( array(
		'theme_location'    => 'menu-1',
		'container' 	=> '',
		'fallback_cb'   => 'consal_menu_cb',
		'items_wrap' 	=> '<ul class="nav navbar-nav">%3$s</ul>' 
	));
}

// Main Menu Callback 
function consal_menu_cb (){
	echo '<ul class="nav navbar-nav">';
	echo '<li><a href="' . admin_url( 'nav-menus.php' ) . '">'. esc_html('Add a menu','').'</a></li>';
	echo '</ul>';
}

// blog read more
function consal_blog_read_more(){
	global $consal_opt;
	if(isset($consal_opt)){ 
		echo esc_html($consal_opt['readmore_text']); 
	} 
	else { 
		esc_html_e('Read More ...', 'consal');
	}
}

// display pages breadcrumb
function consal_pages_breadcrumb(){
	global $consal_opt;
	$condition = true;
	
	if ( class_exists( 'ReduxFrameworkPlugin' ) ){
		$condition = $consal_opt['breadcrumb_on_off'];
	}
	
	if($condition){ ?>
	<!--================================
		2.START BREADCRUMB AREA CSS
	=================================-->
	<section class="breadcrumb pages_bdcmb">
	  <div class="container">
		<div class="row">
		  <div class="col-md-12">
			<div class="breadcrumb_title_wrapper">
			  <div class="page_title">
				<h1><?php the_title();?></h1>
			  </div>

			  <div class="bread_crumb">
				<?php if(function_exists('bcn_display')) {
					bcn_display();
				}?>
			  </div>

			</div>
		  </div>
		</div>
	  </div>
	</section>
	<!--================================
		2.END BREADCRUMB AREA CSS
	=================================-->
	<?php }
}

// display blog breadcrumb
function consal_blogs_breadcrumb(){
	global $consal_opt;
	$condition = true;
	if ( class_exists( 'ReduxFrameworkPlugin' ) ){
		$condition = $consal_opt['breadcrumb_on_off_blog'];
	}
	if($condition){ ?>
	<!--================================
		2.START BREADCRUMB AREA CSS
	=================================-->
	<section class="breadcrumb blog-bdcmb">
	  <div class="container">
		<div class="row">
		  <div class="col-md-12">
			<div class="breadcrumb_title_wrapper">
			  <div class="page_title">
				<h1><?php esc_html_e('Blog' , 'consal');?></h1>
			  </div>
			  <ul class="bread_crumb">
				<li><a href="<?php echo esc_url( home_url('/') ); ?>"><?php esc_html_e('Home' , 'consal');?></a></li>
				<li class="bread_active"><?php esc_html_e('Blog' , 'consal');?></li>
			  </ul>
			</div>
		  </div>
		</div>
	  </div>
	</section>
	<!--================================
		2.END BREADCRUMB AREA CSS
	=================================-->
	<?php }
}

// display single blog breadcrumb
function consal_single_blog_breadcrumb(){
	global $consal_opt;
	$condition = true;
	if ( class_exists( 'ReduxFrameworkPlugin' ) ){
		$condition = $consal_opt['breadcrumb_on_off_blog'];
	}
	if($condition){ ?>
	<!--================================
		2.START BREADCRUMB AREA CSS
	=================================-->
	<section class="breadcrumb blog-bdcmb">
	  <div class="container">
		<div class="row">
		  <div class="col-md-12">
			<div class="breadcrumb_title_wrapper">
			  <div class="page_title">
				<h1><?php esc_html_e('Blog' , 'consal');?></h1>
			  </div>
			  <ul class="bread_crumb">
				<li><a href="<?php echo esc_url( home_url('/') ); ?>"><?php esc_html_e('Home' , 'consal');?></a></li>
				<li class="bread_active"><?php esc_html_e('Blog' , 'consal');?></li>
			  </ul>
			</div>
		  </div>
		</div>
	  </div>
	</section>
	<!--================================
		2.END BREADCRUMB AREA CSS
	=================================-->
	<?php }
}

// display archive breadcrumb
function consal_archive_breadcrumb(){
	global $consal_opt;
	$condition = true;
	if ( class_exists( 'ReduxFrameworkPlugin' ) ){
		$condition = $consal_opt['breadcrumb_on_off_blog'];
	}
	if($condition){ ?>
	<!--================================
		2.START BREADCRUMB AREA CSS
	=================================-->
	<section class="breadcrumb blog-bdcmb">
	  <div class="container">
		<div class="row">
		  <div class="col-md-12">
			<div class="breadcrumb_title_wrapper">
			  <div class="page_title">
			    <?php the_archive_title( '<h1>', '</h1>' ); ?>
			  </div>
			  <ul class="bread_crumb">
				<li><a href="<?php echo esc_url( home_url('/') ); ?>"><?php esc_html_e('Home' , 'consal');?></a></li>
				<li class="bread_active"><?php the_archive_title(); ?></li>
			  </ul>
			</div>
		  </div>
		</div>
	  </div>
	</section>
	<!--================================
		2.END BREADCRUMB AREA CSS
	=================================-->
	<?php }
}

// display search breadcrumb
function consal_search_breadcrumb(){
	global $consal_opt;
	$condition = true;
	if ( class_exists( 'ReduxFrameworkPlugin' ) ){
		$condition = $consal_opt['breadcrumb_on_off_blog'];
	}
	if($condition){ ?>
	<!--================================
		2.START BREADCRUMB AREA CSS
	=================================-->
	<section class="breadcrumb blog-bdcmb">
	  <div class="container">
		<div class="row">
		  <div class="col-md-12">
			<div class="breadcrumb_title_wrapper">
			  <div class="page_title">
				<h1><?php printf( esc_html__( 'Search Results for: %s', 'consal' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			  </div>
			  <ul class="bread_crumb">
				<li><a href="<?php echo esc_url( home_url('/') ); ?>"><?php esc_html_e('Home' , 'consal');?></a></li>
				<li class="bread_active">
					<?php if ( have_posts() ) :
						printf( '<span>' . get_search_query() . '</span>' );
					else :
						esc_html_e( 'Nothing Found', 'consal' );
					endif; ?>
				</li>
			  </ul>
			</div>
		  </div>
		</div>
	  </div>
	</section>
	<!--================================
		2.END BREADCRUMB AREA CSS
	=================================-->
	<?php }
}

// display error page breadcrumb
function consal_error_page_breadcrumb(){
	global $consal_opt;
	$condition = true;
	if ( class_exists( 'ReduxFrameworkPlugin' ) ){
		$condition = $consal_opt['breadcrumb_on_off_error'];
	}
	if($condition){ ?>	
	<!--================================
		2.START BREADCRUMB AREA CSS
	=================================-->
	<section class="breadcrumb error-page">
	  <div class="container">
		<div class="row">
		  <div class="col-md-12">
			<div class="breadcrumb_title_wrapper">
			  <div class="page_title">
				<h1><?php wp_title(''); ?></h1>
			  </div>
			  <ul class="bread_crumb">
				<li><a href="<?php echo esc_url( home_url('/') ); ?>"><?php esc_html_e('Home' , 'consal');?></a></li>
				<li class="bread_active"><?php wp_title(''); ?></li>
			  </ul>
			</div>
		  </div>
		</div>
	  </div>
	</section>
	<!--================================
		2.END BREADCRUMB AREA CSS
	=================================-->
	<?php }
}

// Contact copyright text
function consal_copyright(){
	global $consal_opt;
	if( isset($consal_opt['copyright']) && $consal_opt['copyright']!='' ) {
		echo wp_kses($consal_opt['copyright'], array(
			'a' => array(
				'href' => array(),
				'title' => array()
			),
			'br' => array(),
			'em' => array(),
			'strong' => array(),
		));
	} else {
		echo esc_html__('Copyright &copy; 2017 ' , 'consal'). " <a href='".esc_url('coderspoint.net')."' target='_blank'>".esc_html__('Consal' , 'consal')."</a> ".esc_html__('All Rights Reserved by CodersPoint.' , 'consal');

	};
}


// 404 error page title
function consal_not_found_title() {
	global $consal_opt;
	if ($consal_opt['notfound_title']){
		echo esc_html( $consal_opt['notfound_title'] );
	}
	else {
		echo esc_html('Oops! The page doesn\'t seem to be exists.', 'consal');
	}
}

// 404 error page error massage
function consal_not_found_content() {
	global $consal_opt;
	if ($consal_opt['notfound_content']){
		echo esc_html( $consal_opt['notfound_content'] );
	}
}

// footer sidebar function
function consal_footer_sidebar(){
	if ( is_active_sidebar( 'footer-sidebar' ) ) : ?>
		<div class="footer_wrapper">
			<div class="container">
				<div class="row">
					<?php dynamic_sidebar( 'footer-sidebar' ); ?>
				</div>
			</div>
		</div>
	<?php endif;
}


//Add google map API
function consal_googlemap_api(){
	global $consal_opt;
	//Add google map API
	$google_map_js_url = 'https://maps.googleapis.com/maps/api/js?key=' . $consal_opt['map_apy_key'];
	return esc_url_raw($google_map_js_url);
}

//For enqueue google map script
function consal_googlemap_api_enqueue(){
	wp_enqueue_script( 'google-map', consal_googlemap_api(), array(), null );
}

// Google map 
function consal_googlemap (){ 
	global $consal_opt; 
	$map_desc = str_replace(array("\r\n", "\r", "\n"), "<br />", $consal_opt['map_desc']);
	$map_desc = addslashes($map_desc);
	ob_start(); 
?>
		<script type="text/javascript">
        var myCenter=new google.maps.LatLng(<?php if(isset($consal_opt['map_lat'])) { echo esc_js($consal_opt['map_lat']); } else { echo '40.76732'; } ?>, <?php if(isset($consal_opt['map_long'])) { echo esc_js($consal_opt['map_long']); } else { echo '-74.20487'; } ?>);
            function initialize()
            {
            var mapProp = {
              center:myCenter,
              zoom:<?php if(isset($consal_opt['map_zoom'])) { echo esc_js($consal_opt['map_zoom']); } else { echo 14; } ?>,
              scrollwheel:<?php if($consal_opt['map_scrollwheel'] == 0 ) { echo 'false'; } else { echo 'true'; } ?>,
              mapTypeId:google.maps.MapTypeId.ROADMAP,
                styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"<?php echo esc_js($consal_opt['map_color']);?>"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
              };

            var map=new google.maps.Map(document.getElementById("google_map"),mapProp);

            var marker=new google.maps.Marker({
              position:myCenter,
              animation:google.maps.Animation.BOUNCE,
              icon:'<?php if( isset($consal_opt['map_marker']['url']) && $consal_opt['map_marker']['url']!='') :
					echo esc_js($consal_opt['map_marker']['url']);
				else :
					echo get_template_directory_uri() . '/images/map-marker.png';
				endif; ?>'
            });
			var infowindow = new google.maps.InfoWindow({
				content: "<?php echo wp_kses($map_desc, array(
							'a' => array(
								'href' => array(),
								'title' => array()
							),
							'i' => array(
								'class' => array()
							),
							'img' => array(
								'src' => array(),
								'alt' => array()
							),
							'br' => array(),
							'em' => array(),
							'strong' => array(),
							'p' => array(),
						)); ?>"
			});
			infowindow.open(map,marker);

            marker.setMap(map);
            }
			var mapContainer = jQuery('#google_map');
			if(mapContainer.length){
				google.maps.event.addDomListener(window, 'load', initialize);
			}
	</script>
	<?php echo ob_get_clean();
}