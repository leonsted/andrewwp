<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */

get_header();

consal_pages_breadcrumb();

?> 
 
<div class="page-content default-page">
	<div id="primary" class="content-area">
		<div class="container">
			<div class="row">
				<div class="blog-grid-area col-xs-12 <?php echo 'col-md-'.$pagecolclass; ?>">
					<div id="main" class="site-main">
						<?php while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/content', 'page' );
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						endwhile; // End of the loop.
						?>			
					</div>	
				</div>
			</div>
		</div>
	</div><!-- #main -->
</div><!-- #primary -->	
<?php
get_footer();
