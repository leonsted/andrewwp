<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
//get social icons to add in item:
$icon = fw_ext( 'shortcodes' )->get_shortcode( 'icon' );
//get social icons to add in item:
$icons_social = fw_ext( 'shortcodes' )->get_shortcode( 'icons_social' );

$options = array(
	'title'         => array(
		'type'  => 'text',
		'label' => esc_html__( 'Title of the Box', 'innox' ),
	),
	'title_tag'     => array(
		'type'    => 'select',
		'value'   => 'h3',
		'label'   => esc_html__( 'Title Tag', 'innox' ),
		'choices' => array(
			'h2' => esc_html__( 'H2', 'innox' ),
			'h3' => esc_html__( 'H3', 'innox' ),
			'h4' => esc_html__( 'H4', 'innox' ),
		)
	),
	'content'       => array(
		'type'          => 'wp-editor',
		'label'         => esc_html__( 'Item text', 'innox' ),
		'desc'          => esc_html__( 'Enter desired item content', 'innox' ),
		'size'          => 'small', // small, large
		'editor_height' => 400,
	),
	'item_style'    => array(
		'type'    => 'select',
		'label'   => esc_html__( 'Item Box Style', 'innox' ),
		'choices' => array(
			''                                => esc_html__( 'Default (no border or background)', 'innox' ),
			'content-padding with_border'     => esc_html__( 'Bordered', 'innox' ),
			'content-padding with_background' => esc_html__( 'Muted Background', 'innox' ),
			'content-padding with_border with_background' => esc_html__( 'Bordered Muted Background', 'innox' ),
			'content-padding ls ms'           => esc_html__( 'Grey background', 'innox' ),
			'content-padding ds'              => esc_html__( 'Darkgrey background', 'innox' ),
			'content-padding ds ms'           => esc_html__( 'Dark background', 'innox' ),
			'content-padding cs'              => esc_html__( 'Main color background', 'innox' ),
			'content-padding big-padding with_border'        => esc_html__( 'Bordered with Padding', 'innox' ),
			'content-padding big-padding with_background'    => esc_html__( 'Muted Background with Padding', 'innox' ),
			'content-padding big-padding with_border with_background'    => esc_html__( 'Bordered Muted Background with Padding', 'innox' ),
			'content-padding big-padding ls ms'              => esc_html__( 'Grey background with Padding', 'innox' ),
			'content-padding big-padding ds'                 => esc_html__( 'Darkgrey background with Padding', 'innox' ),
			'content-padding big-padding ds ms'              => esc_html__( 'Dark background with Padding', 'innox' ),
			'content-padding big-padding cs'                 => esc_html__( 'Main color background with Padding', 'innox' ),
		)
	),
	'link'          => array(
		'type'  => 'text',
		'value' => '',
		'label' => esc_html__( 'Item link', 'innox' ),
		'desc'  => esc_html__( 'Link on title and optional button', 'innox' ),
	),
	'item_image'    => array(
		'type'        => 'upload',
		'value'       => '',
		'label'       => esc_html__( 'Item Image', 'innox' ),
		'image'       => esc_html__( 'Image for your item. Not all item layouts show image', 'innox' ),
		'help'        => esc_html__( 'Image for your item. Image can appear as an element, or background or even can be hidden depends from chosen item type', 'innox' ),
		'images_only' => true,
	),
	'image_right'   => array(
		'type'         => 'switch',
		'label'        => esc_html__( 'Image to the right', 'innox' ),
		'left-choice'  => array(
			'value' => '',
			'label' => esc_html__( 'No', 'innox' ),
		),
		'right-choice' => array(
			'value' => 'true',
			'label' => esc_html__( 'Yes', 'innox' ),
		),
	),
	'responsive_lg' => array(
		'label'   => esc_html__( 'Image width on wide screens', 'innox' ),
		'desc'    => __( 'Select image column width on wide screens (>1200px)', 'innox' ),
		'value'   => '6',
		'type'    => 'select',
		'choices' => array(
			'12' => esc_html__( 'Full Width', 'innox' ),
			'6'  => esc_html__( '1/2', 'innox' ),
			'4'  => esc_html__( '1/3', 'innox' ),
			'3'  => esc_html__( '1/4', 'innox' ),
		)
	),
	'responsive_md' => array(
		'label'   => esc_html__( 'Image width on middle screens', 'innox' ),
		'desc'    => __( 'Select image column width on middle screens (>992px)', 'innox' ),
		'value'   => '4',
		'type'    => 'select',
		'choices' => array(
			'12' => esc_html__( 'Full Width', 'innox' ),
			'6'  => esc_html__( '1/2', 'innox' ),
			'4'  => esc_html__( '1/3', 'innox' ),
			'3'  => esc_html__( '1/4', 'innox' ),
		)
	),
	'responsive_sm' => array(
		'label'   => esc_html__( 'Image width on small screens', 'innox' ),
		'desc'    => __( 'Select image column width on small screens (>768px)', 'innox' ),
		'value'   => '2',
		'type'    => 'select',
		'choices' => array(
			'12' => esc_html__( 'Full Width', 'innox' ),
			'6'  => esc_html__( '1/2', 'innox' ),
			'4'  => esc_html__( '1/3', 'innox' ),
			'3'  => esc_html__( '1/4', 'innox' ),
		)
	),
	'responsive_xs' => array(
		'label'   => esc_html__( 'Image width on extra small screens', 'innox' ),
		'desc'    => esc_html__( 'Select image column width on extra small screens (<767px)', 'innox' ),
		'value'   => '1',
		'type'    => 'select',
		'choices' => array(
			'12' => esc_html__( 'Full Width', 'innox' ),
			'6'  => esc_html__( '1/2', 'innox' ),
			'4'  => esc_html__( '1/3', 'innox' ),
			'3'  => esc_html__( '1/4', 'innox' ),
		)
	),
	'icons'         => array(
		'type'            => 'addable-box',
		'value'           => '',
		'label'           => esc_html__( 'Additional info', 'innox' ),
		'desc'            => esc_html__( 'Add icons with title and text', 'innox' ),
		'box-options'     => $icon->get_options(),
		'add-button-text' => esc_html__( 'Add New', 'innox' ),
		'template'        => '{{=title}}',
		'sortable'        => true,
	),
	$icons_social->get_options(),

);