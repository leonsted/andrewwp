<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$portfolio = fw()->extensions->get( 'portfolio' );
if ( empty( $portfolio ) ) {
	return;
}

$options = array(
	'layout_variant' => array(
		'type'    => 'multi-picker',
		'label'   => false,
		'desc'    => false,
		'value'   => false,
		'picker'  => array(
			'layout'        => array(
				'label'   => esc_html__( 'Portfolio Layout', 'innox' ),
				'desc'    => esc_html__( 'Choose projects layout', 'innox' ),
				'value'   => 'isotope',
				'type'    => 'select',
				'choices' => array(
					'carousel' => esc_html__( 'Carousel', 'innox' ),
					'isotope'  => esc_html__( 'Masonry Grid', 'innox' ),
				)
			),
		),
		'choices' => array(
			'carousel'   => array(
				'loop' => array(
					'label'     => esc_html__( 'Loop Carousel', 'innox' ),
					'value'     => '',
					'type'      => 'switch',
				),
				'center' => array(
					'label'     => esc_html__( 'Center Carousel', 'innox' ),
					'value'     => '',
					'type'      => 'switch',
				),
				'dots' => array(
					'label'     => esc_html__( 'Show Dots', 'innox' ),
					'value'     => '',
					'type'      => 'switch',
				),
				'nav' => array(
					'label'     => esc_html__( 'Show Nav', 'innox' ),
					'value'     => '',
					'type'      => 'switch',
				),
				'autoplay' => array(
					'label'     => esc_html__( 'Auto Play Carousel', 'innox' ),
					'value'     => '',
					'type'      => 'switch',
				),
			),
			'isotope'   => array(),
		),
	),


	'item_layout'   => array(
		'label'   => esc_html__( 'Item layout', 'innox' ),
		'desc'    => esc_html__( 'Choose Item layout', 'innox' ),
		'value'   => 'item-regular',
		'type'    => 'select',
		'choices' => array(
			'item-regular'  => esc_html__( 'Image with hovering title', 'innox' ),
			'item-square'  => esc_html__( 'Square Image', 'innox' ),
			'item-title'    => esc_html__( 'Image with title', 'innox' ),
			'item-extended' => esc_html__( 'Image with title and excerpt', 'innox' ),
		)
	),
	'number'        => array(
		'type'       => 'text',
		'value'      => '6',
		'label'      => esc_html__( 'Items number', 'innox' ),
		'desc'       => esc_html__( 'Number of portfolio projects tu display', 'innox' ),
	),
	'margin'        => array(
		'label'   => esc_html__( 'Horizontal item margin (px)', 'innox' ),
		'desc'    => esc_html__( 'Select horizontal item margin', 'innox' ),
		'value'   => '30',
		'type'    => 'select',
		'choices' => array(
			'0'  => esc_html__( '0', 'innox' ),
			'1'  => esc_html__( '1px', 'innox' ),
			'2'  => esc_html__( '2px', 'innox' ),
			'10' => esc_html__( '10px', 'innox' ),
			'30' => esc_html__( '30px', 'innox' ),
		)
	),
	'responsive_lg' => array(
		'label'   => esc_html__( 'Columns on wide screens', 'innox' ),
		'desc'    => esc_html__( 'Select items number on wide screens (>=1200px)', 'innox' ),
		'value'   => '4',
		'type'    => 'select',
		'choices' => array(
			'1' => esc_html__( '1', 'innox' ),
			'2' => esc_html__( '2', 'innox' ),
			'3' => esc_html__( '3', 'innox' ),
			'4' => esc_html__( '4', 'innox' ),
			'5' => esc_html__( '5', 'innox' ),
			'6' => esc_html__( '6', 'innox' ),
		)
	),
	'responsive_md' => array(
		'label'   => esc_html__( 'Columns on middle screens', 'innox' ),
		'desc'    => esc_html__( 'Select items number on middle screens (>=992px)', 'innox' ),
		'value'   => '3',
		'type'    => 'select',
		'choices' => array(
			'1' => esc_html__( '1', 'innox' ),
			'2' => esc_html__( '2', 'innox' ),
			'3' => esc_html__( '3', 'innox' ),
			'4' => esc_html__( '4', 'innox' ),
			'5' => esc_html__( '5', 'innox' ),
			'6' => esc_html__( '6', 'innox' ),
		)
	),
	'responsive_sm' => array(
		'label'   => esc_html__( 'Columns on small screens', 'innox' ),
		'desc'    => esc_html__( 'Select items number on small screens (>=768px)', 'innox' ),
		'value'   => '2',
		'type'    => 'select',
		'choices' => array(
			'1' => esc_html__( '1', 'innox' ),
			'2' => esc_html__( '2', 'innox' ),
			'3' => esc_html__( '3', 'innox' ),
			'4' => esc_html__( '4', 'innox' ),
			'5' => esc_html__( '5', 'innox' ),
		)
	),
	'responsive_xs' => array(
		'label'   => esc_html__( 'Columns on extra small screens', 'innox' ),
		'desc'    => esc_html__( 'Select items number on extra small screens (>=500px)', 'innox' ),
		'value'   => '1',
		'type'    => 'select',
		'choices' => array(
			'1' => esc_html__( '1', 'innox' ),
			'2' => esc_html__( '2', 'innox' ),
			'3' => esc_html__( '3', 'innox' ),
			'4' => esc_html__( '4', 'innox' ),
		)
	),
	'responsive_xxs' => array(
		'label'   => esc_html__( 'Columns on extra small screens', 'innox' ),
		'desc'    => esc_html__( 'Select items number on extra small screens (<500px)', 'innox' ),
		'value'   => '1',
		'type'    => 'select',
		'choices' => array(
			'1' => esc_html__( '1', 'innox' ),
			'2' => esc_html__( '2', 'innox' ),
			'3' => esc_html__( '3', 'innox' ),
			'4' => esc_html__( '4', 'innox' ),
		)
	),
	'show_filters'  => array(
		'type'         => 'switch',
		'value'        => '',
		'label'        => esc_html__( 'Show filters', 'innox' ),
		'desc'         => esc_html__( 'Hide or show categories filters', 'innox' ),
		'left-choice'  => array(
			'value' => '',
			'label' => esc_html__( 'No', 'innox' ),
		),
		'right-choice' => array(
			'value' => 'true',
			'label' => esc_html__( 'Yes', 'innox' ),
		),
	),
);