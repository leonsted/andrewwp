<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'image'            => array(
		'type'  => 'upload',
		'label' => esc_html__( 'Choose Image', 'innox' ),
		'desc'  => esc_html__( 'Either upload a new, or choose an existing image from your media library', 'innox' )
	),
	'size'             => array(
		'type'    => 'group',
		'options' => array(
			'width'  => array(
				'type'  => 'text',
				'label' => esc_html__( 'Width', 'innox' ),
				'desc'  => esc_html__( 'Set image width', 'innox' ),
				'value' => 300
			),
			'height' => array(
				'type'  => 'text',
				'label' => esc_html__( 'Height', 'innox' ),
				'desc'  => esc_html__( 'Set image height', 'innox' ),
				'value' => 200
			),
			'frame' => array(
				'type'         => 'switch',
				'label'        => esc_html__( 'With Frame', 'innox' ),
				'desc'         => esc_html__( 'Put image in offset frame', 'innox' )
			),
		)
	),
	'image-link-group' => array(
		'type'    => 'group',
		'options' => array(
			'link'   => array(
				'type'  => 'text',
				'label' => esc_html__( 'Image Link', 'innox' ),
				'desc'  => esc_html__( 'Where should your image link to?', 'innox' )
			),
			'target' => array(
				'type'         => 'switch',
				'label'        => esc_html__( 'Open Link in New Window', 'innox' ),
				'desc'         => esc_html__( 'Select here if you want to open the linked page in a new window', 'innox' ),
				'right-choice' => array(
					'value' => '_blank',
					'label' => esc_html__( 'Yes', 'innox' ),
				),
				'left-choice'  => array(
					'value' => '_self',
					'label' => esc_html__( 'No', 'innox' ),
				),
			),
		)
	)
);

