<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'slider_background' => array(
		'type'        => 'select',
		'value'       => 'ls',
		'label'       => esc_html__( 'Slider background', 'innox' ),
		'desc'        => esc_html__( 'Select slider background color', 'innox' ),
		'choices'     => array(
			'ls'    => esc_html__( 'Light', 'innox' ),
			'ls ms' => esc_html__( 'Light Muted', 'innox' ),
			'ds'    => esc_html__( 'Dark', 'innox' ),
			'ds ms' => esc_html__( 'Dark Muted', 'innox' ),
			'cs'    => esc_html__( 'Accent Color 1', 'innox' ),
			'cs gradient lighten_gradient'    => esc_html__( 'Accent Color 1 With White Lightening', 'innox' ),
			'cs main_color2'    => esc_html__( 'Accent Color 2', 'innox' ),
			'cs main_color2 gradient lighten_gradient'    => esc_html__( 'Accent Color 2 With White Lightening', 'innox' ),
			'cs gradient'    => esc_html__( 'Accent Colors Gradient', 'innox' ),
		),
		/**
		 * Allow save not existing choices
		 * Useful when you use the select to populate it dynamically from js
		 */
		'no-validate' => false,
	),

	'slider_responsive_layout' => array(
		'type'    => 'multi-picker',
		'label'   => false,
		'desc'    => false,
		'value'   => false,
		'picker'  => array(
			'all_scr_cover' => array(
				'type'      => 'switch',
				'label'     => esc_html__( 'Image overlay on small screens', 'innox' ),
				'desc'      => esc_html__( 'Leave image overlay on small screens same as it is on large resolution', 'innox' ),
				'value'     => 'all-scr-cover',
				'left-choice'   => array(
					'value' => 'all-scr-cover',
					'label' => esc_html__( 'Yes', 'innox' )
				),
				'right-choice'   => array(
					'value' => '',
					'label' => esc_html__( 'No', 'innox' )
				)
			),
		),
		'choices' => array(
			'all-scr-cover'   => array(
				'slider_height' => array(
					'label'     => esc_html__( 'Slider Height', 'innox' ),
					'desc'      => esc_html__( 'Choose how to determine slider height, by content or by image', 'innox' ),
					'value'     => '',
					'type'      => 'switch',
					'left-choice'   => array(
						'value' => '',
						'label' => esc_html__( 'Content', 'innox' )
					),
					'right-choice'   => array(
						'value' => 'image-dependant',
						'label' => esc_html__( 'Image', 'innox' )
					)
				),
			),
			''   => array(),
		),
	),
	'slider_custom_class' => array(
		'label' => esc_html__('Custom Class', 'innox'),
		'desc'  => esc_html__( 'Slider section custom class', 'innox' ),
		'type' => 'text',
	),
);
