<?php
/**
 * The template part for selected title (breadcrubms) section
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$background_image = function_exists( 'fw_get_db_customizer_option' ) ? fw_get_db_customizer_option( 'breadcrumbs_background_image' ) : '';
$background_image_url = isset( $background_image['data']['icon'] ) ? 'url(' . esc_url( $background_image['data']['icon'] ) . ')' : '';
?>
<section class="page_breadcrumbs cs breadcrumbs1 background_cover section_padding_50 columns_padding_5" <?php echo !empty( $background_image_url ) ? 'style="background-image: ' . $background_image_url . '"' : ''; ?>>
	<div class="container">
		<div class="row">
            <span class="section_bg_header">
                <?php
                get_template_part( 'template-parts/breadcrumbs/page-background-title-text' );
                ?>
            </span>
            <div class="col-xs-12 text-center greylinks grey">

                    <?php if ( function_exists( 'fw_ext_breadcrumbs' ) ) : ?>
                    <h1 class="sr-only">
                    <?php else: ?>
                    <h1 class="small">
                    <?php endif; ?>
                        <?php
                        get_template_part( 'template-parts/breadcrumbs/page-title-text' );
                        ?>
                    </h1>

                    <?php
                    if ( function_exists( 'woocommerce_breadcrumb123' ) ) {
                        woocommerce_breadcrumb( array(
                            'delimiter'   => '',
                            'wrap_before' => '<nav class="woocommerce-breadcrumb" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '><ol class="breadcrumb big-spacing darklinks">',
                            'wrap_after'  => '</ol></nav>',
                            'before'      => '<li>',
                            'after'       => '</li>',
                            'home'        => esc_html_x( 'Home', 'breadcrumb', 'innox' )
                        ) );
                    } elseif ( function_exists( 'fw_ext_breadcrumbs' ) ) {
                        fw_ext_breadcrumbs();
                    }
	            ?>
            </div>
		</div>
	</div>
</section>