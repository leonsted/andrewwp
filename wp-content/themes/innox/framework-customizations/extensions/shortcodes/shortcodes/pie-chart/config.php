<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

//$cfg = array();

$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Pie Chart', 'innox' ),
	'description' => esc_html__( 'Add a Pie Chart', 'innox' ),
	'tab'         => esc_html__( 'Content Elements', 'innox' )
);