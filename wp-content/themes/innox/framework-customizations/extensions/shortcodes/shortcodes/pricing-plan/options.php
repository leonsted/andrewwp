<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

//find fw_ext
$shortcodes_extension = fw()->extensions->get( 'shortcodes' );
$button_options = array();
if ( ! empty( $shortcodes_extension ) ) {
	$button_options = $shortcodes_extension->get_shortcode( 'button' )->get_options();
}

$options = array(
	'tab_main' => array(
		'type' => 'tab',
		'options' => array(
			'title'   => array(
				'type'  => 'text',
				'value' => '',
				'label' => esc_html__( 'Pricing plan title', 'innox' ),
			),
			'currency'   => array(
				'type'  => 'text',
				'value' => '',
				'label' => esc_html__( 'Currency Sign', 'innox' ),
			),
			'price'   => array(
				'type'  => 'text',
				'value' => '',
				'label' => esc_html__( 'Integer Price', 'innox' ),
			),
			'price_decimal'   => array(
				'type'  => 'text',
				'value' => '',
				'label' => esc_html__( 'Decimal Price', 'innox' ),
			),
			'accent_color' => array(
				'type'        => 'select',
				'value'       => '',
				'label'       => esc_html__( 'Accent Color', 'innox' ),
				'choices'     => array(
					'' => esc_html__( 'Accent Color 1', 'innox' ),
					'2' => esc_html__( 'Accent Color 2', 'innox' ),
					'3' => esc_html__( 'Accent Color 3', 'innox' ),
					'4' => esc_html__( 'Accent Color 4', 'innox' ),
				),
				'no-validate' => false,
			),
			'description'   => array(
				'type'  => 'text',
				'value' => '',
				'label' => esc_html__( 'Pricing description', 'innox' ),
			),
			'features'         => array(
				'type'            => 'addable-box',
				'value'           => '',
				'label'           => esc_html__( 'Pricing plan features', 'innox' ),
				'box-options'     => array(
					'feature_name'   => array(
						'type'  => 'text',
						'value' => '',
						'label' => esc_html__( 'Feature name', 'innox' ),
					),
					'feature_checked' => array(
						'type'        => 'select',
						'value'       => '',
						'label'       => esc_html__( 'Default, checked or unchecked', 'innox' ),
						'choices'     => array(
							'default' => esc_html__( 'Default', 'innox' ),
							'enabled' => esc_html__( 'Enabled', 'innox' ),
							'disabled' => esc_html__( 'Disabled', 'innox' ),
						),
						'no-validate' => false,
					),
				),
				'template'        => '{{=feature_name}}',
				'limit'           => 0, // limit the number of boxes that can be added
				'add-button-text' => esc_html__( 'Add', 'innox' ),
				'sortable'        => true,
			),
		),
		'title' => esc_html__('Info', 'innox'),
	),

	'tab_button' => array(
		'type' => 'tab',
		'options' => array(
			'price_buttons'     => array(
				'type'        => 'addable-box',
				'value'       => '',
				'label'       => esc_html__( 'Price Buttons', 'innox' ),
				'desc'        => esc_html__( 'Choose a button, link for it and text inside it', 'innox' ),
				'template'    => 'Button',
				'box-options' => array(
					$button_options
				),
				'limit'           => 5, // limit the number of boxes that can be added
				'add-button-text' => esc_html__( 'Add', 'innox' ),
			),
		),
		'title' => esc_html__('Button', 'innox'),
	),



);