<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

//$cfg = array();

$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Person Box', 'innox' ),
	'description' => esc_html__( 'Add a Person image with name and position', 'innox' ),
	'tab'         => esc_html__( 'Content Elements', 'innox' )
);