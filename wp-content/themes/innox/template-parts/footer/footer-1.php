<?php
/**
 * The template part for selected footer
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<footer class="page_footer ds dark section_padding_top_150 section_padding_bottom_140 columns_margin_bottom_40 columns_padding_30">
	<div class="container">

		<div class="row">
			<?php dynamic_sidebar( 'sidebar-footer' ); ?>
		</div>

	</div>
</footer><!-- .page_footer -->