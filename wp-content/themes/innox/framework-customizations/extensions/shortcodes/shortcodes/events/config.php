<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}


$events = fw()->extensions->get( 'events' );
if ( empty( $events ) ) {
	return;
}


$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Events', 'innox' ),
		'description' => esc_html__( 'Events in Tile view', 'innox' ),
		'tab'         => esc_html__( 'Widgets', 'innox' )
	)
);