<?php
/**
 * Template Name: Testimonial Page Template
 * The template for displaying all portfolio.
 *
 * This is the template that displays all Testimonial by default.
 *
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */

get_header();
global $consal_opt;

if(isset($consal_opt)){
	$pagelayout = 'nosidebar';
} else {
	$pagelayout = 'sidebar';
}
if(isset($consal_opt['page_layout']) && $consal_opt['page_layout']!=''){
	$pagelayout = $consal_opt['page_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$pagelayout = $_GET['layout'];
}
$pagesidebar = 'right';
if(isset($consal_opt['sidebarsepage_pos']) && $consal_opt['sidebarsepage_pos']!=''){
	$pagesidebar = $consal_opt['sidebarsepage_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$pagesidebar = $_GET['sidebar'];
}
switch($pagelayout) {
	case 'sidebar':
		$pageclass = 'page-sidebar';
		$pagecolclass = 9;
		break;
	case 'fullwidth':
		$pageclass = 'page-fullwidth';
		$pagecolclass = 12;
		$pagesidebar = 'none';
		break;
	default:
		$pageclass = 'page-nosidebar';
		$pagecolclass = 12;
		$pagesidebar = 'none';
} consal_pages_breadcrumb(); ?>
    <!--================================
        3.START TESTIMONIAL AREA
    =================================-->
    <section class="testimonials">
      <div class="container">
        <div class="row">
			<?php if($pagesidebar=='left') : ?>
				<?php get_sidebar('page'); ?>
			<?php endif; ?>	
			<div class="blog-grid-area section_padding col-xs-12 <?php echo 'col-md-'.$pagecolclass; ?>">
			<?php global $consal_opt;
				$post_par_page = $consal_opt['testimonialnumber']; 
				// the query to set the posts per page to 3
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array('posts_per_page' => $post_par_page, 'post_type' => 'consal_testimonial', 'paged' => $paged );
				query_posts($args);
				// the loop
				if ( have_posts() ) : while (have_posts()) : the_post();
				$testi_dsgntin = get_post_meta(get_the_id(),'consal_test_designation',true); ?>
				<!-- SINGLE PORTFOLIO -->
				<div class="col-md-6 col-sm-6">
					<div class="single_testimonial">
						<div class="testmonial_img">
							<?php the_post_thumbnail('consal-testimonial-thumb');?>
						</div>
						<div class="testimonial_contents">
							<div class="name_desig">
								<p class="name"><?php the_title(); ?></p>
								<span class="desig"><?php echo esc_attr( $testi_dsgntin ); ?></span>
							</div>
							<div class="testimonial_text">
								<p><?php the_content(); ?></p>
							</div>
						</div>
					</div>
				</div>
				<?php endwhile; ?>
				<!-- pagination -->
				<div class="pagination_area">				
					<?php the_posts_pagination( array(
					'type'      => 'list',
					'prev_text' => __( '<i class="fa fa-angle-left"></i>', 'consal' ),
					'next_text' => __( '<i class="fa fa-angle-right"></i>', 'consal' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Page', 'consal' ) . ' </span>',
					) ); ?>
					
				</div>	
				<?php else :
					get_template_part( 'template-parts/content', 'none' );
				endif; ?>
				<div class="partner section_padding_test padding_bottom">
					<?php if(($consal_opt['enable_partner_slider'])== 1 && function_exists ('consal_partner_shortcode') ){
						echo do_shortcode('[ourpartner]');
					} ?>
				</div>
			</div>
			<?php if( $pagesidebar=='right') : ?>
				<?php get_sidebar('page'); ?>
			<?php endif; ?>
		</div>
      </div>
    </section>
    <!--================================
        3.END TESTIMONIAL AREA
    =================================-->
<?php
get_footer();
