<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'social_icons' => array(
		'type'            => 'addable-box',
		'value'           => '',
		'label'           => esc_html__( 'Social Buttons', 'innox' ),
		'desc'            => esc_html__( 'Optional social buttons', 'innox' ),
		'template'        => '{{=icon}}',
		'box-options'     => array(
			'icon'       => array(
				'type'  => 'icon',
				'label' => esc_html__( 'Social Icon', 'innox' ),
				'set'   => 'social-icons',
			),
			'icon_text'       => array(
				'type'  => 'text',
				'label' => esc_html__( 'Social Icon Text', 'innox' ),
				'desc'  => esc_html__( 'This text displayed instead of icon for "Text Icon"', 'innox' )
			),
			'icon_class' => array(
				'type'        => 'select',
				'value'       => '',
				'label'       => esc_html__( 'Icon type', 'innox' ),
				'desc'        => esc_html__( 'Select one of predefined social button types', 'innox' ),
				'choices'     => array(
					''                                    => esc_html__( 'Default', 'innox' ),
					'text-icon'                           => esc_html__( 'Text Icon', 'innox' ),
					'dark-icon'                           => esc_html__( 'Dark Icon', 'innox' ),
					'border-icon'                         => esc_html__( 'Simple Bordered Icon', 'innox' ),
					'border-icon rounded-icon'            => esc_html__( 'Rounded Bordered Icon', 'innox' ),
					'bg-icon'                             => esc_html__( 'Simple Background Icon', 'innox' ),
					'bg-icon rounded-icon'                => esc_html__( 'Rounded Background Icon', 'innox' ),
					'color-icon bg-icon'                  => esc_html__( 'Color Light Background Icon', 'innox' ),
					'color-icon bg-icon rounded-icon'     => esc_html__( 'Color Light Background Rounded Icon', 'innox' ),
					'color-icon'                          => esc_html__( 'Color Icon', 'innox' ),
					'color-icon border-icon'              => esc_html__( 'Color Bordered Icon', 'innox' ),
					'color-icon border-icon rounded-icon' => esc_html__( 'Rounded Color Bordered Icon', 'innox' ),
					'color-bg-icon'                       => esc_html__( 'Color Background Icon', 'innox' ),
					'color-bg-icon rounded-icon'          => esc_html__( 'Rounded Color Background Icon', 'innox' ),

				),
				/**
				 * Allow save not existing choices
				 * Useful when you use the select to populate it dynamically from js
				 */
				'no-validate' => false,
			),
			'icon_url'   => array(
				'type'  => 'text',
				'value' => '',
				'label' => esc_html__( 'Icon Link', 'innox' ),
				'desc'  => esc_html__( 'Provide a URL to your icon', 'innox' ),
			)
		),
		'limit'           => 0, // limit the number of boxes that can be added
		'add-button-text' => esc_html__( 'Add', 'innox' ),
		'sortable'        => true,
	),
	'icons_wrapper_class' => array(
		'type'      => 'text',
		'label'     => esc_html__( 'Icons Container Custom Class', 'innox' )
	)
);