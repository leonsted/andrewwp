<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

//get button to add in a teaser:
$button         = fw_ext( 'shortcodes' )->get_shortcode( 'button' );
$button_options = $button->get_options();
unset( $button_options['link'] );
$button_array = array(
	'button' => array(
		'type'    => 'multi-picker',
		'label'   => false,
		'desc'    => false,
		'value'   => false,
		'picker'  => array(
			'show_button' => array(
				'type'         => 'switch',
				'label'        => esc_html__( 'Show teaser button', 'innox' ),
				'left-choice'  => array(
					'value' => '',
					'label' => esc_html__( 'No', 'innox' ),
				),
				'right-choice' => array(
					'value' => 'button',
					'label' => esc_html__( 'Yes', 'innox' ),
				),
			),
		),
		'choices' => array(
			''       => array(),
			'button' => $button_options,
		),
	)
);

$teaser_center_array = array(
	'teaser_offset_icon' => array(
		'type'         => 'switch',
		'value'        => '',
		'label'        => esc_html__( 'Top offset icon', 'innox' ),
		'left-choice'  => array(
			'value' => '',
			'label' => esc_html__( 'No', 'innox' ),
		),
		'right-choice' => array(
			'value' => 'text-center',
			'label' => esc_html__( 'Yes', 'innox' ),
		),
	),
	'teaser_center' => array(
		'type'         => 'switch',
		'value'        => '',
		'label'        => esc_html__( 'Center teaser contents', 'innox' ),
		'left-choice'  => array(
			'value' => '',
			'label' => esc_html__( 'No', 'innox' ),
		),
		'right-choice' => array(
			'value' => 'text-center',
			'label' => esc_html__( 'Yes', 'innox' ),
		),
	),
);

$icon_options = array(
	'type'    => 'group',
	'options' => array(
		'icon'       => array(
			'type'  => 'icon-v2',
			'label' => esc_html__( 'Choose an Icon', 'innox' ),
		),
		'icon_size'  => array(
			'type'    => 'select',
			'value'   => 'size_normal',
			'label'   => esc_html__( 'Icon Size', 'innox' ),
			'choices' => array(
				'size_small'  => esc_html__( 'Small', 'innox' ),
				'size_normal' => esc_html__( 'Normal', 'innox' ),
				'size_big'    => esc_html__( 'Big', 'innox' ),
			)
		),
		'icon_style' => array(
			'type'    => 'image-picker',
			'value'   => '',
			'label'   => esc_html__( 'Icon Style', 'innox' ),
			'desc'    => esc_html__( 'Select one of predefined icon styles. If not set - no icon will appear.', 'innox' ),
			'help'    => esc_html__( 'If not set - no icon will appear.', 'innox' ),
			'choices' => array(
				''                            => fw_get_template_customizations_directory_uri() . '/extensions/shortcodes/shortcodes/teaser/static/img/icon_teaser_00.png',
				'default_icon'                => fw_get_template_customizations_directory_uri() . '/extensions/shortcodes/shortcodes/teaser/static/img/icon_teaser_01.png',
				'border_icon'                 => fw_get_template_customizations_directory_uri() . '/extensions/shortcodes/shortcodes/teaser/static/img/icon_teaser_02.png',
				'background_icon'               => fw_get_template_customizations_directory_uri() . '/extensions/shortcodes/shortcodes/teaser/static/img/icon_teaser_03.png',
				'border_icon round'           => fw_get_template_customizations_directory_uri() . '/extensions/shortcodes/shortcodes/teaser/static/img/icon_teaser_04.png',
				'background_icon round'         => fw_get_template_customizations_directory_uri() . '/extensions/shortcodes/shortcodes/teaser/static/img/icon_teaser_05.png',
				'round light_bg_color thick_border_icon' => fw_get_template_customizations_directory_uri() . '/extensions/shortcodes/shortcodes/teaser/static/img/icon_teaser_06.png',
			),
			'blank' => false, // (optional) if true, images can be deselected
		),
	),
);

$image_options = array(
	'type'        => 'upload',
	'value'       => '',
	'label'       => esc_html__( 'Teaser Image', 'innox' ),
	'image'       => esc_html__( 'Image for your teaser.', 'innox' ),
	'help'        => esc_html__( 'Image for your teaser. Image can appear as an element, or background or even can be hidden depends from chosen teaser type', 'innox' ),
	'images_only' => true,
);

$option_teaser_icon = array(
	'icon_options' => $icon_options,
);

$option_teaser_image = array(
	'teaser_image' => $image_options,
);

$option_teaser_square = array(
	'teaser_image' => $image_options,
	'teaser_banner' => array(
		'type'  => 'switch',
		'label' => esc_html__( 'Teaser Banner', 'innox' ),
		'desc' => esc_html__( 'Put whole teaser in a link', 'innox' )
	)
);

$option_teaser_vertical_center = array(
	'is_vertical_center'         => array(
		'label' => esc_html__( 'Vertical center', 'innox' ),
		'desc'  => esc_html__( 'Align vertically icon and content in side teasers', 'innox' ),
		'type'  => 'switch',
	),
);

$option_teaser_counter = array(
	'icon_options'    => $icon_options,
	'counter_options' => array(
		'type'    => 'group',
		'options' => array(

			'number'                  => array(
				'type'  => 'text',
				'value' => 10,
				'label' => esc_html__( 'Count To Number', 'innox' ),
				'desc'  => esc_html__( 'Choose value to count to', 'innox' ),
			),
			'counter_additional_text' => array(
				'type'  => 'text',
				'value' => '',
				'label' => esc_html__( 'Add some text after counter', 'innox' ),
				'desc'  => esc_html__( 'You can add "+", "%", decimal values etc.', 'innox' ),
			),
			'speed'                   => array(
				'type'       => 'slider',
				'value'      => 1000,
				'properties' => array(
					'min'  => 500,
					'max'  => 5000,
					'step' => 100,
				),
				'label'      => esc_html__( 'Counter Speed (counter teaser only)', 'innox' ),
				'desc'       => esc_html__( 'Choose counter speed (in milliseconds)', 'innox' ),
			),
		),
	),
);

$options = array(
	'title'        => array(
		'type'  => 'text',
		'label' => esc_html__( 'Teaser Title', 'innox' ),
	),

	'link'         => array(
		'type'  => 'text',
		'value' => '',
		'label' => esc_html__( 'Teaser link', 'innox' ),
		'desc'  => esc_html__( 'Link on title and optional button', 'innox' ),
	),

	'teaser_types' => array(
		'type'         => 'multi-picker',
		'label'        => false,
		'desc'         => false,
		'picker'       => array(
			'teaser_type' => array(
				'type'    => 'image-picker',
				'value'   => 'icon_top',
				'label'   => esc_html__( 'Teaser Type', 'innox' ),
				'desc'    => esc_html__( 'Select one of predefined teaser types', 'innox' ),
				'choices' => array(
					'icon_top'      => fw_get_template_customizations_directory_uri() . '/extensions/shortcodes/shortcodes/teaser/static/img/icon_top.png',
					'icon_left'     => fw_get_template_customizations_directory_uri() . '/extensions/shortcodes/shortcodes/teaser/static/img/icon_left.png',
					'icon_right'    => fw_get_template_customizations_directory_uri() . '/extensions/shortcodes/shortcodes/teaser/static/img/icon_right.png',
					'icon_image_bg' => fw_get_template_customizations_directory_uri() . '/extensions/shortcodes/shortcodes/teaser/static/img/icon_image_bg.png',
					'counter'       => fw_get_template_customizations_directory_uri() . '/extensions/shortcodes/shortcodes/teaser/static/img/icon_counter.png',
					'icon_background' => fw_get_template_customizations_directory_uri() . '/extensions/shortcodes/shortcodes/teaser/static/img/icon_background.png',
				),
				'blank'   => false, // (optional) if true, images can be deselected
			),
		),
		'choices'      => array(
			'icon_top'      => array_merge( $option_teaser_icon, $teaser_center_array, $button_array ),
			'icon_left'     => array_merge( $option_teaser_icon, $option_teaser_vertical_center, $button_array ),
			'icon_right'    => array_merge( $option_teaser_icon, $option_teaser_vertical_center, $button_array ),
			'icon_image_bg' => array_merge( $option_teaser_icon, $option_teaser_square, $button_array ),
			'counter'       => $option_teaser_counter,
			'icon_background' => $option_teaser_icon
		),
		'show_borders' => true,
	),

	'teaser_style' => array(
		'type'    => 'select',
		'label'   => esc_html__( 'Teaser Box Style', 'innox' ),
		'choices' => array(
			''                             => esc_html__( 'Default (no border or background)', 'innox' ),
			'with_padding big-padding rounded with_border'     => esc_html__( 'Bordered', 'innox' ),
			'with_padding big-padding rounded with_background' => esc_html__( 'Muted Background', 'innox' ),
			'with_padding big-padding rounded ls ms'           => esc_html__( 'Grey background', 'innox' ),
			'with_padding big-padding rounded ds'              => esc_html__( 'Dark background', 'innox' ),
			'with_padding big-padding rounded ds ms'           => esc_html__( 'Darkgrey background', 'innox' ),
			'with_padding big-padding rounded main_bg_color'   => esc_html__( 'Accent color', 'innox' ),
		)
	),

	'teaser_accent_color' => array(
		'type'    => 'select',
		'value'   => '',
		'label'   => esc_html__( 'Teaser accent color', 'innox' ),
		'desc'    => esc_html__( 'Color of the title on mouse hover', 'innox' ),
		'choices' => array(
			''       => esc_html__( 'Grey (default)', 'innox' ),
			'dark'   => esc_html__( 'Dark', 'innox' ),
			'color1' => esc_html__( 'Accent Color', 'innox' ),
			'color1 light_bg' => esc_html__( 'Accent Color with Light Background', 'innox' ),
		)
	),

	'content'      => array(
		'type'  => 'wp-editor',
		'label' => esc_html__( 'Teaser text', 'innox' ),
		'desc'  => esc_html__( 'Enter desired teaser content', 'innox' ),
	),

	'title_tag' => array(
		'type'    => 'select',
		'value'   => 'h3',
		'label'   => esc_html__( 'Title Tag', 'innox' ),
		'choices' => array(
			'h2' => esc_html__( 'H2', 'innox' ),
			'h3' => esc_html__( 'H3', 'innox' ),
			'h4' => esc_html__( 'H4', 'innox' ),
			'h5' => esc_html__( 'H5', 'innox' ),
			'h6' => esc_html__( 'H6', 'innox' ),
		)
	),

	'title_text_wight' => array(
		'type'    => 'select',
		'value'   => '',
		'label'   => esc_html__( 'Title text weight', 'innox' ),
		'choices' => array(
			''      => esc_html__( 'Regular', 'innox' ),
			'thin'  => esc_html__( 'Thin', 'innox' ),
			'semibold'  => esc_html__( 'Semibold', 'innox' ),
			'bold'  => esc_html__( 'Bold', 'innox' ),
		),
	),

	'title_text_transform' => array(
		'type'    => 'select',
		'value'   => '',
		'label'   => esc_html__( 'Title text transform', 'innox' ),
		'choices' => array(
			''                => esc_html__( 'None', 'innox' ),
			'text-lowercase'  => esc_html__( 'Lowercase', 'innox' ),
			'text-uppercase'  => esc_html__( 'Uppercase', 'innox' ),
			'text-capitalize' => esc_html__( 'Capitalize', 'innox' ),

		),
	),
);