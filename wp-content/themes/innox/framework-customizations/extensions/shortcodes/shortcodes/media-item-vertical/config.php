<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Media item (vertical)', 'innox' ),
	'description' => esc_html__( 'Add an image with bottom text block', 'innox' ),
	'tab'         => esc_html__( 'Content Elements', 'innox' )
);