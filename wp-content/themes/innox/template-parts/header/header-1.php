<?php
/**
 * The template part for selected header
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$social_icons_list    = function_exists( 'fw_get_db_customizer_option' ) ? fw_get_db_customizer_option( 'social_icons' ) : '';
$social_icons_wrapper    = function_exists( 'fw_get_db_customizer_option' ) ? fw_get_db_customizer_option( 'icons_wrapper_class' ) : '';
$social_icons_options   = array( 'social_icons' => $social_icons_list, 'icons_wrapper_class' => $social_icons_wrapper );
?>

<header class="page_header wide_menu tall_header header_white toggler_xxs_right">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 display-flex v-center">
                <div class="header_left_logo">
					<?php get_template_part( 'template-parts/header/header-logo' ); ?>
                </div>

                <div class="header_mainmenu text-center">
                    <div class="mainmenu_wrapper primary-navigation">
						<?php wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'     => 'sf-menu nav-menu nav',
							'container'      => 'ul'
						) ); ?>
                    </div>
                    <!-- header toggler -->
                    <span class="toggle_menu"><span></span></span>
                </div>

				<?php if ( ! empty( $social_icons_list ) ) : ?>
                    <div class="header_right_buttons hidden-xxs">
                        <?php
                        //get icons-social shortcode to render icons in team member item
                        $shortcodes_extension = fw()->extensions->get( 'shortcodes' );
                        if ( ! empty( $shortcodes_extension ) ) {
                            echo fw_ext( 'shortcodes' )->get_shortcode( 'icons_social' )->render( $social_icons_options );
                        }
                        ?>
                    </div>
				<?php endif; ?>

            </div>
        </div>
    </div>
</header>