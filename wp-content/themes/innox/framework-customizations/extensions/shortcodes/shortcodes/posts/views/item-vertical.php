<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * Shortcode Posts - vertical item layout type 2
 */

$terms          = get_the_terms( get_the_ID(), 'category' );
$filter_classes = '';
foreach ( $terms as $term ) {
	$filter_classes .= ' filter-' . $term->slug;
}

$show_post_thumbnail = ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) ? false : true;

?>
<article <?php post_class( "vertical-item content-padding big-padding with_background text-center" . $filter_classes ); ?>>
    <?php innox_post_thumbnail( false, true ); ?>
    <div class="item-content">

        <?php
        the_title( '<h3 class="entry-title small highlight"><a href="' . esc_url( get_permalink() ) . '">', '</a></h3>' );
        ?>

	    <?php if ( !empty( get_the_content() ) ) : ?>
            <div class="entry-content content-3lines-ellipsis">
			    <?php the_excerpt(); ?>
            </div><!-- .entry-content -->
	    <?php endif; ?>
    </div>

    <footer class="entry-meta ls ms">
        <div class="inline-content with_dividers darklinks grey">
            <span>
                <?php innox_posted_on( true ); ?>
            </span>
                <span>
                <?php echo esc_html__( 'by', 'innox' ) . ' ' . get_the_author_posts_link(); ?>
            </span>
        </div>
    </footer>
</article><!-- eof vertical-item -->
