<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'text' => array(
		'type'   => 'wp-editor',
		'label'  => esc_html__( 'Content', 'innox' ),
		'desc'   => esc_html__( 'Enter some content for this texblock', 'innox' ),
		'reinit' => true,
		'teeny' => false,
	),
);
