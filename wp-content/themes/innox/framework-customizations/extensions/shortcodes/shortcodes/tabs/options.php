<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$contact_form = fw_ext( 'shortcodes' )->get_shortcode( 'contact_form' )->get_options();

$options = array(
	'tabs'       => array(
		'type'          => 'addable-popup',
		'label'         => esc_html__( 'Tabs', 'innox' ),
		'popup-title'   => esc_html__( 'Add/Edit Tabs', 'innox' ),
		'desc'          => esc_html__( 'Create your tabs', 'innox' ),
		'template'      => '{{=tab_title}}',
		'popup-options' => array(
			'tab_title'          => array(
				'type'  => 'text',
				'label' => esc_html__( 'Tab Title', 'innox' )
			),
			'tab_content'        => array(
				'type'  => 'wp-editor',
				'label' => esc_html__( 'Tab Content', 'innox' ),
			),
			'tab_featured_image' => array(
				'type'        => 'upload',
				'value'       => '',
				'label'       => esc_html__( 'Tab Featured Image', 'innox' ),
				'image'       => esc_html__( 'Featured image for your tab', 'innox' ),
				'help'        => esc_html__( 'Image for your tab. It appears on the top of your tab content', 'innox' ),
				'images_only' => true,
			),
			'tab_icon'           => array(
				'type'  => 'icon',
				'label' => esc_html__( 'Icon in tab title', 'innox' ),
				'set'   => 'rt-icons-2',
			),
			'tab_form' => array(
				'type'         => 'multi-picker',
				'label'        => false,
				'desc'         => false,
				'picker'       => array(
					'add_form' => array(
						'type'      => 'switch',
						'value'     => '',
						'label' => esc_html__('Add Form in Tab', 'innox'),


						'left-choice' => array(
							'value' => '',
							'label' => esc_html__('No', 'innox'),
						),
						'right-choice' => array(
							'value' => 'form',
							'label' => esc_html__('Yes', 'innox'),
						),


					),

				),
				'choices'      => array(
					''                  => array(),
					'form'   => array(
						'form_options'  => array(
							'type' => 'popup',
							'button' => esc_html__('Form', 'innox'),
							'size' => 'large', // small, medium, large
							'popup-options' => array(
								$contact_form
							),
						),
					)
				),
			),
		),
	),
	'small_tabs' => array(
		'type'         => 'switch',
		'value'        => '',
		'label'        => esc_html__( 'Small Tabs', 'innox' ),
		'desc'         => esc_html__( 'Decrease tabs size', 'innox' ),
		'left-choice'  => array(
			'value' => '',
			'label' => esc_html__( 'No', 'innox' ),
		),
		'right-choice' => array(
			'value' => 'small-tabs',
			'label' => esc_html__( 'Yes', 'innox' ),
		),
	),
	'half_width' => array(
		'type'         => 'switch',
		'value'        => '',
		'label'        => esc_html__( 'Half Width Tabs', 'innox' ),
		'desc'         => esc_html__( 'Two tabs per line', 'innox' ),
		'left-choice'  => array(
			'value' => '',
			'label' => esc_html__( 'No', 'innox' ),
		),
		'right-choice' => array(
			'value' => 'half-width-tabs',
			'label' => esc_html__( 'Yes', 'innox' ),
		),
	),
	'top_border' => array(
		'type'         => 'switch',
		'value'        => '',
		'label'        => esc_html__( 'Top color border', 'innox' ),
		'desc'         => esc_html__( 'Add top color border to tab content', 'innox' ),
		'left-choice'  => array(
			'value' => '',
			'label' => esc_html__( 'No top border', 'innox' ),
		),
		'right-choice' => array(
			'value' => 'top-color-border',
			'label' => esc_html__( 'Color top border', 'innox' ),
		),
	),
	'id'         => array( 'type' => 'unique' ),
	'tab_color'     => array(
		'type'    => 'select',
		'value'   => '',
		'label'   => esc_html__( 'Accent Color', 'innox' ),
		'choices' => array(
			''       => esc_html__( 'Accent Color 1', 'innox' ),
			'color2' => esc_html__( 'Accent Color 2', 'innox' ),
			'color3' => esc_html__( 'Accent Color 3', 'innox' ),
			'color4' => esc_html__( 'Accent Color 4', 'innox' ),
		),
	),
);