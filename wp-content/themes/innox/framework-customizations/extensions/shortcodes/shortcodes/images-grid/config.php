<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Images Grid', 'innox' ),
		'description' => esc_html__( 'Show images grid with optional links', 'innox' ),
		'tab'         => esc_html__( 'Media Elements', 'innox' ),
	)
);