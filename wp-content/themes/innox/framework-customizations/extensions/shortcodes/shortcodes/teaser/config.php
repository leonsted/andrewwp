<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

//$cfg = array();

$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Teaser Icon Box', 'innox' ),
	'description' => esc_html__( 'Add a various teaser icon boxes', 'innox' ),
	'tab'         => esc_html__( 'Content Elements', 'innox' )
);