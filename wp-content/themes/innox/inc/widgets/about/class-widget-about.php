<?php if ( ! defined( 'ABSPATH' ) ) {
	die();
}
if ( ( defined( 'FW' ) ) && ! ( class_exists( 'Innox_Widget_About' ) ) ) :

class Innox_Widget_About extends WP_Widget {

	/**
	 * Widget constructor.
	 */
	private $options;
	private $prefix;

	function __construct() {

		$widget_ops = array(
			'classname'   => 'widget_about',
			'description' => esc_html__( 'Add company logo with description and social icons', 'innox' ),
		);

		parent::__construct( false, esc_html__( 'Theme - About', 'innox' ), $widget_ops );

		//find fw_ext
		$shortcodes_extension = fw()->extensions->get( 'shortcodes' );
		$button_options = array();
		if ( ! empty( $shortcodes_extension ) ) {
			$button_options = $shortcodes_extension->get_shortcode( 'button' )->get_options();
		}

		//Create our options by using Unyson option types
		$this->options = array(
			'title' => array(
				'type'  => 'text',
				'label' => esc_html__( 'Widget Title', 'innox' ),
			),
			'show_logo'     => array(
				'label' => esc_html__( 'Display logo?', 'innox' ),
				'type'  => 'switch',
				'value' => false,
				'left-choice' => array(
					'value' => false,
					'label' => esc_html__('No', 'innox'),
				),
				'right-choice' => array(
					'value' => true,
					'label' => esc_html__('Yes', 'innox'),
				),
			),
			'about' => array(
				'type'  => 'wp-editor',
				'value' => '',
				'label' => esc_html__('Description', 'innox'),
			),
			'social_icons' => array(
				'type'            => 'addable-box',
				'value'           => '',
				'label'           => esc_html__( 'Social Buttons', 'innox' ),
				'desc'            => esc_html__( 'Optional social buttons', 'innox' ),
				'template'        => '{{=icon}}',
				'box-options'     => array(
					'icon'       => array(
						'type'  => 'icon',
						'label' => esc_html__( 'Social Icon', 'innox' ),
						'set'   => 'social-icons',
					),
					'icon_class' => array(
						'type'        => 'select',
						'value'       => '',
						'label'       => esc_html__( 'Icon type', 'innox' ),
						'desc'        => esc_html__( 'Select one of predefined social button types', 'innox' ),
						'choices'     => array(
							''                                    => esc_html__( 'Default', 'innox' ),
							'border-icon'                         => esc_html__( 'Simple Bordered Icon', 'innox' ),
							'border-icon rounded-icon'            => esc_html__( 'Rounded Bordered Icon', 'innox' ),
							'bg-icon'                             => esc_html__( 'Simple Background Icon', 'innox' ),
							'bg-icon rounded-icon'                => esc_html__( 'Rounded Background Icon', 'innox' ),
							'color-icon bg-icon'                  => esc_html__( 'Color Light Background Icon', 'innox' ),
							'color-icon bg-icon rounded-icon'     => esc_html__( 'Color Light Background Rounded Icon', 'innox' ),
							'color-icon'                          => esc_html__( 'Color Icon', 'innox' ),
							'color-icon border-icon'              => esc_html__( 'Color Bordered Icon', 'innox' ),
							'color-icon border-icon rounded-icon' => esc_html__( 'Rounded Color Bordered Icon', 'innox' ),
							'color-bg-icon'                       => esc_html__( 'Color Background Icon', 'innox' ),
							'color-bg-icon rounded-icon'          => esc_html__( 'Rounded Color Background Icon', 'innox' ),

						),
						/**
						 * Allow save not existing choices
						 * Useful when you use the select to populate it dynamically from js
						 */
						'no-validate' => false,
					),
					'icon_url'   => array(
						'type'  => 'text',
						'value' => '',
						'label' => esc_html__( 'Icon Link', 'innox' ),
						'desc'  => esc_html__( 'Provide a URL to your icon', 'innox' ),
					)
				),
				'limit'           => 0, // limit the number of boxes that can be added
				'add-button-text' => esc_html__( 'Add', 'innox' ),
				'sortable'        => true,
			),
			'custom_buttons'     => array(
				'type'        => 'addable-box',
				'value'       => '',
				'label'       => esc_html__( 'Custom Buttons', 'innox' ),
				'desc'        => esc_html__( 'Choose a button, link for it and text inside it', 'innox' ),
				'template'    => 'Button',
				'box-options' => array(
					$button_options
				),
				'limit'           => 5, // limit the number of boxes that can be added
				'add-button-text' => esc_html__( 'Add', 'innox' ),
			),
		);
		$this->prefix  = 'widget_about';
	}

	function widget( $args, $instance ) {
		extract( wp_parse_args( $args ) );

		$title     = esc_attr( $instance['title'] );
		if ( !empty( $instance['title'] ) ) {
			$title = $before_title  . $title . $after_title;
		} else {
			$title = '';
		}

		$params = array();

		foreach ( $instance as $key => $value ) {
			$params[ $key ] = $value;
		}

		$instance = $params;

		$filepath = INNOX_THEME_PATH . '/inc/widgets/about/views/widget.php';

		if ( file_exists( $filepath ) ) {
			include( $filepath );
		} else {
			esc_html_e( 'View not found', 'innox' );
		}
	}

	function update( $new_instance, $old_instance ) {
		return fw_get_options_values_from_input(
			$this->options,
			FW_Request::POST( fw_html_attr_name_to_array_multi_key( $this->get_field_name( $this->prefix ) ), array() )
		);
	}

	function form( $values ) {

		$prefix = $this->get_field_id( $this->prefix ); // Get unique prefix, preventing duplicated key
		$id     = 'fw-widget-options-' . $prefix;

		// Print our options
		echo '<div class="fw-force-xs fw-theme-admin-widget-wrap fw-framework-widget-options-widget" data-fw-widget-id="' . esc_attr( $id ) . '" id="' . esc_attr( $id ) . '">';

		echo fw()->backend->render_options( $this->options, $values, array(
			'id_prefix'   => $prefix . '-',
			'name_prefix' => $this->get_field_name( $this->prefix ),
		) );
		echo '</div>';

		return $values;
	}
}

endif;