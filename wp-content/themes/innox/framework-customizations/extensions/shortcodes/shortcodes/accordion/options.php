<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'tabs' => array(
		'type'          => 'addable-popup',
		'label'         => esc_html__( 'Panels', 'innox' ),
		'popup-title'   => esc_html__( 'Add/Edit Accordion Panels', 'innox' ),
		'desc'          => esc_html__( 'Create your accordion panels', 'innox' ),
		'template'      => '{{=tab_title}}',
		'popup-options' => array(
			'tab_title'          => array(
				'type'  => 'text',
				'label' => esc_html__( 'Title', 'innox' )
			),
			'tab_content'        => array(
				'type'  => 'wp-editor',
				'label' => esc_html__( 'Content', 'innox' )
			),
			'tab_featured_image' => array(
				'type'        => 'upload',
				'value'       => '',
				'label'       => esc_html__( 'Panel Featured Image', 'innox' ),
				'image'       => esc_html__( 'Image for your panel.', 'innox' ),
				'help'        => esc_html__( 'It appears to the left from your content', 'innox' ),
				'images_only' => true,
			),
			'tab_icon'           => array(
				'type'  => 'icon',
				'label' => esc_html__( 'Icon in panel title', 'innox' ),
				'set'   => 'rt-icons-2',
			),
		)
	),
	'id'   => array( 'type' => 'unique' ),
	'accordion_color'     => array(
		'type'    => 'select',
		'value'   => '',
		'label'   => esc_html__( 'Accent Color', 'innox' ),
		'choices' => array(
			''       => esc_html__( 'Accent Color', 'innox' ),
			'collapse-unstyled' => esc_html__( 'Unstyled', 'innox' ),
		),
	),
);