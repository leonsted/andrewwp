<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => esc_html__('Video', 'innox'),
	'description'   => esc_html__('Add a Video', 'innox'),
	'tab'           => esc_html__('Media Elements', 'innox'),
);
