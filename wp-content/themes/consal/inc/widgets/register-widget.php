<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

function consal_widgets_init() {
	
	register_sidebar( array(
		'name' => esc_html__( 'Blog Sidebar', 'consal' ),
		'id' => 'sidebar-1',
		'description' => esc_html__( 'Sidebar on blog page', 'consal' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<div class="widget_title"><h4>',
		'after_title' => '</h4></div>',
	) );
	
	register_sidebar( array(
		'name' => esc_html__( 'Pages Sidebar', 'consal' ),
		'id' => 'sidebar-page',
		'description' => esc_html__( 'Sidebar on content pages', 'consal' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<div class="widget_title"><h4>',
		'after_title' => '</h4></div>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar', 'consal' ),
		'id'            => 'footer-sidebar',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="footer_widgets %2$s col-xs-12 col-md-3">',
		'after_widget' => '</aside>',
		'before_title' => '<div class="widget_title"><h4>',
		'after_title' => '</h4></div>',
	) );
	
}
add_action( 'widgets_init', 'consal_widgets_init' );

/**
 * Widget
 */
require_once get_template_directory() . '/inc/widgets/consal-about-widget.php';
require_once get_template_directory() . '/inc/widgets/consal-recent-post-widget.php';