<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'consal' ); ?></a>
	
	
    <!-- preloader -->
    <div class="preloader-bg">
        <div class="preloader"></div>
    </div>
	

    <!--================================
        1.START HERO-SECTION
    =================================-->
    <header class="hero_section">
		<?php consal_header_top() ?>	
        <!-- main menu starts -->
        <div class="mainmenu">
            <nav class="navbar navbar-default">
              <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only"><?php esc_html_e('Toggle navigation','consal'); ?></span>
                    <span class="cross">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </span>
                  </button>
				  <?php consal_logo();?>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-right navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php consal_main_menu (); ?>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container -->
            </nav>
        </div>
        <!-- main menu ends -->
    </header>
    <!--================================
        1.END HERO-SECTION
    =================================-->

	<div id="content" class="site-content">
