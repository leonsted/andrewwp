<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$map_shortcode = fw_ext('shortcodes')->get_shortcode('map');
$options = array(
	'data_provider' => array(
		'type'  => 'multi-picker',
		'label' => false,
		'desc'  => false,
		'picker' => array(
			'population_method' => array(
				'label'   => esc_html__('Population Method', 'innox'),
				'desc'    => esc_html__( 'Select map population method (Ex: events, custom)', 'innox' ),
				'type'    => 'select',
				'choices' => $map_shortcode->_get_picker_dropdown_choices(),
			)
		),
		'choices' => $map_shortcode->_get_picker_choices(),
		'show_borders' => false,
		'hide_picker' => true
	),
	'gmap-key' => array_merge(
		array(
			'label' => esc_html__( 'Google Maps API Key', 'innox' ),
			'desc' => sprintf(
				__( 'Create an application in %sGoogle Console%s and add the Key here.', 'innox' ),
				'<a href="https://console.developers.google.com/flows/enableapi?apiid=places_backend,maps_backend,geocoding_backend,directions_backend,distance_matrix_backend,elevation_backend&keyType=CLIENT_SIDE&reusekey=true">',
				'</a>'
			),
		),
		version_compare(fw()->manifest->get_version(), '2.5.7', '>=')
		? array(
			'type' => 'gmap-key',
		)
		: array(
			'type' => 'text',
			'fw-storage' => array(
				'type'      => 'wp-option',
				'wp_option' => 'fw-option-types:gmap-key',
			),
		)
	),
	'map_type' => array(
		'type'  => 'select',
		'label' => esc_html__('Map Type', 'innox'),
		'desc'  => esc_html__('Select map type', 'innox'),
		'choices' => array(
			'roadmap'   => esc_html__('Roadmap', 'innox'),
			'terrain' => esc_html__('Terrain', 'innox'),
			'satellite' => esc_html__('Satellite', 'innox'),
			'hybrid'    => esc_html__('Hybrid', 'innox')
		)
	),
	'map_height' => array(
		'label' => esc_html__('Map Height', 'innox'),
		'desc'  => esc_html__('Set map height (Ex: 300)', 'innox'),
		'type'  => 'text'
	),
	'disable_scrolling' => array(
		'type'  => 'switch',
		'value' => false,
		'label' => esc_html__('Disable zoom on scroll', 'innox'),
		'desc'  => esc_html__('Prevent the map from zooming when scrolling until clicking on the map', 'innox'),
		'left-choice' => array(
			'value' => false,
			'label' => esc_html__('Yes', 'innox'),
		),
		'right-choice' => array(
			'value' => true,
			'label' => esc_html__('No', 'innox'),
		),
	),
	'map_zoom' => array(
		'type'  => 'slider',
		'value' => 16,
		'properties' => array(
			/*
			'min' => 0,
			'max' => 100,
			'step' => 1, // Set slider step. Always > 0. Could be fractional.
			*/
		),
		'label' => esc_html__('Map zoom', 'innox'),
	),

	'map_marker' => array(
		'type'  => 'background-image',
		'label' => esc_html__('Map Location Marker', 'innox'),
		'desc'  => esc_html__('Choose an image to display as map location marker', 'innox'),
//		'images_only' => true,
	)

);