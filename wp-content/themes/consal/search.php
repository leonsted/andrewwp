<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */

global $consal_opt;

get_header();

if(isset($consal_opt)){
	$bloglayout = 'nosidebar';
} else {
	$bloglayout = 'sidebar';
}
if(isset($consal_opt['blog_layout']) && $consal_opt['blog_layout']!=''){
	$bloglayout = $consal_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$bloglayout = $_GET['layout'];
}
$blogsidebar = 'right';
if(isset($consal_opt['sidebarblog_pos']) && $consal_opt['sidebarblog_pos']!=''){
	$blogsidebar = $consal_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$blogsidebar = $_GET['sidebar'];
}
switch($bloglayout) {
	case 'sidebar':
		$blogclass = 'blog-sidebar';
		$blogcolclass = 9;
		$consal_postthumb = 'consal-category-thumb'; //750x510px
		break;
	case 'fullwidth':
		$blogclass = 'blog-fullwidth';
		$blogcolclass = 12;
		$blogsidebar = 'none';
		$consal_postthumb = 'consal-category-full'; //1144x510px
		break;
	default:
		$blogclass = 'blog-nosidebar';
		$blogcolclass = 12;
		$blogsidebar = 'none';
		$consal_postthumb = 'consal-post-thumb'; //500x500px
} consal_pages_breadcrumb(); ?>

	<div class="section_padding blog_posts padding_bottom">
		<div id="primary" class="content-area">
			<div id="main" class="site-main">	
				<div class="container">
					<div class="row">
						<?php if($blogsidebar=='left') : ?>
							<?php get_sidebar(); ?>
						<?php endif; ?>	
						<div class="blog-grid-area col-xs-12 <?php echo 'col-md-'.$blogcolclass; ?>">
						<?php
						if ( have_posts() ) : ?>
							<div class="blog-grid-area-inner">
							<?php 
							/* Start the Loop */
							while ( have_posts() ) : the_post();

								/*
								 * Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'template-parts/content-search', get_post_format() );

							endwhile; ?>
							</div>
							<div class="pagination_area">				
								<?php the_posts_pagination( array(
								'type'      => 'list',
								'prev_text' => __( '<i class="fa fa-angle-left"></i>', 'consal' ),
								'next_text' => __( '<i class="fa fa-angle-right"></i>', 'consal' ),
								'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Page', 'consal' ) . ' </span>',
								) ); ?>
								
							</div>	
							
						<?php else :

							get_template_part( 'template-parts/content', 'none' );

						endif; ?>
						</div>
						<?php if( $blogsidebar=='right') : ?>
							<?php get_sidebar(); ?>
						<?php endif; ?>						
					</div>
				</div>	
			</div><!-- #main -->
		</div><!-- #primary -->	
	</div>

<?php
get_footer();
