<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$custom_column_classes = array(
	'custom_classes'    => array(
		'type'  => 'text',
		'label' => esc_html__( 'Custom column classes', 'innox' ),
		'desc'  => esc_html__( 'Enter custom column classes', 'innox' ),
	),
);

$options = array(
	'column_align'     => array(
		'type'    => 'select',
		'value'   => '',
		'label'   => esc_html__( 'Text alignment in column', 'innox' ),
		'desc'    => esc_html__( 'Select text alignment inside your column', 'innox' ),
		'choices' => array(
			''            => esc_html__( 'Inherit', 'innox' ),
			'text-left'   => esc_html__( 'Left', 'innox' ),
			'text-center' => esc_html__( 'Center', 'innox' ),
			'text-right'  => esc_html__( 'Right', 'innox' ),
		),
	),
	'column_padding'   => array(
		'type'    => 'select',
		'value'   => '',
		'label'   => esc_html__( 'Column padding', 'innox' ),
		'desc'    => esc_html__( 'Select optional internal column paddings', 'innox' ),
		'choices' => array(
			''           => esc_html__( 'No padding', 'innox' ),
			'padding_10' => esc_html__( '10px', 'innox' ),
			'padding_20' => esc_html__( '20px', 'innox' ),
			'padding_30' => esc_html__( '30px', 'innox' ),
			'padding_40' => esc_html__( '40px', 'innox' ),
            'with_padding' => esc_html__( 'Theme style padding', 'innox' ),
            'with_padding big-padding' => esc_html__( 'Theme style big padding', 'innox' ),

		),
	),
	'background_color' => array(
		'type'    => 'select',
		'value'   => '',
		'label'   => esc_html__( 'Background color', 'innox' ),
		'desc'    => esc_html__( 'Select background color', 'innox' ),
		'help'    => esc_html__( 'Select one of predefined background colors', 'innox' ),
		'choices' => array(
			''                      => esc_html__( 'Transparent (No Background)', 'innox' ),
			'with_background'      => esc_html__( 'Muted', 'innox' ),
			'ds'                    => esc_html__( 'Dark Grey', 'innox' ),
			'ds ms'                 => esc_html__( 'Dark', 'innox' ),
			'main_bg_color'         => esc_html__( 'Accent color 1', 'innox' ),
			'main_bg_color2'        => esc_html__( 'Accent color 2', 'innox' ),
		),
	),
	'column_border' => array(
		'type'    => 'select',
		'value'   => '',
		'label'   => esc_html__( 'Column border', 'innox' ),
		'desc'    => esc_html__( 'Select optional border for column', 'innox' ),
		'choices' => array(
			''                      => esc_html__( 'Transparent (No Background)', 'innox' ),
			'with_background'       => esc_html__( 'No border', 'innox' ),
			'with_border'           => esc_html__( 'With thin border', 'innox' ),
			'with_border thick_border'  => esc_html__( 'With thick border', 'innox' ),
		),
	),
	'background_image' => array(
		'label'   => esc_html__( 'Background Image', 'innox' ),
		'desc'    => esc_html__( 'Select the background image', 'innox' ),
		'type'    => 'background-image',
		'choices' => array(//	in future may will set predefined images
		)
	),
	'column_inner_box_custom_class'    => array(
		'type'  => 'text',
		'label' => esc_html__( 'Custom class', 'innox' ),
		'desc'  => esc_html__( 'This class will be applied to the inner "div" of the column', 'innox' ),
	),
	'column_animation' => array(
		'type'    => 'select',
		'value'   => '',
		'label'   => esc_html__( 'Animation type', 'innox' ),
		'desc'    => esc_html__( 'Select one of predefined animations', 'innox' ),
		'choices' => array(
			''               => esc_html__( 'None', 'innox' ),
			'slideDown'      => esc_html__( 'slideDown', 'innox' ),
			'scaleAppear'    => esc_html__( 'scaleAppear', 'innox' ),
			'fadeInLeft'     => esc_html__( 'fadeInLeft', 'innox' ),
			'fadeInUp'       => esc_html__( 'fadeInUp', 'innox' ),
			'fadeInRight'    => esc_html__( 'fadeInRight', 'innox' ),
			'fadeInDown'     => esc_html__( 'fadeInDown', 'innox' ),
			'fadeIn'         => esc_html__( 'fadeIn', 'innox' ),
			'slideRight'     => esc_html__( 'slideRight', 'innox' ),
			'slideUp'        => esc_html__( 'slideUp', 'innox' ),
			'slideLeft'      => esc_html__( 'slideLeft', 'innox' ),
			'expandUp'       => esc_html__( 'expandUp', 'innox' ),
			'slideExpandUp'  => esc_html__( 'slideExpandUp', 'innox' ),
			'expandOpen'     => esc_html__( 'expandOpen', 'innox' ),
			'bigEntrance'    => esc_html__( 'bigEntrance', 'innox' ),
			'hatch'          => esc_html__( 'hatch', 'innox' ),
			'tossing'        => esc_html__( 'tossing', 'innox' ),
			'pulse'          => esc_html__( 'pulse', 'innox' ),
			'floating'       => esc_html__( 'floating', 'innox' ),
			'bounce'         => esc_html__( 'bounce', 'innox' ),
			'pullUp'         => esc_html__( 'pullUp', 'innox' ),
			'pullDown'       => esc_html__( 'pullDown', 'innox' ),
			'stretchLeft'    => esc_html__( 'stretchLeft', 'innox' ),
			'stretchRight'   => esc_html__( 'stretchRight', 'innox' ),
			'fadeInUpBig'    => esc_html__( 'fadeInUpBig', 'innox' ),
			'fadeInDownBig'  => esc_html__( 'fadeInDownBig', 'innox' ),
			'fadeInLeftBig'  => esc_html__( 'fadeInLeftBig', 'innox' ),
			'fadeInRightBig' => esc_html__( 'fadeInRightBig', 'innox' ),
			'slideInDown'    => esc_html__( 'slideInDown', 'innox' ),
			'slideInLeft'    => esc_html__( 'slideInLeft', 'innox' ),
			'slideInRight'   => esc_html__( 'slideInRight', 'innox' ),
			'moveFromLeft'   => esc_html__( 'moveFromLeft', 'innox' ),
			'moveFromRight'  => esc_html__( 'moveFromRight', 'innox' ),
			'moveFromBottom' => esc_html__( 'moveFromBottom', 'innox' ),
		),
	),
	'animation_delay'    => array(
		'type'  => 'text',
		'label' => esc_html__( 'Animation delay (milliseconds)', 'innox' ),
		'desc'  => esc_html__( 'You can leave it empty, default values would be used', 'innox' ),
	),
	'custom_column' => array(
		'type'    => 'multi-picker',
		'label'   => false,
		'desc'    => false,
		'picker'  => array(
			'custom' => array(
				'type'         => 'switch',
				'label'        => esc_html__( 'Custom column layout', 'innox' ),
				'desc'        => esc_html__( 'Set your own column classes. It overrides other options. Use it only if you know what you doing', 'innox' ),
				'left-choice'  => array(
					'value' => '',
					'label' => esc_html__( 'No', 'innox' ),
				),
				'right-choice' => array(
					'value' => 'custom_cl',
					'label' => esc_html__( 'Yes', 'innox' ),
				),
			),
		),
		'choices' => array(
			''       => array(),
			'custom_cl' => $custom_column_classes,
		),
        'show_borders' => true,
	)

);
