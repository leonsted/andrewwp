<?php 
/**
 * Adds About info Widget.
 */

class Consal_info_widget extends WP_Widget{
	/**
	 * Register widget with WordPress.
	 */
	function __construct(){
		$widget_ops = array( 'info' => esc_html__('About Info.', 'consal'),'customize_selective_refresh' => true, );
 		parent:: __construct('Consal_info_widget', esc_html__('Consal: About Info', 'consal'),$widget_ops );
	}
	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget($args, $instance){
		$image	= isset( $instance['image'] ) ? $instance['image'] : '';
		$info  = isset( $instance['info'] ) ? $instance['info'] : '';
		?>
		<?php echo $args['before_widget']; ?>
			<div class="about_us_wrapper">
				<div class="footer_logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo-footer"><img src="<?php echo esc_attr ($image); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
				</div>
				<div class="footer_about_us">
					<p><?php echo esc_attr ($info); ?></p>
				</div>
			</div>
		<?php echo $args['after_widget']; ?>

		<?php
	}
	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update($new_instance, $old_instance){
		$instace = array();
		$instance['image']   = ( !empty( $new_instance['image'] ) ) ? strip_tags ( $new_instance['image'] ) : '';
		$instance['info'] = ( !empty($new_instance['info']) ) ? strip_tags ( $new_instance['info'] ) : '';
		return $instance;

	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form($instance){

		$image 		 = !empty($instance['image']) ? $instance['image'] : '';
		$info = !empty($instance['info']) ? $instance['info'] : '';
	?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('image')); ?>"><?php esc_html_e('Image:' ,'consal') ?></label>
			<input type="text" id="<?php echo esc_attr($this->get_field_id( 'image' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'image' )); ?>" value="<?php echo esc_attr( $image ); ?>" class="widefat image" />
		</p>
		<p>
			 <button type="submit" class="upload_image btn btn-primary"><?php esc_html_e('Select/ Upload','consal' ) ?></button>
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('info')); ?>"><?php esc_html_e('Description:' ,'consal') ?></label>
			<textarea id="<?php echo esc_attr($this->get_field_id('info')); ?>" name="<?php echo esc_attr($this->get_field_name('info')); ?>" rows="6" class="widefat"><?php echo esc_attr( $info ); ?></textarea>
		<p>

	<?php
	}
}

// register Short description widget
function consal_info_widget() {
    register_widget( 'Consal_info_widget' );
}
add_action( 'widgets_init', 'consal_info_widget' );