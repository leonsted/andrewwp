<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * Single service loop item layout
 */

$ext_services_settings = fw()->extensions->get( 'services' )->get_settings();
$taxonomy_name = $ext_services_settings['taxonomy_name'];

$icon_array = fw_ext_services_get_icon();
?>

<article <?php post_class( 'vertical-item content-padding big-padding ds ms with_shadow text-center' ) ?>>
    <div class="item-media">
	    <?php the_post_thumbnail( 'innox-square-width' ); ?>
    </div>
    <div class="item-content">
        <header class="entry-header">
            <span class="small-text highlight"><?php echo esc_html__( 'industry', 'innox' ); ?></span>
            <h3 class="entry-title">
                <a href="<?php the_permalink(); ?>">
			        <?php the_title(); ?>
                </a>
            </h3>
        </header>
        <div>
		    <?php the_excerpt(); ?>
        </div>
        <div class="topmargin_35">
            <a href="<?php the_permalink(); ?>" class="theme_button color1 block_button"><?php echo esc_html__( 'Read more', 'innox' ); ?></a>
        </div>
    </div>
</article><!-- eof .teaser -->