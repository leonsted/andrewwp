<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Custom Container Start', 'innox' ),
	'description' => esc_html__( 'Wrap elements in custom container. Opening container tag', 'innox' ),
	'tab'         => esc_html__( 'Layout Elements', 'innox' )
);