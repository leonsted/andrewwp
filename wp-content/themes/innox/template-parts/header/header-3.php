<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 6/5/18
 * Time: 11:31 AM
 */

$slider_id = fw_get_db_post_option( get_the_ID(), 'slider_id', false );
?>

<?php if ( $slider_id ) : ?>
<div class="transparent_wrapper">
<?php endif; ?>
<header class="page_header header_darkgrey ds page_header_side vertical_menu_header">
	<div class="container-fluid">
		<div class="row flex-wrap v-center">
			<div class="col-xs-12">
				<?php get_template_part( 'template-parts/header/header-logo-dark' ); ?>

				<span class="toggle_menu_side header-slide"><span></span></span>

				<div class="scrollbar-macosx">
					<div class="side_header_inner">
						<div class="container-fluid">
							<div class="row flex-wrap v-center">
								<div class="col-xs-12 col-sm-6">
									<?php get_template_part( 'template-parts/header/header-logo-dark' ); ?>
								</div>
								<div class="col-xs-12 col-sm-6 text-center text-sm-right">
									<div class="widget_search">
										<?php get_search_form() ?>
									</div>
								</div>
							</div>
						</div>
						<div class="header-side-menu darklinks">
							<?php wp_nav_menu( array(
								'theme_location' => 'primary',
								'menu_class'     => 'nav menu-side-click',
								'container'      => 'ul'
							) ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<?php if ( $slider_id ) : ?>
</div>
<?php endif; ?>
