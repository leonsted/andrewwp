<?php
/**
 * The template for displaying all single service posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */
get_header();

consal_pages_breadcrumb(); 

?>

	<section class="services_section section_padding">
		<div id="primary" class="content-area">
			<div id="main" class="site-main">	
				<div class="container">

					<div class="blog-grid-area section_padding col-xs-12 <?php echo 'col-md-'.$pagecolclass; ?>">
					<?php
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/service', 'single' );					
					endwhile; // End of the loop.
					?>
					</div>

				</div>
			</div><!-- #main -->
		</div><!-- #primary -->	
	</section>
    <!-- 3.END PROJECT DETAIL -->
    

<?php get_footer();