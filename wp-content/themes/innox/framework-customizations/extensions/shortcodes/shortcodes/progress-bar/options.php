<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(

	'title' => array(
		'type'       => 'text',
		'value'      => '',
		'label'      => esc_html__( 'Progress Bar title', 'innox' ),
	),
	'percent' => array(
		'type'       => 'slider',
		'value'      => 80,
		'properties' => array(
			'min'  => 0,
			'max'  => 100,
			'step' => 1,
		),
		'label'      => esc_html__( 'Count To', 'innox' ),
		'desc'       => esc_html__( 'Choose percent to count to', 'innox' ),
	),
	'background_class' => array(
		'type'    => 'select',
		'value'   => '',
		'label'   => esc_html__( 'Context background color', 'innox' ),
		'desc'    => esc_html__( 'Select one of predefined background colors', 'innox' ),
		'choices' => array(
			'' => esc_html__( 'Accent Color', 'innox' ),
			'progress-bar-success' => esc_html__( 'Success', 'innox' ),
			'progress-bar-info'    => esc_html__( 'Info', 'innox' ),
			'progress-bar-warning' => esc_html__( 'Warning', 'innox' ),
			'progress-bar-danger'  => esc_html__( 'Danger', 'innox' ),

		),
	),
);