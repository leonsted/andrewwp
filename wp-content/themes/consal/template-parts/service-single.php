<?php
/**
 * Template part for displaying services posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */
?>
    


<!-- SINGLE TAB CONTENT -->
<div class="service_detail_wrapper">

	<div class="row">

		<div class="col-md-12">
			<div class="single_service_img">
			<?php the_post_thumbnail(); ?>
			</div>

			<div class="service_title">
			<h3><?php the_title();?></h3>
			</div>

			<div class="service_detail">
			<?php the_content();?>
			</div>	
		</div>

	</div>


<?php
	global $consal_opt;
	$show_rel_service = $consal_opt['show_rel_service'];
	if ( $show_rel_service ) {
?>

	<div class="related-services clearfix">
		<h2 class="wpb_heading"><?php echo esc_html( $consal_opt['rel_ser_title'] ); ?></h2>
		
		<div class="row">
		<?php
			$postargs = array(
				'exclude'          => get_the_ID(),
				'post_type' 	   => 'service',
				'posts_per_page'    => 4,
			);					
			$rel_services = get_posts( $postargs );
			foreach ( $rel_services as $post ) {
			?>						
				<div class="col-md-3 rel-sing-ser">
					<a href="<?php the_permalink();?>"><?php the_post_thumbnail('consal-homepost-thumb'); ?></a>
					<a href="<?php the_permalink();?>"><h4><?php the_title(); ?></h4></a>
				</div>						
			<?php
			}
			wp_reset_postdata();
		?>				
		</div>
		
	</div>

<?php
	}
?>

</div>