<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Pricing plan', 'innox' ),
		'description' => esc_html__( 'Display single pricing plan', 'innox' ),
		'tab'         => esc_html__( 'Content Elements', 'innox' )
	)
);