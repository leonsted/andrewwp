<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
//get social icons to add in member item:
$icons_social = fw_ext( 'shortcodes' )->get_shortcode( 'icons_social' );

$options = array(
	'image' => array(
		'label' => esc_html__( 'Person Image', 'innox' ),
		'desc'  => esc_html__( 'Either upload a new, or choose an existing image from your media library', 'innox' ),
		'type'  => 'upload'
	),
	'name'  => array(
		'label' => esc_html__( 'Person Name', 'innox' ),
		'desc'  => esc_html__( 'Name of the person', 'innox' ),
		'type'  => 'text',
		'value' => ''
	),
	'job'   => array(
		'label' => esc_html__( 'Person Position Title', 'innox' ),
		'type'  => 'text',
		'value' => ''
	),
	'accent_color'  => array(
		'type'    => 'select',
		'value'   => 'highlight',
		'label'   => esc_html__( 'Accent Color', 'innox' ),
		'choices' => array(
			'highlight'  => esc_html__( 'Accent Color 1', 'innox' ),
			'highlight2'  => esc_html__( 'Accent Color 2', 'innox' ),
			'highlight3'  => esc_html__( 'Accent Color 3', 'innox' ),
			'highlight4'  => esc_html__( 'Accent Color 4', 'innox' ),
		)
	),
);