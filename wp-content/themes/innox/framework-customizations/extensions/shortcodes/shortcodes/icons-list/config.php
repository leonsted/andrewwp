<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Icons in list', 'innox' ),
		'description' => esc_html__( 'Several icons in bordered list', 'innox' ),
		'tab'         => esc_html__( 'Content Elements', 'innox' ),
	)
);