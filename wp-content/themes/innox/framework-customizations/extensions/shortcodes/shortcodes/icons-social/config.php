<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Social Icons', 'innox' ),
	'description' => esc_html__( 'Add set of social icons', 'innox' ),
	'tab'         => esc_html__( 'Content Elements', 'innox' )
);