<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$events = fw()->extensions->get( 'events' );
if ( empty( $events ) ) {
	return;
}

$options = array(
	'number'        => array(
		'type'       => 'text',
		'value'      => '6',
		'label'      => esc_html__( 'Items number', 'innox' ),
		'desc'       => esc_html__( 'Number of events to display', 'innox' ),
	),
	'margin'        => array(
		'label'   => esc_html__( 'Horizontal item margin (px)', 'innox' ),
		'desc'    => esc_html__( 'Select horizontal item margin', 'innox' ),
		'value'   => '30',
		'type'    => 'select',
		'choices' => array(
			'0'  => esc_html__( '0', 'innox' ),
			'1'  => esc_html__( '1px', 'innox' ),
			'2'  => esc_html__( '2px', 'innox' ),
			'10' => esc_html__( '10px', 'innox' ),
			'30' => esc_html__( '30px', 'innox' ),
		)
	),
	'item_layout'        => array(
		'label'   => esc_html__( 'Item Layout', 'innox' ),
		'desc'    => esc_html__( 'Choose item layout', 'innox' ),
		'value'   => 'vertical',
		'type'    => 'select',
		'choices' => array(
			'vertical' => esc_html__( 'Vertical', 'innox' ),
			'horizontal'  => esc_html__( 'Horizontal', 'innox' ),
		)
	),
	'layout'        => array(
		'label'   => esc_html__( 'Post Layout', 'innox' ),
		'desc'    => esc_html__( 'Choose post layout', 'innox' ),
		'value'   => 'carousel',
		'type'    => 'select',
		'choices' => array(
			'carousel' => esc_html__( 'Carousel', 'innox' ),
			'isotope'  => esc_html__( 'Masonry Grid', 'innox' ),
		)
	),
	'responsive_lg' => array(
		'label'   => esc_html__( 'Columns on large screens', 'innox' ),
		'desc'    => __( 'Select items number on wide screens (>1200px)', 'innox' ),
		'value'   => '4',
		'type'    => 'select',
		'choices' => array(
			'1' => esc_html__( '1', 'innox' ),
			'2' => esc_html__( '2', 'innox' ),
			'3' => esc_html__( '3', 'innox' ),
			'4' => esc_html__( '4', 'innox' ),
			'6' => esc_html__( '6', 'innox' ),
		)
	),
	'responsive_md' => array(
		'label'   => esc_html__( 'Columns on middle screens', 'innox' ),
		'desc'    => __( 'Select items number on middle screens (>992px)', 'innox' ),
		'value'   => '3',
		'type'    => 'select',
		'choices' => array(
			'1' => esc_html__( '1', 'innox' ),
			'2' => esc_html__( '2', 'innox' ),
			'3' => esc_html__( '3', 'innox' ),
			'4' => esc_html__( '4', 'innox' ),
			'6' => esc_html__( '6', 'innox' ),
		)
	),
	'responsive_sm' => array(
		'label'   => esc_html__( 'Columns on small screens', 'innox' ),
		'desc'    => __( 'Select items number on small screens (>768px)', 'innox' ),
		'value'   => '2',
		'type'    => 'select',
		'choices' => array(
			'1' => esc_html__( '1', 'innox' ),
			'2' => esc_html__( '2', 'innox' ),
			'3' => esc_html__( '3', 'innox' ),
			'4' => esc_html__( '4', 'innox' ),
			'6' => esc_html__( '6', 'innox' ),
		)
	),
	'responsive_xs' => array(
		'label'   => esc_html__( 'Columns on extra small screens', 'innox' ),
		'desc'    => esc_html__( 'Select items number on extra small screens (<767px)', 'innox' ),
		'value'   => '1',
		'type'    => 'select',
		'choices' => array(
			'1' => esc_html__( '1', 'innox' ),
			'2' => esc_html__( '2', 'innox' ),
			'3' => esc_html__( '3', 'innox' ),
			'4' => esc_html__( '4', 'innox' ),
			'6' => esc_html__( '6', 'innox' ),
		)
	),
	'show_filters'  => array(
		'type'         => 'switch',
		'value'        => false,
		'label'        => esc_html__( 'Show filters', 'innox' ),
		'desc'         => esc_html__( 'Hide or show categories filters', 'innox' ),
		'left-choice'  => array(
			'value' => false,
			'label' => esc_html__( 'No', 'innox' ),
		),
		'right-choice' => array(
			'value' => true,
			'label' => esc_html__( 'Yes', 'innox' ),
		),
	),
	'cat' => array(
		'type'  => 'multi-select',
		'label' => esc_html__('Select categories', 'innox'),
		'desc'  => esc_html__('You can select one or more categories', 'innox'),
		'population' => 'taxonomy',
		'source' => 'fw-event-taxonomy-name',
		'prepopulate' => 10,
		'limit' => 100,
	)
);