<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Contact form', 'innox' ),
	'description' => esc_html__( 'Build contact forms', 'innox' ),
	'tab'         => esc_html__( 'Content Elements', 'innox' ),
	'popup_size'  => 'large',
	'type'        => 'special'
);