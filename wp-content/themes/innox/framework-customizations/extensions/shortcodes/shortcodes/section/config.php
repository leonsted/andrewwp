<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'tab'         => esc_html__( 'Layout Elements', 'innox' ),
		'title'       => esc_html__( 'Section', 'innox' ),
		'description' => esc_html__( 'Add a Section', 'innox' ),
		'type'        => 'section', // WARNING: Do not edit this
	)
);