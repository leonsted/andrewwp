/*
Theme Name: Consal - Agency & Corporate WordPress Theme
Author: CodersPoint
Version: 1.0.0
*/


(function($){
        "use strict";
        var $window = $(window),
			windowWidth = $window.width(),
			windowHeight =  $window.height(),
			headerHeight = $('header').height(),
			footerHeight = $('footer').height(),
			mainContentMinHeight = windowHeight - headerHeight - footerHeight;
			
		function setMainContentMinHeight(height){
			$('#content').css('min-height', height + 'px');
		}
		setMainContentMinHeight(mainContentMinHeight);
			
		$window.on('resize', function(){
			var windowWidth = $window.width(),
				windowHeight =  $window.height(),
				headerHeight = $('header').height(),
				footerHeight = $('footer').height(),
				mainContentMinHeight = windowHeight - headerHeight - footerHeight;
				
			setMainContentMinHeight(mainContentMinHeight);
		});
			
			
        /* mobile menu */
        $('.navbar-toggle').on('click',function(){
          $(this).toggleClass('open');
        });
        function mobileMenu(selector,dropdown){
            $(selector + '> a').on('click', function(){
                $(this).siblings(dropdown).slideToggle();
                $(dropdown).not($(this).siblings(dropdown)).slideUp();
                $('.hk_dropdwon').not($(this).siblings('.hk_dropdwon')).slideUp();
                $('.hk_megamenu').not($(this).siblings('.hk_megamenu')).slideUp();
            }).removeAttr('href');
        }
        if(windowWidth < 768){
            $('.dropdwon').hide();
            mobileMenu('.has_dropdown','.dropdwon');
        }


        /* Login modal js*/
        $('.login_modal_wrapper').hide();
        $('.hk_login_modal > a').on('click',function(){
            $('.login_modal_wrapper').fadeToggle(500);
        });

        /* accordion jquery */
        $('.panel-title > a').on('click',function(){
            $(this).parents('.panel-heading').toggleClass('active');
            $('.panel-heading').not($(this).parents('.panel-heading')).removeClass('active');
            $(this).children('span.fa').toggleClass('fa-minus fa-plus');
            $('.panel-title span.fa').not($(this).children('span.fa')).removeClass('fa-minus').addClass('fa-plus');
        });

        /*========= all sliders js =========*/        
       

        $(".testimonial_slider").owlCarousel({
            loop:       false,
            margin:     0,
            nav:        true,
            autoplay:   false,
            dots:       false,
            navText : 
                [ 
                    '<i class="fa fa-angle-left" aria-hidden="true"></i>', 
                    '<i class="fa fa-angle-right" aria-hidden="true"></i>' 
                ],
            responsive: {
                0:{
                    items:1
                },
                767:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        });

        /* PARTNERS CAROUSEL*/
        $('.partner_carousel').owlCarousel({
            loop:true,
            margin: 30,
            nav: false,
            autoplay: consal_partnerscroll,
            dots: false,
            responsive:{
                0:{
                    items:2
                },
                767:{
                    items:4
                },
                1000:{
                    items:consal_partnernumber
                }
            }
        });

        /* RECENT POST CAROUSEL */
        var relatedProject = $('.related_project_slider');
        relatedProject.owlCarousel({
            loop:false,
            margin: 30,
            nav: false,
            autoplay: true,
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                767:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        });

        /* related project custom trigger */
        $('.related_project_slider_wrapper .prev').on('click', function() {
            relatedProject.trigger('prev.owl.carousel');
        });
        $('.related_project_slider_wrapper .next').on('click', function() {
            relatedProject.trigger('next.owl.carousel');
        });

        /* venobox js */
        $('.venobox').venobox({
            numeratio: true,
            spinner: 'cube-grid'
        });
        /* pricing table js*/
        var pricingTable = $('.hk_single_price_table');
        pricingTable.on('mouseover',function(){
            pricingTable.removeClass('active');
        });
        pricingTable.on('mouseleave',function(){
            $('.consal').addClass('active');
        });

        /* preloader js */
        $window.on('load', function(){
            $('.preloader').fadeOut(500);
            $('.preloader-bg').delay('500').fadeOut(1000);

            /*isotope and packery*/
            $('.portfolio_wrapper').isotope({
                layoutMode: 'packery',
                itemSelector: '.grid_item',
            });

            /*portfolio sorting*/
            $('.filter_list').on( 'click', 'li', function() {
                var filterValue = $(this).attr('data-filter');
                $('.portfolio_wrapper').isotope({ filter: filterValue });
                $(this).addClass('active');
                $('.filter_list li').not(this).removeClass('active');
            });
			



			// Replace search button
			$('input.consal-search-submit')
			.replaceWith('<button class="blog_search_btn"><i class="fa fa-search"></i></button>');
			
			$('input.wysija-submit.wysija-submit-field')
			.replaceWith('<button class="wysija-submit wysija-submit-field" type="submit"><i class="fa fa-envelope"></i></button>');
        });

	


    /*--------------------------------------------------------*/
    /*    Styling Section Title with jquery   */
    /*--------------------------------------------------------*/


    jQuery('.title').each( function(){

      var title = jQuery(this).html();

      var title = title.replace('|', '<span>');
      var title = title.replace('|', '</span>');

      jQuery(this).html(title);

    });



})(jQuery);
