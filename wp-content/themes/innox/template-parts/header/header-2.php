<?php
/**
 * The template part for selected header
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$header_variant  = function_exists( 'fw_get_db_customizer_option' ) ? fw_get_db_customizer_option( 'header' )['header_var'] : '';
if ( function_exists('fw_get_db_post_option') ) {
	$page_header = fw_get_db_post_option( $post->ID, 'header' );
	$header_variant = '2' === $page_header ? $page_header : $header_variant;
}


$social_icons_list    = function_exists( 'fw_get_db_customizer_option' ) ? fw_get_db_customizer_option( 'social_icons' ) : '';
$social_icons_wrapper    = function_exists( 'fw_get_db_customizer_option' ) ? fw_get_db_customizer_option( 'icons_wrapper_class' ) : '';
$social_icons_options   = array( 'social_icons' => $social_icons_list, 'icons_wrapper_class' => $social_icons_wrapper );
$toplogo_teasers = function_exists( 'fw_get_db_customizer_option' ) ? fw_get_db_customizer_option( 'header' )[ $header_variant ]['topline_combined_teasers'] : '';
$menu_text_align = ( !empty( $social_icons_list ) ) ? 'text-center' : 'text-right';
$header_logo_additional_class = '';
$header_teasers_additional_class = 'hidden-xxs';
$header_menu_toggler_position = 'toggler_xxs_right';
$header_menu_toggler_visibility = '';

if ( !empty( $toplogo_teasers ) || !empty( $social_icons_list ) ) {
	//display logo and teasers in header if no top logo section
	$header_logo_additional_class = 'affix-visible hidden-xs';
	$header_teasers_additional_class = 'affix-visible hidden-xs';

	//menu toggler position if no top logo section
	$header_menu_toggler_position = '';
	$header_menu_toggler_visibility = 'hidden-xs';
}

//slider before header
$show_slider = function_exists( 'fw_get_db_customizer_option' ) ? fw_get_db_customizer_option( 'header' )['2']['header_slider']['slider_before_header'] === 'before_header' : false;
$toplogo_slider_overlap = function_exists( 'fw_get_db_customizer_option' ) ? fw_get_db_customizer_option( 'header' )['2']['header_slider']['before_header']['toplogo_overlap'] : false;
$slider_id = fw_get_db_post_option( get_the_ID(), 'slider_id', false );

?>

<?php //display top logo section only if teasers or social icons not empty
if ( !empty( $toplogo_teasers ) || !empty( $social_icons_list ) ) : ?>
	<?php if ( $show_slider && $toplogo_slider_overlap && $slider_id ) : ?>
		<div class="transparent_wrapper">
	<?php endif; ?>
	<section class="page_toplogo toplogo1 cs dark_divided toggler_right">
		<div class="container-fluid">
			<div class="row flex-wrap v-center">
				<div class="col-xs-12 col-sm-6">
					<?php get_template_part( 'template-parts/header/header-logo-dark' ); ?>

					<!-- header toggler -->
					<span class="toggle_menu visible-xs"><span></span></span>
				</div>
				<div class="col-xs-12 col-sm-6 text-right hidden-xs">
					<div class="inline-content big-spacing">
						<?php foreach ( $toplogo_teasers as $teaser ) : ?>
							<span class="small-text grey greylinks">
                            <?php if ( $teaser['teaser_icon']['type'] !== 'none' ) : ?>
	                            <span class="teaser_icon small main_bg_color round">
                                <?php if ( $teaser['teaser_icon']['type'] === 'icon-font' ) : ?>
	                                <i class="<?php echo esc_attr( $teaser['teaser_icon']['icon-class'] ); ?>"></i>
                                <?php else:
	                                echo wp_get_attachment_image( $teaser['teaser_icon']['attachment-id'] );
                                endif; ?>
                                </span>
                            <?php endif; ?>
								<?php if ( $teaser['teaser_text_link'] ) : ?>
								<a href="<?php echo esc_url( $teaser['teaser_text_link'] ) ?>">
                            <?php endif;
                            echo wp_kses_post( $teaser['teaser_text'] );
                            if ( $teaser['teaser_text_link'] ) : ?>
                                </a>
							<?php endif; ?>
                        </span>
						<?php endforeach; ?>

						<?php if ( ! empty( $social_icons_list ) ) : ?>
							<div>
								<?php
								//get icons-social shortcode to render icons in team member item
								$shortcodes_extension = fw()->extensions->get( 'shortcodes' );
								if ( ! empty( $shortcodes_extension ) ) {
									echo fw_ext( 'shortcodes' )->get_shortcode( 'icons_social' )->render( $social_icons_options );
								}
								?>
							</div>
						<?php endif; //social icons ?>

					</div>
				</div>
			</div>
		</div>
	</section>
	<?php if ( $show_slider && $toplogo_slider_overlap && $slider_id ) : ?>
		</div>
	<?php endif; //top logo overlap ?>
<?php endif; //top logo teasers or social icons ?>

<?php if ( $show_slider ) {
	do_action( 'innox_slider' );
} ?>

<header class="page_header wide_menu header_white <?php echo esc_attr( $header_menu_toggler_position ); ?>">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 display-flex v-center">
				<div class="header_left_logo <?php echo esc_attr( $header_logo_additional_class ); ?>">
					<?php get_template_part( 'template-parts/header/header-logo' ); ?>
				</div>

				<div class="header_mainmenu <?php echo esc_attr( $menu_text_align ); ?>">
					<div class="mainmenu_wrapper primary-navigation">
						<?php wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'     => 'sf-menu nav-menu nav',
							'container'      => 'ul'
						) ); ?>
					</div>
					<!-- header toggler -->
					<span class="toggle_menu <?php echo esc_attr( $header_menu_toggler_visibility); ?>"><span></span></span>
				</div>

				<?php if ( ! empty( $social_icons_list ) ) : ?>
					<div class="header_right_buttons small-text grey darklinks <?php echo esc_attr( $header_teasers_additional_class ); ?>">
						<?php if ( ! empty( $social_icons_list ) ) : ?>
                            <?php
                            //get icons-social shortcode to render icons in team member item
                            $shortcodes_extension = fw()->extensions->get( 'shortcodes' );
                            if ( ! empty( $shortcodes_extension ) ) {
                                echo fw_ext( 'shortcodes' )->get_shortcode( 'icons_social' )->render( $social_icons_options );
                            }
                            ?>
						<?php endif; //social icons ?>
					</div>
				<?php endif; ?>

			</div>
		</div>
	</div>
</header>