<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'box_id' => array(
		'type'    => 'box',
		'title'   => esc_html__( 'Options for child categories', 'innox' ),
		'options' => array(
			'layout'        => array(
				'label'   => esc_html__( 'Portfolio Layout', 'innox' ),
				'desc'    => esc_html__( 'Choose projects layout', 'innox' ),
				'value'   => 'isotope',
				'type'    => 'select',
				'choices' => array(
					'carousel' => esc_html__( 'Carousel', 'innox' ),
					'isotope'  => esc_html__( 'Masonry Grid', 'innox' ),
				)
			),
			'item_layout'   => array(
				'label'   => esc_html__( 'Item layout', 'innox' ),
				'desc'    => esc_html__( 'Choose Item layout', 'innox' ),
				'value'   => 'item-regular',
				'type'    => 'select',
				'choices' => array(
					'item-regular'  => esc_html__( 'Regular (just image)', 'innox' ),
					'item-title'    => esc_html__( 'Image with title', 'innox' ),
					'item-extended' => esc_html__( 'Image with title and excerpt', 'innox' ),
				)
			),
			'full_width'    => array(
				'type'         => 'switch',
				'value'        => false,
				'label'        => esc_html__( 'Full width gallery', 'innox' ),
				'desc'         => esc_html__( 'Enable full width container for gallery', 'innox' ),
				'left-choice'  => array(
					'value' => false,
					'label' => esc_html__( 'No', 'innox' ),
				),
				'right-choice' => array(
					'value' => true,
					'label' => esc_html__( 'Yes', 'innox' ),
				),
			),
			'margin'        => array(
				'label'   => esc_html__( 'Horizontal item margin (px)', 'innox' ),
				'desc'    => esc_html__( 'Select horizontal item margin', 'innox' ),
				'value'   => '30',
				'type'    => 'select',
				'choices' => array(
					'0'  => esc_html__( '0', 'innox' ),
					'1'  => esc_html__( '1px', 'innox' ),
					'2'  => esc_html__( '2px', 'innox' ),
					'10' => esc_html__( '10px', 'innox' ),
					'30' => esc_html__( '30px', 'innox' ),
				)
			),
			'responsive_lg' => array(
				'label'   => esc_html__( 'Columns on large screens', 'innox' ),
				'desc'    => __( 'Select items number on wide screens (>1200px)', 'innox' ),
				'value'   => '4',
				'type'    => 'select',
				'choices' => array(
					'1' => esc_html__( '1', 'innox' ),
					'2' => esc_html__( '2', 'innox' ),
					'3' => esc_html__( '3', 'innox' ),
					'4' => esc_html__( '4', 'innox' ),
					'6' => esc_html__( '6', 'innox' ),
				)
			),
			'responsive_md' => array(
				'label'   => esc_html__( 'Columns on middle screens', 'innox' ),
				'desc'    => __( 'Select items number on middle screens (>992px)', 'innox' ),
				'value'   => '3',
				'type'    => 'select',
				'choices' => array(
					'1' => esc_html__( '1', 'innox' ),
					'2' => esc_html__( '2', 'innox' ),
					'3' => esc_html__( '3', 'innox' ),
					'4' => esc_html__( '4', 'innox' ),
					'6' => esc_html__( '6', 'innox' ),
				)
			),
			'responsive_sm' => array(
				'label'   => esc_html__( 'Columns on small screens', 'innox' ),
				'desc'    => __( 'Select items number on small screens (>768px)', 'innox' ),
				'value'   => '2',
				'type'    => 'select',
				'choices' => array(
					'1' => esc_html__( '1', 'innox' ),
					'2' => esc_html__( '2', 'innox' ),
					'3' => esc_html__( '3', 'innox' ),
					'4' => esc_html__( '4', 'innox' ),
					'6' => esc_html__( '6', 'innox' ),
				)
			),
			'responsive_xs' => array(
				'label'   => esc_html__( 'Columns on extra small screens', 'innox' ),
				'desc'    => esc_html__( 'Select items number on extra small screens (<767px)', 'innox' ),
				'value'   => '1',
				'type'    => 'select',
				'choices' => array(
					'1' => esc_html__( '1', 'innox' ),
					'2' => esc_html__( '2', 'innox' ),
					'3' => esc_html__( '3', 'innox' ),
					'4' => esc_html__( '4', 'innox' ),
					'6' => esc_html__( '6', 'innox' ),
				)
			),
			'show_filters'  => array(
				'type'         => 'switch',
				'value'        => false,
				'label'        => esc_html__( 'Show filters', 'innox' ),
				'desc'         => esc_html__( 'Hide or show categories filters', 'innox' ),
				'left-choice'  => array(
					'value' => false,
					'label' => esc_html__( 'No', 'innox' ),
				),
				'right-choice' => array(
					'value' => true,
					'label' => esc_html__( 'Yes', 'innox' ),
				),
			),
		)
	)
);