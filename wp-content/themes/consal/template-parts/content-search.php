<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */

?>
<div class="col-md-12 blog-post-single">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="row">
			<div class="single_blog_post clearfix">
				<div class="col-md-6">
					<?php if ( has_post_thumbnail() ) { ?>
					  <div class="blog_image">
						<?php the_post_thumbnail( 'post-thumbwide', array( 'class' => 'img-responsive' ) ); ?>
					  </div>
					<?php } ?>
				</div>
				<div class="col-md-6 blog_content">
					<div class="blog_title">
						<?php the_title( '<h3><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
					</div>
					<div class="meta-data">
						<p>
							<?php esc_html_e('by', 'consal'); ?> <?php the_author_posts_link(); ?>
							<span><?php echo esc_html(get_the_date()); ?></span>
							</a>
							<?php comments_popup_link( esc_html__('No Comment','consal'), esc_html__('1 Comment', 'consal'), esc_html__('% Comments','consal'), ' ', esc_html__('Comments off','consal')); ?>
						</p>
					</div>
					<div class="blog_text">
						<?php the_excerpt(); ?>
						<a href="<?php echo esc_url( get_permalink()); ?>" class="btn read_more"><?php consal_blog_read_more(); ?></a>
						<?php	wp_link_pages( array(
								'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'consal' ),
								'after'  => '</div>',
							) );
						?>
					</div>
				</div>
			</div>
		</div>
	</article><!-- #post-## -->
</div>
