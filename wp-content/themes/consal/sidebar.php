<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
$blogsidebar = 'right';
if(isset($consal_opt['sidebarblog_pos']) && $consal_opt['sidebarblog_pos']!=''){
	$blogsidebar = $consal_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$blogsidebar = $_GET['sidebar'];
}
?>
<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
	<div id="secondary" class="col-xs-12 col-md-3">
		<div class="shop-left-sidebar <?php echo esc_attr($blogsidebar);?>">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div>
	</div><!-- #secondary -->
<?php endif; ?>
