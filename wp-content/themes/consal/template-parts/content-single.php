<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */

?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="single_blog_contents reveal animated">
			<div class="single_blog_header">
				<?php if ( has_post_thumbnail() ) { ?>
				<div class="single_blog_img">
					<?php the_post_thumbnail( 'consal-post-thumb', array( 'class' => 'img-responsive' ) ); ?>
				</div>
				<?php } ?>
				<div class="blog_title_meta">
					<div class="meta-data">
						<p>
							<?php esc_html_e('by', 'consal'); ?> <?php the_author_posts_link(); ?>
							<span><?php echo esc_html(get_the_date()); ?></span>
							</a>
							<?php comments_popup_link( esc_html__('No Comment','consal'), esc_html__('1 Comment', 'consal'), esc_html__('% Comments','consal'), ' ', esc_html__('Comments off','consal')); ?>
						</p>
					</div>
					<h3><?php the_title(); ?></h3>
				</div>
			</div>

			<!-- single blog post -->
			<div class="single_blog_content">
				<?php the_content();
					wp_link_pages( array(
						'before'      => '<div class="pagination"><span class="page-links-title">' . esc_html__( 'Pages:', 'consal' ) . '</span>',
						'after'       => '</div>',
						'link_before' => '<span>',
						'link_after'  => '</span>',
					) );
				 ?>
				<div class="tag_share">
				  <!-- Blog post tsgs -->
					<?php if(has_tag()) { ?>
						<div class="tags">
							<ul>
								<li><i class="fa fa-tags"></i></li>
								<?php the_tags( '<li>', '</li> <li>', '</li>' ); ?>
							</ul>
						</div><!-- Blog post tags ends -->
					<?php } ?>
					<?php if( function_exists('consal_helper_plugin') ) { ?>
						<div class="social_icons">
							<?php consal_blog_sharing(); ?>
						</div>
					<?php } ?>	
					
				</div>
			</div>
		</div>
	</article><!-- #post-## -->
