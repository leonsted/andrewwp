<?php

/**
  ReduxFramework Sample Config File
  For full documentation, please visit: https://docs.reduxframework.com
 * */

if (!class_exists('Consal_Theme_Config')) {

    class Consal_Theme_Config {

        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if (!class_exists('ReduxFramework')) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if (  true == Redux_Helpers::isTheme(__FILE__) ) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }

        }

        public function initSettings() {

            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        /**

          This is a test function that will let you see when the compiler hook occurs.
          It only runs if a field	set with compiler=>true is changed.

         * */
        function compiler_action($options, $css, $changed_values) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r($changed_values); // Values that have changed since the last save
            echo "</pre>";
        }

        /**

          Custom function for filtering the sections array. Good for child themes to override or add to the sections.
          Simply include this function in the child themes functions.php file.

          NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
          so you must use get_template_directory_uri() if you want to use any of the built in icons

         * */
        function dynamic_section($sections) {
            //$sections = array();
            $sections[] = array(
                'title' => esc_html__('Section via hook', 'consal'),
                'desc' => esc_html__('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'consal'),
                'icon' => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }

        /**

          Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

         * */
        function change_arguments($args) {
            return $args;
        }

        /**

          Filter hook for filtering the default value of any given field. Very useful in development mode.

         * */
        function change_defaults($defaults) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }

        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
				// If Redux is running as a plugin, this will remove the demo notice and links
				add_action( 'redux/plugin/hooks', array( $this, 'remove_demo' ) );				
            }
        }

        public function setSections() {

            /**
              Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            // Background Patterns Reader
            $sample_patterns_path   = ReduxFramework::$_dir . '../sample/patterns/';
            $sample_patterns_url    = ReduxFramework::$_url . '../sample/patterns/';
            $sample_patterns        = array();

            ob_start();

            $ct             = wp_get_theme();
            $this->theme    = $ct;
            $item_name      = $this->theme->get('Name');
            $tags           = $this->theme->Tags;
            $screenshot     = $this->theme->get_screenshot();
            $class          = $screenshot ? 'has-screenshot' : '';

            $customize_title = sprintf(esc_html__('Customize &#8220;%s&#8221;', 'consal'), $this->theme->display('Name'));
            
            ?>
            <div id="current-theme" class="<?php echo esc_attr($class); ?>">
            <?php if ($screenshot) : ?>
                <?php if (current_user_can('edit_theme_options')) : ?>
                        <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
                            <img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview', 'consal'); ?>" />
                        </a>
                <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview', 'consal'); ?>" />
                <?php endif; ?>

                <h4><?php echo esc_attr($this->theme->display('Name')); ?></h4>

                <div>
                    <ul class="theme-info">
                        <li><?php printf(esc_html__('By %s', 'consal'), $this->theme->display('Author')); ?></li>
                        <li><?php printf(esc_html__('Version %s', 'consal'), $this->theme->display('Version')); ?></li>
                        <li><?php echo '<strong>' . esc_html__('Tags', 'consal') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo esc_attr($this->theme->display('Description')); ?></p>
                </div>
            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();
	
 		// General
		$this->sections[] = array(
			'title'     => esc_html__('General', 'consal'),
			'desc'      => esc_html__('Use this section to select for general theme options', 'consal'),
			'icon'      => 'el-icon-cog',
			'fields'    => array(
				array(
					'id'        => 'logo_main',
					'type'      => 'media',
					'title'     => esc_html__('Logo', 'consal'),
					'compiler'  => 'true',
					'mode'      => false,
					'desc'      => esc_html__('Standard logo size is 130x40 px', 'consal'),
				),
				array(
					'id'        => 'background_opt',
					'type'      => 'background',
					'output'    => array('body'),
					'title'     => esc_html__('Body Background', 'consal'),
					'subtitle'  => esc_html__('Body background with image, color.', 'consal'),
					'default'  => array(
						'background-color' => '#FFFFFF',
					)
				),
				array(
					'id'            => 'bodyfont',
					'type'          => 'typography',
					'title'         => esc_html__('Body font', 'consal'),
					'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
					'font-backup'   => true,    // Select a backup non-google font in addition to a google font
					'subsets'       => false, // Only appears if google is true and subsets not set to false
					'text-align'   => false,
					'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
					'output'        => array('body , p'), // An array of CSS selectors to apply this font style to dynamically
					'units'         => 'px', // Defaults to px
					'subtitle'      => esc_html__('Main body font.', 'consal'),
					'default'       => array(
						'color'         => '#777',
						'font-weight'    => '400',
						'font-family'   => 'Raleway',
						'google'        => true,
						'font-size'     => '14px',
						'line-height'   => '25px'),
				),
				array(
					'id'            => 'headingfont',
					'type'          => 'typography',
					'title'         => esc_html__('Heading font', 'consal'),
					'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
					'font-backup'   => false,    // Select a backup non-google font in addition to a google font
					'subsets'       => false, // Only appears if google is true and subsets not set to false
					'font-size'     => false,
					'line-height'   => false,
					'text-align'   => false,
					'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
					'output'        => array('h1, h2, h3, h4, h5, h6'), // An array of CSS selectors to apply this font style to dynamically
					'units'         => 'px', // Defaults to px
					'subtitle'      => esc_html__('Change heading ( h1, h2, h3, h4, h5, h6 ) font, color, weight.', 'consal'),
					'default'       => array(
						'color'         => '#333',
						'font-weight'    => '600',
						'font-family'   => 'Raleway',
						'google'        => true,
					),
				)				
			),
		);
		
		// Colors
		$this->sections[] = array(
			'title'     => esc_html__('Colors', 'consal'),
			'desc'      => esc_html__('Use this section to select options for theme color', 'consal'),
			'icon'      => 'el-icon-tint',
			'fields'    => array(	
				array(         
					'id'       => 'opt-background',
					'type'     => 'background',
					'output'    => array('
                        .about_us_title h1:before,
                        .btn_colored,
                        .section_title .title::before,
                        .filter_list li span::before,
                        .filter_list li span::after,
                        .progress-bar,
                        .slider_navigatiors span:hover,
                        .team_img:before,
                        .comments-area .comment-respond .comment-form #submit,
                        input[type="submit"], 
                        botton,
                        .portfolio_item_top_layer .content_wrapper .links a,
                        .filter_list li span::before, 
                        .filter_list li span::after,
                        .single_blog_post .blog_tag,.page-numbers > li > a:hover,
                        .page-numbers > li .current,.page-numbers > li > a,
                        .page-numbers > li > span,
                        .page-content .vc_tta-color-grey.vc_tta-style-classic .vc_tta-panel.vc_active .vc_tta-panel-heading,
                        .post_date,
                        .social_icons li a:before,
                        .hero_btn,
                        .about_us_image:before,
                        .about_us_image:after'
                    ),
					'title'     => esc_html__('Primary Background Color', 'consal'),
                    'transparent' => false,
                    'background-image'      => false,
                    'background-repeat'     => false,
                    'background-attachment' => false,
                    'background-position'   => false,
                    'background-size'       => false,
					'default'  => array(
						'background-color' => '#77be2d',
					)
				),			
				array(
					'id'        => 'primary_color',
					'type'      => 'color',
					'output'    => array('
                        .contact_email ul li span i,
                        .social_icons li a,
                        .highlighted,
                        .portfolio_filter ul li span:hover, 
                        .portfolio_filter ul li.active span,
                        .work_point ul li .icon,
                        .name_desig .name,
                        .testimonial_text:before,
                        .contact_address li span,
                        .hover_overlay .social_links ul li a:hover,
                        .footer_widgets ul li a:hover,
                        .about_us_btn .consal_btn:hover,
                        .huge_text > p,
                        .blog_title_meta .meta-data p a:hover,
                        .blog_title h2:hover a,
                        .footer_social ul li a:hover,
                        .footer_text a:hover,
                        .btn_colored:hover,
                        .consal_btn.btn_colored:hover,
                        .promo_btn:hover,
                        .tag_share .tags ul li:first-child i,
                        .comments-area .comment-respond .comment-form #submit:hover,
                        .service_title h4 a:hover,
                        .service_title h4 a:hover,
                        aside.widget ul li a:hover,
                        .widget .catagory_list a:hover, 
                        .widget .catagory_list a:hover span,
                        a:hover, 
                        a:focus, 
                        a:active,
                        a,
                        .blog_search input:focus,
                        .blog_search .blog_search_btn:hover'
                    ),
					'title'     => esc_html__('Primary Color', 'consal'),
					'subtitle'  => esc_html__('Pick a color for primary color (default: #77be2d).', 'consal'),
					'transparent' => false,
					'validate'  => 'color',
					'default'  => '#77be2d',
				),
				array( 
					'id'       => 'primary_border',
					'type'     => 'border',
					'title'    => __('Primary border', 'consal'),
					'subtitle' => __('Pick a border for primary border (default: #77be2d ).', 'consal'),
					'output'   => array('
                        .slider_navigatiors span:hover,
                        .about_us_btn .consal_btn:hover,
                        .work_point ul li .icon,
                        .btn_colored,
                        .page-numbers > li > a:hover, 
                        .page-numbers > li .current,
                        .page_not_found .consal_btn:hover,
                        .comment-respond input:focus, 
                        .comment-respond textarea:focus,
                        .comments-area .comment-respond .comment-form #submit:hover,
                        .blog_search input:focus'
                    ),
					'default'  => array(
						'border-color'  => '#77be2d', 
						'border-style'  => 'solid', 
						'border-top'    => '1px', 
						'border-right'  => '1px', 
						'border-bottom' => '1px', 
						'border-left'   => '1px'
					)
				)			
			),
		);
			
		//Header
		$this->sections[] = array(
			'title'     => esc_html__('Header', 'consal'),
			'desc'      => esc_html__('Use this section to select for header options', 'consal'),
			'icon'      => 'el-icon-home-alt',
		);
			
		//Header Menu
		$this->sections[] = array(
			'title'     => esc_html__('Menu', 'consal'),
			'desc'      => esc_html__('Menu options', 'consal'),
			'icon'      => 'el-icon-tasks',
			'subsection' => true,
			'fields'    => array(
				array(
					'id'        => 'background_menu',
					'type'      => 'background',
					'output'    => array('.navbar-default,.menu-item-has-children ul.sub-menu, .has_mehgamenu .megamenu'),
					'title'     => esc_html__('Menu Background', 'consal'),
                    'transparent' => false,
                    'background-image'      => false,
                    'background-repeat'     => false,
                    'background-attachment' => false,
                    'background-position'   => false,
                    'background-size'       => false,
					'default'  => array(                   
						'background-color' => '#fff',
					)
				),
				array(
					'id'            => 'menufont',
					'type'          => 'typography',
					'title'         => esc_html__('Menu font', 'consal'),
					'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
					'font-backup'   => false,    // Select a backup non-google font in addition to a google font
					'subsets'       => false, // Only appears if google is true and subsets not set to false
					'line-height'   => false,
					'transition'   => false,
					'text-align'   => false,
					'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
					'output'        => array('.mainmenu .navbar-nav li > a'), // An array of CSS selectors to apply this font style to dynamically
					'units'         => 'px', // Defaults to px
					'subtitle'      => esc_html__('Change menu font, color, size etc.', 'consal'),
					'default'       => array(
						'color'			=> '#333',
						'font-weight'    => '600',
						'font-family'   => 'Raleway',
						'font-size'     => '14px',
						'google'        => true,
					),
				),
				array(
					'id'       => 'menu_hover_color',
					'type'     => 'color',
					'output'    => array('.navbar-default .navbar-nav > li > a:focus, .navbar-default .navbar-nav > li > a:hover,.menu-item-has-children ul.sub-menu li a:hover,.menu-item-has-children ul.sub-menu li.current-menu-item a,.mainmenu .navbar-nav > li.current-menu-item > a'),
					'title'    => esc_html__('Menu Hover Color', 'consal'), 
					'subtitle' => esc_html__('Pick a color for the Menu Hover.', 'consal'),
					'default'  => '#77be2d',
					'validate' => 'color',
                    'transparent' => false,
				)
			),
		);
			
		//Header Top
		$this->sections[] = array(
			'title'     => esc_html__('Header Top Bar', 'consal'),
			'desc'      => esc_html__('Use this section to select options for header top bar.', 'consal'),
			'icon'      => 'el-icon-ok',
			'subsection'=> true,
			'fields'    => array(			
				array(
					'id'        => 'show_header_top_bar',
					'type'     => 'switch',
					'title'    => esc_html__('Header Top Bar', 'consal'),
					'subtitle'	=> esc_html__('Show & Hide header top bar.', 'consal'),
					'default'  => false,
				),
				array(
					'id'        => 'background_top_header',
					'type'      => 'background',
					'output'    => array('.tiny_header'),
					'title'     => esc_html__('Topbar Background Color', 'consal'),
                    'transparent' => false,
                    'background-image'      => false,
                    'background-repeat'     => false,
                    'background-attachment' => false,
                    'background-position'   => false,
                    'background-size'       => false,
					'default'   => array (
                            'background-color'  =>  '#555'
                        )
				),
				array(
					'id'       => 'topbar_color',
					'type'     => 'color',
					'output'    => array('.contact_email ul li a, .contact_email ul li span'),
					'title'    => esc_html__('Topbar Text Color', 'consal'), 
					'subtitle' => esc_html__('', 'consal'),
                    'transparent' => false,
					'default'  => '#fff',
					'validate' => 'color',
				),
				array(
					'id'        => 'top_bar_phone',
					'type'      => 'text',
					'title'     => esc_html__('Phone Number', 'consal'),
					'subtitle'	=> esc_html__('Enter phone number at top bar.', 'consal'),
					'default'   => esc_html__('0123-1234-56789', 'consal'),
				),
				array(
					'id'        => 'top_bar_email',
					'type'      => 'text',
					'title'     => esc_html__('Email Address', 'consal'),
					'subtitle'	=> esc_html__('Enter email address at top bar.', 'consal'),
					'default'   => esc_html__('admin@yourdomain.com', 'consal'),
				),
				array(
					'id'       => 'topbar_social_icons',
					'type'     => 'sortable',
					'title'    => esc_html__('Social Icons', 'consal'),
					'subtitle' => esc_html__('Enter social links at top bar.', 'consal'),
					'desc'     => esc_html__('Drag/drop to re-arrange', 'consal'),
					'mode'     => 'text',
					'options'  => array(
						'facebook'     => 'facebook',
						'twitter'     => 'twitter',
						'instagram'	=> 'instagram',
						'tumblr'     => 'tumblr',
						'pinterest'     => 'pinterest',
						'google-plus'     => 'google-plus',
						'linkedin'     => 'linkedin',
						'behance'     => 'behance',
						'dribbble'     => 'dribbble',
						'youtube'     => 'youtube',
						'vimeo'     => 'vimeo',
						'rss'     => 'rss',
					),
					'default' => array(
						'facebook'		=> '',
						'twitter'		=> '',
						'pinterest'     => '',
						'google-plus'   => '',
						'linkedin'   	=> '',
                        'instagram'     => '',
                        'tumblr'        => '',
						'behance'     	=> '',
						'dribbble'    	=> '',
						'youtube'   	=> '',
						'vimeo'    		=> '',
						'rss'    		=> '',
					),
				)
			),
		);

		//Footer
		$this->sections[] = array(
			'title'     => esc_html__('Footer', 'consal'),
			'desc'      => esc_html__('Use this section to select options for footer options', 'consal'),
			'icon'      => 'el-icon-photo',
			'fields'    => array(
				array(
					'id'        => 'background_footer_color',
					'type'      => 'background',
					'output'    => array('.footer:before'),
					'title'     => esc_html__('Footer Background', 'consal'),
					'subtitle'  => esc_html__('Footer background color.', 'consal'),
                    'transparent' => false,
					'background-image' => false,
					'background-repeat' => false,
					'background-attachment' => false,
					'background-position' => false,
					'background-size' => false,
					'default'  => array(
						'background-color' => '#333',
						
					)
				),
				array(
					'id'        => 'background_footer',
					'type'      => 'background',
					'output'    => array('footer.footer'),
					'title'     => esc_html__('Footer Background', 'consal'),
					'subtitle'  => esc_html__('Header background with image.', 'consal'),
					'background-color' => false,
				)
			),
		);

		//Footer Copyright
		$this->sections[] = array(
			'title'     => esc_html__('Copyright', 'consal'),
			'desc'      => esc_html__('Use this section to select options for copyright.', 'consal'),
			'icon'      => 'el-icon-brush',
			'subsection'=> true,
			'fields'    => array(
				array(
					'id'        => 'background_footer_copyright',
					'type'      => 'background',
					'output'    => array('.tiny_footer'),
					'title'     => esc_html__('Footer Copyright Background', 'consal'),
					'subtitle'  => esc_html__('Footer Copyright background color with image.', 'consal'),
                    'transparent' => false,
					'default'  => array(
						'background-color' => '#333',
						
					)
				),
				array(
					'id'       => 'copyrightolor',
					'type'     => 'color',
					'output'    => array('.tiny_footer p,.tiny_footer p a,.footer_social ul li a'),
					'title'    => esc_html__('Copyright Color', 'consal'), 
					'subtitle' => esc_html__('Pick a color for the Copyright (default: #EEE).', 'consal'),
					'default'  => '#EEE',
					'validate' => 'color',
                    'transparent' => false,
				),				
				array(
					'id'      	=> 'copyright',
					'type'    	=> 'editor',
					'title'   	=> esc_html__('Copyright information', 'consal'),
					'subtitle'	=> esc_html__('HTML tags allowed: a, br, em, strong', 'consal'),
					'default'	=> esc_html__('Copyright &copy; 2017. Designed by ' , 'consal')." <a href='".esc_url('coderspoint.net', 'consal')."' target='_blank'>".esc_html__('CodersPoint.' , 'consal')."</a> ",
					'args' 		=> array(
						'teeny'            => true,
						'textarea_rows'    => 5,
						'media_buttons'	=> false,
					)
				),
				array(
					'id'       => 'footer_social_icons',
					'type'     => 'sortable',
					'title'    => esc_html__('Social Icons', 'consal'),
					'subtitle' => esc_html__('Enter social links at footer.', 'consal'),
					'desc'     => esc_html__('Drag/drop to re-arrange', 'consal'),
					'mode'     => 'text',
					'options'  => array(
						'facebook'     => 'facebook',
						'twitter'     => 'twitter',
						'instagram'	=> 'instagram',
						'tumblr'     => 'tumblr',
						'pinterest'     => 'pinterest',
						'google-plus'     => 'google-plus',
						'linkedin'     => 'linkedin',
						'behance'     => 'behance',
						'dribbble'     => 'dribbble',
						'youtube'     => 'youtube',
						'vimeo'     => 'vimeo',
						'rss'     => 'rss',
					),
					'default' => array(
						'facebook'		=> '',
						'twitter'		=> '',
						'instagram'		=> '',
						'tumblr'     	=> '',
						'pinterest'     => '',
						'google-plus'   => '',
						'linkedin'   	=> '',
						'behance'     	=> '',
						'dribbble'    	=> '',
						'youtube'   	=> '',
						'vimeo'    		=> '',
						'rss'    		=> '',
					),
				),

			),
		);
	
		// Sidebar
		$this->sections[] = array(
			'title'     => esc_html__('Sidebar', 'consal'),
			'desc'      => esc_html__('Use this section to select for Sidebar options', 'consal'),
			'icon'      => 'el-icon-th-large',
			'fields'    => array(
				array(
					'id'        => 'blog_layout',
					'type'      => 'select',
					'title'     => esc_html__('Blog Layout', 'consal'),
					'customizer_only'   => false,
					'options'   => array(
						'nosidebar' => esc_html__('No Sidebar', 'consal'),
						'sidebar' => esc_html__('Sidebar', 'consal'),
					),
					'default'   => esc_html__('sidebar', 'consal')
				),
				array(
					'id'       => 'sidebarblog_pos',
					'type'     => 'radio',
					'title'    => esc_html__('Blog Sidebar Position', 'consal'),
					'subtitle' => esc_html__('Sidebar on Blog pages', 'consal'),
					'options'  => array(
						'left' => esc_html__('Left', 'consal'),
						'right' => esc_html__('Right', 'consal')
					),
					'default'  => esc_html__('right', 'consal')
				),
				array(
					'id'        => 'page_layout',
					'type'      => 'select',
					'title'     => esc_html__('Page Layout', 'consal'),
					'customizer_only'   => false,
					'options'   => array(
						'nosidebar' => esc_html__('No Sidebar', 'consal'),
						'sidebar' => esc_html__('Sidebar', 'consal'),
					),
					'default'   => esc_html__('nosidebar', 'consal')
				),
				array(
					'id'       => 'sidebarsepage_pos',
					'type'     => 'radio',
					'title'    => esc_html__('Pages Sidebar Position', 'consal'),
					'subtitle' => esc_html__('Sidebar on pages', 'consal'),
					'options'  => array(
						'left' => esc_html__('Left', 'consal'),
						'right' => esc_html__('Right', 'consal'),
					),
					'default'  => esc_html__('left', 'consal'),
				),			
			),
		);
	
		// Blog options
		$this->sections[] = array(
			'title'     => esc_html__('Blog', 'consal'),
			'desc'      => esc_html__('Use this section to select options for blog', 'consal'),
			'icon'      => 'el-icon-italic',
			'fields'    => array(
				array(
					'id'       => 'breadcrumb_on_off_blog',
					'type'     => 'switch',
					'title'    => esc_html__('Breadcrumb Switch', 'consal'),
					'subtitle'  => esc_html__('Breadcrumb show & hide switch', 'consal'),
					'default'  => true,
				),
				array(
					'id'        => 'breadcrumb_bg_blog',
					'type'      => 'background',
					'output'    => array('.breadcrumb.blog-bdcmb'),
					'title'     => esc_html__('Breadcrumb Background Image', 'consal'),
					'subtitle'  => esc_html__('Pick a breadcrumb background with image and color.', 'consal'),
					'background-color'	=> false
				),
				array(
					'id'        => 'readmore_text',
					'type'      => 'text',
					'title'     => esc_html__('Read more text', 'consal'),
					'default'   => esc_html__('Read More ...', 'consal')
				),
				array(
					'id'        => 'excerpt_length',
					'type'      => 'slider',
					'title'     => esc_html__('Excerpt length on blog page', 'consal'),
					"default"   => 22,
					"min"       => 10,
					"step"      => 2,
					"max"       => 120,
					'display_value' => 'text'
				)
			),
		);

		// Pages
		$this->sections[] = array(
			'title'     => esc_html__('Pages', 'consal'),
			'desc'      => esc_html__('Use this section to select options for pages options', 'consal'),
			'icon'      => 'el-icon-cog',
			'fields'    => array(
				array(
					'id'       => 'breadcrumb_on_off',
					'type'     => 'switch',
					'title'    => esc_html__('Breadcrumb Switch', 'consal'),
					'subtitle'  => esc_html__('Breadcrumb show & hide switch', 'consal'),
					'default'  => true,
				),
				array(
					'id'        => 'breadcrumb_bg_page',
					'type'      => 'background',
					'output'    => array('.breadcrumb.pages_bdcmb'),
					'title'     => esc_html__('Breadcrumb Background Image', 'consal'),
					'subtitle'  => esc_html__('Pick a breadcrumb background with image and color.', 'consal'),
					'background-color'	=> false
				)
			),
		);
		
		//Partner logos
		$this->sections[] = array(
			'title'     => esc_html__('Partner Logos', 'consal'),
			'desc'      => esc_html__('Upload partner logos and links.', 'consal'),
			'icon'      => 'el-icon-th',
			'fields'    => array(
				array(
					'id'        => 'partnernumber',
					'type'      => 'slider',
					'title'     => esc_html__('Number of partner logo per page', 'consal'),
					'desc'      => esc_html__('Number of partner logos per page, default value: 6', 'consal'),
					"default"   => 6,
					"min"       => 1,
					"step"      => 1,
					"max"       => 12,
					'display_value' => 'text'
				),
				array(
					'id'       => 'partnerscroll',
					'type'     => 'switch',
					'title'    => esc_html__('Auto scroll', 'consal'),
					'default'  => true,
				),
				array(
					'id'          => 'partner_logos',
					'type'        => 'slides',
					'title'       => esc_html__('Logos', 'consal'),
					'desc'        => esc_html__('Upload logo image and enter logo link.', 'consal'),
					'placeholder' => array(
						'title'           => esc_html__('Title', 'consal'),
						'description'     => esc_html__('Description', 'consal'),
						'url'             => esc_html__('Link', 'consal'),
					),
				),
			),
        );
		
		//Testimonial Page
		$this->sections[] = array(
			'title'     => esc_html__('Testimonial Page', 'consal'),
			'desc'      => esc_html__('Testimonial Page Setting', 'consal'),
			'icon'      => 'el-icon-align-center',
			'fields'    => array(
				array(
					'id'        => 'testimonialnumber',
					'type'      => 'slider',
					'title'     => esc_html__('Number of post per page', 'consal'),
					'desc'      => esc_html__('Number of post per page, default value: 8', 'consal'),
					"default"   => 8,
					"min"       => 1,
					"step"      => 1,
					"max"       => 12,
					'display_value' => 'text'
				),
				array(
					'id'       => 'enable_partner_slider',
					'type'     => 'switch',
					'title'    => esc_html__('Show/Hide Partner Slider', 'consal'),
					'default'  => true,
				),
			),
        );
		

		// Single Service & Projects
        
		$this->sections[] = array(
			'title'     => esc_html__('Single Service & Projects', 'consal'),
			'desc'      => esc_html__('Single Service & Projects Setting', 'consal'),
			'icon'      => 'el el-tasks',
			'fields'    => array(
                array(
                    'id'       => 'show_rel_service',
                    'type'     => 'switch',
                    'title'    => esc_html__('Show Related Services', 'consal'),
                    'subtitle' => esc_html__('Show Related Services in Singel Service Page', 'consal'),
                    'default'  => true,
                ),
                array(
                    'id'       => 'rel_ser_title',
                    'type'     => 'text',
                    'title'    => esc_html__('Related Service Title', 'consal'),
                    'desc'     => esc_html__('Write Related Service Title here', 'consal'),
                    'default'  => esc_html__('Related Services', 'consal')
                ),
                array(
                    'id'       => 'show_rel_project',
                    'type'     => 'switch',
                    'title'    => esc_html__('Show Related Projects', 'consal'),
                    'subtitle' => esc_html__('Show Related Projects in Singel Project Page', 'consal'),
                    'default'  => true,
                ),
				array(
					'id'       => 'rel_project_title',
					'type'     => 'text',
					'title'    => esc_html__('Related Project Title', 'consal'),
					'desc'     => esc_html__('Write Related Project Title here', 'consal'),
					'default'  => esc_html__('Related Projects', 'consal')
				)
			),
        );
		


		//404 Not Found Page
		$this->sections[] = array(
			'title'     => esc_html__('404 Error Page', 'consal'),
			'desc'      => esc_html__('Not Found Page Setting', 'consal'),
			'icon'      => 'el-icon-error-alt',
			'fields'    => array(
				array(
					'id'       => 'breadcrumb_on_off_error',
					'type'     => 'switch',
					'title'    => esc_html__('Breadcrumb Switch', 'consal'),
					'subtitle'  => esc_html__('Breadcrumb show & hide switch', 'consal'),
					'default'  => true,
				),
				array(
					'id'        => 'breadcrumb_bg_error',
					'type'      => 'background',
					'output'    => array('.breadcrumb.error-page'),
					'title'     => esc_html__('Breadcrumb Background Image', 'consal'),
					'subtitle'  => esc_html__('Pick a breadcrumb background with image and color.', 'consal'),
					'background-color'	=> false
				),
				array(
					'id'        => 'notfound_title',
					'type'      => 'text',
					'title'     => esc_html__('404 Not Found Title', 'consal'),
					'default'  	=> esc_html__('Oops! That page can\'t be found.', 'consal'),
				),
				array(
					'id'       => 'notfound_content',
					'type'     => 'text',
					'title'    => esc_html__('404 Not Found Content', 'consal'),
					'default'  => esc_html__('It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'consal'),
				),
				array(
					'id'       => 'enable_consal_cta',
					'type'     => 'switch',
					'title'    => esc_html__('Show/Hide Call to Action', 'consal'),
					'desc'     => esc_html__('Show/Hide call to action section from 404 error page', 'consal'),
					'default'  => true,
				),
			),
        );
			
		// Contact Map
        $this->sections[] = array(
			'title'     => esc_html__('Google Map', 'consal'),
			'desc'      => esc_html__('Use this section to select options for Google Map on contact page', 'consal'),
			'icon'      => 'el-icon-map-marker',
			'fields'    => array(
				array(
					'id'        => 'map_apy_key',
					'type'      => 'text',
					'title'     => esc_html__('Google API Key', 'consal'),
					'subtitle'	=>  '<a target="_blank" href="'.esc_url('https://developers.google.com/maps/documentation/javascript/get-api-key', 'consal').'">'.esc_html__('Click Here', 'consal').'</a> '.esc_html__('to Get a Google Maps API KEY to Get a Google Maps API KEY', 'consal').'', 
				),
				array(
					'id'               => 'map_desc',
					'type'             => 'editor',
					'title'    => esc_html__('Map description', 'consal'),
					'subtitle' => esc_html__('The text on map popup', 'consal'),
					'default'  => esc_html__('Consal - Agency & Corporate WordPress Theme', 'consal'),
					'args'   => array(
						'teeny'            => true,
						'textarea_rows'    => 5,
						'media_buttons'	=> false,
					)
				),
				array(
					'id'        => 'map_lat',
					'type'      => 'text',
					'title'     => esc_html__('Latitude', 'consal'),
					'default'   => '23.8028085'
				),
				array(
					'id'        => 'map_long',
					'type'      => 'text',
					'title'     => esc_html__('Longtitude', 'consal'),
					'default'   => esc_html__('90.4070036', 'consal'),
				),
				array(
					'id'        => 'map_zoom',
					'type'      => 'slider',
					'title'     => esc_html__('Zoom level', 'consal'),
					"default"   => 17,
					"min"       => 0,
					"step"      => 1,
					"max"       => 21,
					'display_value' => 'text'
				),
				array(
					'id'        => 'map_color',
					'type'      => 'color',
					'title'     => esc_html__('Map Color', 'consal'),
					'default'   => esc_html__('#77be2d', 'consal'),
                    'transparent' => false,
				),
				array(
					'id'        => 'map_scrollwheel',
					'type'      => 'switch',
					'title'     => esc_html__('Scroll Wheel', 'consal'),
					'desc'      => esc_html__('Switch scrollwheel On OFF', 'consal'),
					'default'=> 0,
				),				
				array(
					'id'        => 'map_marker',
					'type'      => 'media',
					'title'     => esc_html__('Marker', 'consal'),
					'compiler'  => 'true',
					'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
					'desc'      => esc_html__('Upload marker image here, the image size is 30x43 pixels.', 'consal'),
				),
			),
        );
		
		// Less Compiler
		$this->sections[] = array(
			'title'     => esc_html__('Less Compiler', 'consal'),
			'desc'      => esc_html__('Turn on this option to apply all theme options. Turn of when you have finished changing theme options and your site is ready.', 'consal'),
			'icon'      => 'el-icon-wrench',
			'fields'    => array(
				array(
					'id'        => 'enable_less',
					'type'      => 'switch',
					'title'     => esc_html__('Enable Less Compiler', 'consal'),
					'default'   => true,
				),
			),
		);


            $this->sections[] = array(
                'title'     => esc_html__('Import / Export', 'consal'),
                'desc'      => esc_html__('Import and Export your Redux Framework settings from file, text or URL.', 'consal'),
                'icon'      => 'el-icon-refresh',
                'fields'    => array(
                    array(
                        'id'            => 'opt-import-export',
                        'type'          => 'import_export',
                        'title'         => 'Import Export',
                        'subtitle'      => 'Save and restore your Redux options',
                        'full_width'    => false,
                    ),
                ),
            );

            $this->sections[] = array(
                'icon'      => 'el-icon-info-sign',
                'title'     => esc_html__('Theme Information', 'consal'),
				'icon'      => 'el-icon-website',
                'fields'    => array(
                    array(
                        'id'        => 'opt-raw-info',
                        'type'      => 'raw',
                        'content'   => $item_info,
                    )
                ),
            );
        }

        public function setHelpTabs() {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-1',
                'title'     => esc_html__('Theme Information 1', 'consal'),
                'content'   => esc_html__('<p>This is the tab content, HTML is allowed.</p>', 'consal')
            );

            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-2',
                'title'     => esc_html__('Theme Information 2', 'consal'),
                'content'   => esc_html__('<p>This is the tab content, HTML is allowed.</p>', 'consal')
            );

            // Set the help sidebar
            $this->args['help_sidebar'] = esc_html__('<p>This is the sidebar content, HTML is allowed.</p>', 'consal');
        }

        /**

          All the possible arguments for Redux.
          For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'          => 'consal_opt',            // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
                'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
                'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
                'menu_title'        => esc_html__('Theme Options', 'consal'),
                'page_title'        => esc_html__('Theme Options', 'consal'),
                
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' => '', // Must be defined to add google fonts to the typography module
                
                'async_typography'  => true,                    // Use a asynchronous font on the front end or font string
                //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
                'admin_bar'         => true,                    // Show the panel pages on the admin bar
                'global_variable'   => '',                      // Set a different name for your global variable other than the opt_name
                'dev_mode'          => false,                    // Show the time the page took to load, etc
                'customizer'        => true,                    // Enable basic customizer support
                //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
                //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

                // OPTIONAL -> Give you extra features
                'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
                'menu_icon'         => '',                      // Specify a custom URL to an icon
                'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
                'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
                'page_slug'         => '_options',              // Page slug used to denote the panel
                'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
                'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
                'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.
                
                // CAREFUL -> These options are for advanced use only
                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
                
                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'system_info'           => false, // REMOVE

                // HINTS
                'hints' => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'         => 'light',
                        'shadow'        => true,
                        'rounded'       => false,
                        'style'         => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show'          => array(
                            'effect'        => 'slide',
                            'duration'      => '500',
                            'event'         => 'mouseover',
                        ),
                        'hide'      => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
                        ),
                    ),
                )
            );


            // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
            $this->args['share_icons'][] = array(
                'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
                'title' => 'Visit us on GitHub',
                'icon'  => 'el-icon-github'
                //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
            );
            $this->args['share_icons'][] = array(
                'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
                'title' => 'Like us on Facebook',
                'icon'  => 'el-icon-facebook'
            );
            $this->args['share_icons'][] = array(
                'url'   => 'http://twitter.com/reduxframework',
                'title' => 'Follow us on Twitter',
                'icon'  => 'el-icon-twitter'
            );
            $this->args['share_icons'][] = array(
                'url'   => 'http://www.linkedin.com/company/redux-framework',
                'title' => 'Find us on LinkedIn',
                'icon'  => 'el-icon-linkedin'
            );

            // Panel Intro text -> before the form
            if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
                if (!empty($this->args['global_variable'])) {
                    $v = $this->args['global_variable'];
                } else {
                    $v = str_replace('-', '_', $this->args['opt_name']);
                }
            } else {}
        }
    }
    global $reduxConfig;
    $reduxConfig = new Consal_Theme_Config();
}

/**
  Custom function for the callback referenced above
 */
if (!function_exists('redux_my_custom_field')):
    function redux_my_custom_field($field, $value) {
        print_r($field);
        echo '<br/>';
        print_r($value);
    }
endif;

/**
  Custom function for the callback validation referenced above
 * */
if (!function_exists('redux_validate_callback_function')):
    function redux_validate_callback_function($field, $value, $existing_value) {
        $error = false;
        $value = 'just testing';
        $return['value'] = $value;
        if ($error == true) {
            $return['error'] = $field;
        }
        return $return;
    }
endif;
