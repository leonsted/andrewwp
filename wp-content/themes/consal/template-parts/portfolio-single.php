<?php
/**
 * Template part for displaying portfolio posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */
 

$item_cats = get_the_terms($post->ID, 'portfolio_category');
$item_classes = '';
if($item_cats):
foreach($item_cats as $item_cat) {
	$item_classes .= $item_cat->slug . ', ';
}
endif;
$client_name = get_post_meta($post->ID, 'consal_client_name', true);
?>
    <section class="section projetc_bla">
      <div class="container">
        <div class="row">
          <div class="col-md-7">
            <div class="sngle_project_image">
              <?php the_post_thumbnail(); ?>
            </div>
          </div>


            <div class="col-md-5">

              <div class="project_about">
                <div class="project_content_title">
                  <h3><?php the_title(); ?></h3>
                </div>
                <div class="project_text">
                  <?php the_content(); ?>
                </div>
              </div>

              <ul class="detail_list">
                <li><h4><?php esc_html_e('Project Details', 'consal'); ?></h4></li>
                <li><span><?php esc_html_e('Date', 'consal'); ?></span>
                  <?php echo esc_html(get_the_date()); ?>
                </li>
                <li><span><?php esc_html_e('Client', 'consal'); ?></span>
                  <?php echo esc_attr( $client_name ); ?>
                </li>
                <li><span><?php esc_html_e('Category', 'consal'); ?></span>
                  <?php echo esc_attr( $item_classes ); ?>
                </li>
				<?php if( function_exists('consal_helper_plugin') ) { ?>
                <li>
					<span><?php esc_html_e('Share', 'consal'); ?></span>
					<div class="social_icons project_share">
						<?php consal_blog_sharing(); ?>
					</div>
                </li>
				<?php } ?>
              </ul>

            </div>
        </div>

      </div>
    </section>


<?php
  global $consal_opt;
  $show_rel_project = $consal_opt['show_rel_project'];
  if ( $show_rel_project ) {
?>

        <div class="section rel-project-section clearfix">
            <div class="container">
            <h2 class="wpb_heading"><?php echo $consal_opt['rel_project_title']; ?></h2>
            <div class="row">
              <?php
              $postargs = array(
              'exclude'          => get_the_ID(),
              'post_type'      => 'portfolio',
              'posts_per_page'    => 4,
              );          
              $rel_services = get_posts( $postargs );
              foreach ( $rel_services as $post ) {
              ?>            
                <div class="col-md-3 rel-sing-ser">
                  <a href="<?php the_permalink();?>"><?php the_post_thumbnail('consal-homepost-thumb'); ?></a>
                  <a href="<?php the_permalink();?>"><h4><?php the_title(); ?></h4></a>
                </div>            
              <?php
              }
              wp_reset_postdata();
              ?>        
            </div>
          </div>
        </div>
  
<?php
  }
?>