<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'heading_align' => array(
		'type'    => 'select',
		'value'   => '',
		'label'   => esc_html__( 'Text alignment', 'innox' ),
		'desc'    => esc_html__( 'Select heading text alignment', 'innox' ),
		'choices' => array(
			''   => esc_html__( 'Left', 'innox' ),
			'text-center' => esc_html__( 'Center', 'innox' ),
			'text-right'  => esc_html__( 'Right', 'innox' ),
		),
	),
	'headings'      => array(
		'type'        => 'addable-box',
		'value'       => '',
		'label'       => esc_html__( 'Headings', 'innox' ),
		'desc'        => esc_html__( 'Choose a tag and text inside it', 'innox' ),
		'box-options' => array(

			'heading_variant' => array(
				'type'    => 'multi-picker',
				'label'   => false,
				'desc'    => false,
				'value'   => false,
				'picker'  => array(
					'heading_tag'            => array(
						'type'    => 'select',
						'value'   => 'h3',
						'label'   => esc_html__( 'Heading tag', 'innox' ),
						'desc'    => esc_html__( 'Select a tag for your heading', 'innox' ),
						'choices' => array(
							'h2' => esc_html__( 'H2 tag', 'innox' ),
							'h3' => esc_html__( 'H3 tag', 'innox' ),
							'h4' => esc_html__( 'H4 tag', 'innox' ),
							'h5' => esc_html__( 'H5 tag', 'innox' ),
							'p'  => esc_html__( 'P tag', 'innox' ),
							'span_bg'  => esc_html__( 'Section Background Heading', 'innox' ),
						),
					),
				),
				'choices' => array(
					'h2'        => array(),
					'h3'        => array(),
					'h4'        => array(),
					'h5'        => array(),
					'p'         => array(),
					'span_bg'   => array(
						'position' => array(
							'type'      => 'switch',
							'label'     => esc_html__( 'Heading Position', 'innox' ),
							'desc'      => esc_html__( 'Choose where to put heading, before section or inside section', 'innox' ),
							'value'     => 'inside',
							'left-choice'   => array(
								'value' => '',
								'label' => esc_html__( 'Inside', 'innox' )
							),
							'right-choice'   => array(
								'value' => 'before_section',
								'label' => esc_html__( 'Before', 'innox' )
							)
						),
					)
				),
			),

			'heading_text'           => array(
				'type'  => 'text',
				'value' => '',
				'label' => esc_html__( 'Heading text', 'innox' ),
				'desc'  => esc_html__( 'Text to appear in slide layer', 'innox' ),
			),
			'heading_text_color'     => array(
				'type'    => 'select',
				'value'   => '',
				'label'   => esc_html__( 'Heading text color', 'innox' ),
				'desc'    => esc_html__( 'Select a color for your text in layer', 'innox' ),
				'choices' => array(
					''           => esc_html__( 'Inherited', 'innox' ),
					'highlight'  => esc_html__( 'Accent Color', 'innox' ),
					'grey'       => esc_html__( 'Dark grey theme color', 'innox' ),
					'black'      => esc_html__( 'Dark theme color', 'innox' ),
				),
			),
			'heading_text_weight'    => array(
				'type'    => 'select',
				'value'   => '',
				'label'   => esc_html__( 'Heading text weight', 'innox' ),
				'desc'    => esc_html__( 'Select a weight for your text in layer', 'innox' ),
				'choices' => array(
					''     => esc_html__( 'Normal', 'innox' ),
					'bold' => esc_html__( 'Bold', 'innox' ),
					'thin' => esc_html__( 'Thin', 'innox' ),
				),
			),
			'heading_text_transform' => array(
				'type'    => 'select',
				'value'   => '',
				'label'   => esc_html__( 'Heading text transform', 'innox' ),
				'desc'    => esc_html__( 'Select a weight for your text in layer', 'innox' ),
				'choices' => array(
					''                => esc_html__( 'None', 'innox' ),
					'text-lowercase'  => esc_html__( 'Lowercase', 'innox' ),
					'text-uppercase'  => esc_html__( 'Uppercase', 'innox' ),
					'text-capitalize' => esc_html__( 'Capitalize', 'innox' ),

				),
			),
		),
		'template'    => '{{- heading_text }}',
	)
);
