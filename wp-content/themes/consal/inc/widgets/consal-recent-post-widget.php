<?php

class consal_recent_post_Widget extends WP_Widget {
	public function __construct() {
		$widget_ops = array('classname' => 'consal_recent', 'description' => esc_html__('Consal: Recent Posts Widget','consal') );
		$control_ops = array('id_base' => 'consal_recent-widget');
		parent::__construct('consal_recent-widget', esc_html__('Consal: Recent Posts','consal'), $widget_ops, $control_ops);
	}

	function widget($args, $instance){
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$number = $instance['number'];
		$post_image = $instance[ 'post_image' ] ? 'true' : 'false';
		$post_date = $instance[ 'post_date' ] ? 'true' : 'false';
		echo $before_widget;

		if($title) {
			echo $before_title.$title.$after_title;
		}
		
		?>

		<!-- start coding  -->

		<div class="footer_widgets recent_post">
			<ul>
				<?php
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => $number,
					'has_password' => false,
					'order' => 'DESC'
				);
				$post = new WP_Query($args);
					if($post->have_posts()):
				?>
				<?php while($post->have_posts()): $post->the_post(); ?>
				<?php
					$permalink = get_permalink();
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ),'consal-rcnt-post-wdgt' );
				?>
					<li>
						<?php $x = 0; if( 'on' == $instance[ 'post_image' ] ) : $x++; ?>
						
						<div class="recent_blog_img v_middle">
						  <a href="<?php echo esc_url($permalink); ?>">
							<?php
							if ( has_post_thumbnail() ) {
								the_post_thumbnail( 'consal-widget-latest-thumb', array( 'class' => 'img-responsive' ) );
							}
							?>
						  </a>
						</div>
						<?php endif; ?>
						
						<div class="single_recent_post v_middle <?php echo ($x == 0) ? 'noImage' : null ?>">
							<a href="<?php echo esc_url($permalink); ?>"><?php the_title(); ?></a>
							<?php if( 'on' == $instance[ 'post_date' ] ) : ?>
							<span><?php echo esc_html(get_the_date()); ?></span>
							<?php endif; ?>
						</div>
					</li>
				<?php  endwhile; endif; 
				wp_reset_postdata();
				?>			
			</ul>
		</div>
		<!-- start code here -->

		<?php
		echo $after_widget;
	}

	function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		$instance['number'] = $new_instance['number'];
		$instance['post_image'] = $new_instance['post_image'];
		$instance['post_date'] = $new_instance['post_date'];
		return $instance;
	}

	function form($instance)
	{
		$defaults = array('title' => 'Recent Posts', 'post_image' => '', 'post_date' => '', 'number' => 4);
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title','consal'); ?>:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('Number of Posts to show','consal'); ?>:</label>
			<input class="widefat" type="number" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" value="<?php echo esc_attr($instance['number']); ?>" />
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance[ 'post_image' ], 'on' ); ?> id="<?php echo esc_attr($this->get_field_id( 'post_image' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'post_image' )); ?>" /> 
			<label for="<?php echo esc_attr($this->get_field_id( 'post_image' )); ?>"><?php esc_html_e('Show Post/News Image', 'consal'); ?></label>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance[ 'post_date' ], 'off' ); ?> id="<?php echo esc_attr($this->get_field_id( 'post_date' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'post_date' )); ?>" /> 
			<label for="<?php echo esc_attr($this->get_field_id( 'post_date' )); ?>"><?php esc_html_e('Show Post Date', 'consal'); ?></label>
		</p>
	<?php
	}
}



add_action('widgets_init', 'consal_recent_post_load_widgets');
function consal_recent_post_load_widgets(){
	register_widget('consal_recent_post_Widget');
}