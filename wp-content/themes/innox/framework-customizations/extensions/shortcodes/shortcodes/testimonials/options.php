<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(

	'layout' => array(
		'type'    => 'select',
		'value'   => 'owlcarousel',
		'label'   => esc_html__( 'Testimonials Layout', 'innox' ),
		'desc'    => esc_html__( 'Select one of predefined testimonials layout', 'innox' ),
		'choices' => array(
			'owlcarousel'  => esc_html__( 'Owl Carousel', 'innox' ),
			'isotope' => esc_html__( 'Isotope Layout', 'innox' ),
		),
	),
	'responsive_lg' => array(
		'type'        => 'select',
		'value'       => '3',
		'label'       => __( 'Items count on <1200px', 'innox' ),
		'choices'     => array(
			'4' => '4',
			'3' => '3',
			'2' => '2',
			'1' => '1',
		),
		'no-validate' => false,
	),
	'responsive_md' => array(
		'type'        => 'select',
		'value'       => '2',
		'label'       => __( 'Items count on 992px-1200px', 'innox' ),
		'choices'     => array(
			'4' => '4',
			'3' => '3',
			'2' => '2',
			'1' => '1',
		),
		'no-validate' => false,
	),
	'responsive_sm' => array(
		'type'        => 'select',
		'value'       => '2',
		'label'       => __( 'Items count on 768px-992px', 'innox' ),
		'choices'     => array(
			'3' => '3',
			'2' => '2',
			'1' => '1',

		),
		'no-validate' => false,
	),
	'responsive_xs' => array(
		'type'        => 'select',
		'value'       => '1',
		'label'       => __( 'Items count on >768px', 'innox' ),
		'choices'     => array(
			'2' => '2',
			'1' => '1',
		),
		'no-validate' => false,
	),

	'testimonials'        => array(
		'label'         => esc_html__( 'Testimonials', 'innox' ),
		'popup-title'   => esc_html__( 'Add/Edit Testimonial', 'innox' ),
		'desc'          => esc_html__( 'Here you can add, remove and edit your Testimonials.', 'innox' ),
		'type'          => 'addable-popup',
		'template'      => '{{=review_name}}',
		'popup-options' => array(
			'review_content'       => array(
				'label' => esc_html__( 'Quote', 'innox' ),
				'desc'  => esc_html__( 'Enter the testimonial here', 'innox' ),
				'type'  => 'textarea',
			),
			'review_avatar' => array(
				'type'  => 'upload',
				'label' => esc_html__( 'Choose avatar image', 'innox' ),
			),
			'review_name'   => array(
				'label' => esc_html__( 'Review author name', 'innox' ),
				'desc'  => esc_html__( 'Enter the Name of the Person to quote', 'innox' ),
				'type'  => 'text'
			),
			'review_position'   => array(
				'label' => esc_html__( 'Review author position', 'innox' ),
				'desc'  => esc_html__( 'Enter the Position of the Person to quote', 'innox' ),
				'type'  => 'text'
			),
		)
	)
);