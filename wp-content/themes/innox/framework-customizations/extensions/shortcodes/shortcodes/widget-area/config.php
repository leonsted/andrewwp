<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$cfg['page_builder'] = array(
	'title'       => esc_html__( 'Widget Area', 'innox' ),
	'description' => esc_html__( 'Add a Widget Area', 'innox' ),
	'tab'         => esc_html__( 'Widgets', 'innox' ),
);
