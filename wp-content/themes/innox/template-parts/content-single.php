<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
} ?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'vertical-item content-padding big-padding with_background' ); ?>>

    <?php innox_post_thumbnail(); ?>

    <div class="item-content">
        <header class="entry-header">
	        <?php innox_the_post_meta(); ?>

	        <?php if ( get_the_title() ) : ?>
                <h1 class="entry-title"><?php the_title(); ?></h1>
	        <?php endif; ?>
        </header>

        <?php if ( !empty( get_the_content() ) ) : ?>
            <div class="entry-content">
                <?php
                the_content( esc_html__( 'Read More', 'innox' ) );
                ?>
            </div><!-- .entry-content -->
        <?php endif; //has content ?>

        <?php
        wp_link_pages( array(
            'before'      => '<div class="page-links highlightlinks"><span class="page-links-title">' . esc_html__( 'Pages:', 'innox' ) . '</span>',
            'after'       => '</div>',
            'link_before' => '<span>',
            'link_after'  => '</span>',
        ) );
        ?>

        <div class="entry-meta content-justify v-center v-spacing topmargin_30">
            <?php the_tags( '<div><div class="tag-links">', ' ', '</div></div>' ); ?>

	        <?php if ( function_exists( 'mwt_share_this' ) ) { ?>
                <div>
		        <?php innox_share_this( true, '', 'bg-icon main-bg-color-icon' ); ?>
                </div>
	        <?php } ?>
        </div>

    </div>

</article>

<?php
// Related posts
innox_related_posts( 'carousel' );
?>

