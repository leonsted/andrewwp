<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Count Down', 'innox' ),
		'description' => esc_html__( 'Show count down timer', 'innox' ),
		'tab'         => esc_html__( 'Content Elements', 'innox' ),
	)
);