<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

//find fw_ext
$shortcodes_extension = fw()->extensions->get( 'shortcodes' );
$button_options = array();
if ( ! empty( $shortcodes_extension ) ) {
	$button_options = $shortcodes_extension->get_shortcode( 'button' )->get_options();
}

$options = array(

	array(
		'type'  => 'group',
		'options'   => array(
			'slide_image_class' => array(
				'label' => esc_html__('Image Custom Class', 'innox'),
				'type' => 'text',
			),

			'slide_media_layers'     => array(
				'type'        => 'addable-box',
				'value'       => '',
				'label'       => esc_html__( 'Slide Media Layers', 'innox' ),
				'desc'        => esc_html__( 'Add slide media layers before of after main image', 'innox' ),
				'box-options' => array(
					'media_layer_position'     => array(
						'label' => esc_html__( 'Before / After Main Image', 'innox' ),
						'desc'  => esc_html__( 'Choose where to put media layer, before of after main slide image', 'innox' ),
						'type'  => 'switch',
						'left-choice' => array(
							'value' => 'before',
							'label' => esc_html__('Before', 'innox'),
						),
						'right-choice' => array(
							'value' => 'after',
							'label' => esc_html__('After', 'innox'),
						),
					),
					'media_layer_class' => array(
						'label' => esc_html__('Layer Class', 'innox'),
						'type' => 'text',
					),
					'media_layer_image' => array(
						'label' => esc_html__('Layer Image', 'innox'),
						'type' => 'upload',
					),
					'media_layer_animation'      => array(
						'type'    => 'select',
						'value'   => '',
						'label'   => esc_html__( 'Layer Animation', 'innox' ),
						'desc'    => esc_html__( 'Select one of predefined animations', 'innox' ),
						'choices' => array(
							''               => esc_html__( 'Default', 'innox' ),
							'slideDown'      => esc_html__( 'slideDown', 'innox' ),
							'scaleAppear'    => esc_html__( 'scaleAppear', 'innox' ),
							'fadeInLeft'     => esc_html__( 'fadeInLeft', 'innox' ),
							'fadeInUp'       => esc_html__( 'fadeInUp', 'innox' ),
							'fadeInRight'    => esc_html__( 'fadeInRight', 'innox' ),
							'fadeInDown'     => esc_html__( 'fadeInDown', 'innox' ),
							'fadeIn'         => esc_html__( 'fadeIn', 'innox' ),
							'slideRight'     => esc_html__( 'slideRight', 'innox' ),
							'slideUp'        => esc_html__( 'slideUp', 'innox' ),
							'slideLeft'      => esc_html__( 'slideLeft', 'innox' ),
							'expandUp'       => esc_html__( 'expandUp', 'innox' ),
							'slideExpandUp'  => esc_html__( 'slideExpandUp', 'innox' ),
							'expandOpen'     => esc_html__( 'expandOpen', 'innox' ),
							'bigEntrance'    => esc_html__( 'bigEntrance', 'innox' ),
							'hatch'          => esc_html__( 'hatch', 'innox' ),
							'tossing'        => esc_html__( 'tossing', 'innox' ),
							'pulse'          => esc_html__( 'pulse', 'innox' ),
							'floating'       => esc_html__( 'floating', 'innox' ),
							'bounce'         => esc_html__( 'bounce', 'innox' ),
							'pullUp'         => esc_html__( 'pullUp', 'innox' ),
							'pullDown'       => esc_html__( 'pullDown', 'innox' ),
							'stretchLeft'    => esc_html__( 'stretchLeft', 'innox' ),
							'stretchRight'   => esc_html__( 'stretchRight', 'innox' ),
							'fadeInUpBig'    => esc_html__( 'fadeInUpBig', 'innox' ),
							'fadeInDownBig'  => esc_html__( 'fadeInDownBig', 'innox' ),
							'fadeInLeftBig'  => esc_html__( 'fadeInLeftBig', 'innox' ),
							'fadeInRightBig' => esc_html__( 'fadeInRightBig', 'innox' ),
							'slideInDown'    => esc_html__( 'slideInDown', 'innox' ),
							'slideInLeft'    => esc_html__( 'slideInLeft', 'innox' ),
							'slideInRight'   => esc_html__( 'slideInRight', 'innox' ),
							'moveFromLeft'   => esc_html__( 'moveFromLeft', 'innox' ),
							'moveFromRight'  => esc_html__( 'moveFromRight', 'innox' ),
							'moveFromBottom' => esc_html__( 'moveFromBottom', 'innox' ),
						),
					),
				),
				'template'    => esc_html__( 'Slider Media Layer', 'innox' ),

				'limit'           => 5, // limit the number of boxes that can be added
				'add-button-text' => esc_html__( 'Add', 'innox' ),
			),
		)
	),

	array(
		'type'  => 'group',
		'options'   => array(
			'slide_masking' => array(
				'type'    => 'multi-picker',
				'label'   => false,
				'desc'    => false,
				'value'   => false,
				'picker'  => array(
					'half_side_mask' => array(
						'type'      => 'switch',
						'label'     => esc_html__( 'Half Slide Mask', 'innox' ),
						'desc'      => esc_html__( 'Add half width color overlay to slide', 'innox' ),
						'value'     => '',
						'left-choice'   => array(
							'value' => '',
							'label' => esc_html__( 'No', 'innox' )
						),
						'right-choice'   => array(
							'value' => 'half-mask',
							'label' => esc_html__( 'Yes', 'innox' )
						)
					),
				),
				'choices' => array(
					'half-mask'   => array(
						'mask_animation'      => array(
							'type'    => 'select',
							'value'   => '',
							'label'   => esc_html__( 'Layer Animation', 'innox' ),
							'desc'    => esc_html__( 'Select one of predefined animations', 'innox' ),
							'choices' => array(
								''               => esc_html__( 'Default', 'innox' ),
								'slideDown'      => esc_html__( 'slideDown', 'innox' ),
								'scaleAppear'    => esc_html__( 'scaleAppear', 'innox' ),
								'fadeInLeft'     => esc_html__( 'fadeInLeft', 'innox' ),
								'fadeInUp'       => esc_html__( 'fadeInUp', 'innox' ),
								'fadeInRight'    => esc_html__( 'fadeInRight', 'innox' ),
								'fadeInDown'     => esc_html__( 'fadeInDown', 'innox' ),
								'fadeIn'         => esc_html__( 'fadeIn', 'innox' ),
								'slideRight'     => esc_html__( 'slideRight', 'innox' ),
								'slideUp'        => esc_html__( 'slideUp', 'innox' ),
								'slideLeft'      => esc_html__( 'slideLeft', 'innox' ),
								'expandUp'       => esc_html__( 'expandUp', 'innox' ),
								'slideExpandUp'  => esc_html__( 'slideExpandUp', 'innox' ),
								'expandOpen'     => esc_html__( 'expandOpen', 'innox' ),
								'bigEntrance'    => esc_html__( 'bigEntrance', 'innox' ),
								'hatch'          => esc_html__( 'hatch', 'innox' ),
								'tossing'        => esc_html__( 'tossing', 'innox' ),
								'pulse'          => esc_html__( 'pulse', 'innox' ),
								'floating'       => esc_html__( 'floating', 'innox' ),
								'bounce'         => esc_html__( 'bounce', 'innox' ),
								'pullUp'         => esc_html__( 'pullUp', 'innox' ),
								'pullDown'       => esc_html__( 'pullDown', 'innox' ),
								'stretchLeft'    => esc_html__( 'stretchLeft', 'innox' ),
								'stretchRight'   => esc_html__( 'stretchRight', 'innox' ),
								'fadeInUpBig'    => esc_html__( 'fadeInUpBig', 'innox' ),
								'fadeInDownBig'  => esc_html__( 'fadeInDownBig', 'innox' ),
								'fadeInLeftBig'  => esc_html__( 'fadeInLeftBig', 'innox' ),
								'fadeInRightBig' => esc_html__( 'fadeInRightBig', 'innox' ),
								'slideInDown'    => esc_html__( 'slideInDown', 'innox' ),
								'slideInLeft'    => esc_html__( 'slideInLeft', 'innox' ),
								'slideInRight'   => esc_html__( 'slideInRight', 'innox' ),
								'moveFromLeft'   => esc_html__( 'moveFromLeft', 'innox' ),
								'moveFromRight'  => esc_html__( 'moveFromRight', 'innox' ),
								'moveFromBottom' => esc_html__( 'moveFromBottom', 'innox' ),
							),
						),
						'mask_text'     => array(
							'type'        => 'addable-box',
							'label'       => esc_html__( 'Mask Text', 'innox' ),
							'desc'        => esc_html__( 'Add text to mask (max 3 items)', 'innox' ),
							'template'    => esc_html__( 'Text Item', 'innox' ),
							'box-options' => array(
								'text_item'    => array(
									'type'  => 'text',
									'label' => esc_html__( 'Text Item', 'innox' )
								),
							),
							'limit'           => 3, // limit the number of boxes that can be added
							'add-button-text' => esc_html__( 'Add', 'innox' ),
						),
					),
					''   => array(),
				),
			),
		)
	),

	'slide_align'      => array(
		'type'        => 'select',
		'value'       => 'text-left',
		'label'       => esc_html__( 'Slide text alignment', 'innox' ),
		'desc'        => esc_html__( 'Select slide text alignment', 'innox' ),
		'choices'     => array(
			'text-left'   => esc_html__( 'Left', 'innox' ),
			'text-center' => esc_html__( 'Center', 'innox' ),
			'text-right'  => esc_html__( 'Right', 'innox' ),
		),
		/**
		 * Allow save not existing choices
		 * Useful when you use the select to populate it dynamically from js
		 */
		'no-validate' => false,
	),

	'slide_layers'     => array(
		'type'        => 'addable-box',
		'value'       => '',
		'label'       => esc_html__( 'Slide Layers', 'innox' ),
		'desc'        => esc_html__( 'Choose a tag and text inside it', 'innox' ),
		'box-options' => array(
			'layer_tag'            => array(
				'type'    => 'select',
				'value'   => 'h3',
				'label'   => esc_html__( 'Layer tag', 'innox' ),
				'desc'    => esc_html__( 'Select a tag for your ', 'innox' ),
				'choices' => array(
					'h3' => esc_html__( 'H3 tag', 'innox' ),
					'h2' => esc_html__( 'H2 tag', 'innox' ),
					'h4' => esc_html__( 'H4 tag', 'innox' ),
					'h5' => esc_html__( 'H5 tag', 'innox' ),
					'p'  => esc_html__( 'P tag', 'innox' ),
					'hr'  => esc_html__( 'Divider tag', 'innox' ),

				),
			),
			'layer_animation'      => array(
				'type'    => 'select',
				'value'   => 'fadeIn',
				'label'   => esc_html__( 'Animation type', 'innox' ),
				'desc'    => esc_html__( 'Select one of predefined animations', 'innox' ),
				'choices' => array(
					''               => esc_html__( 'Default', 'innox' ),
					'slideDown'      => esc_html__( 'slideDown', 'innox' ),
					'scaleAppear'    => esc_html__( 'scaleAppear', 'innox' ),
					'fadeInLeft'     => esc_html__( 'fadeInLeft', 'innox' ),
					'fadeInUp'       => esc_html__( 'fadeInUp', 'innox' ),
					'fadeInRight'    => esc_html__( 'fadeInRight', 'innox' ),
					'fadeInDown'     => esc_html__( 'fadeInDown', 'innox' ),
					'fadeIn'         => esc_html__( 'fadeIn', 'innox' ),
					'slideRight'     => esc_html__( 'slideRight', 'innox' ),
					'slideUp'        => esc_html__( 'slideUp', 'innox' ),
					'slideLeft'      => esc_html__( 'slideLeft', 'innox' ),
					'expandUp'       => esc_html__( 'expandUp', 'innox' ),
					'slideExpandUp'  => esc_html__( 'slideExpandUp', 'innox' ),
					'expandOpen'     => esc_html__( 'expandOpen', 'innox' ),
					'bigEntrance'    => esc_html__( 'bigEntrance', 'innox' ),
					'hatch'          => esc_html__( 'hatch', 'innox' ),
					'tossing'        => esc_html__( 'tossing', 'innox' ),
					'pulse'          => esc_html__( 'pulse', 'innox' ),
					'floating'       => esc_html__( 'floating', 'innox' ),
					'bounce'         => esc_html__( 'bounce', 'innox' ),
					'pullUp'         => esc_html__( 'pullUp', 'innox' ),
					'pullDown'       => esc_html__( 'pullDown', 'innox' ),
					'stretchLeft'    => esc_html__( 'stretchLeft', 'innox' ),
					'stretchRight'   => esc_html__( 'stretchRight', 'innox' ),
					'fadeInUpBig'    => esc_html__( 'fadeInUpBig', 'innox' ),
					'fadeInDownBig'  => esc_html__( 'fadeInDownBig', 'innox' ),
					'fadeInLeftBig'  => esc_html__( 'fadeInLeftBig', 'innox' ),
					'fadeInRightBig' => esc_html__( 'fadeInRightBig', 'innox' ),
					'slideInDown'    => esc_html__( 'slideInDown', 'innox' ),
					'slideInLeft'    => esc_html__( 'slideInLeft', 'innox' ),
					'slideInRight'   => esc_html__( 'slideInRight', 'innox' ),
					'moveFromLeft'   => esc_html__( 'moveFromLeft', 'innox' ),
					'moveFromRight'  => esc_html__( 'moveFromRight', 'innox' ),
					'moveFromBottom' => esc_html__( 'moveFromBottom', 'innox' ),
				),
			),
			'layer_text'           => array(
				'type'  => 'text',
				'value' => '',
				'label' => esc_html__( 'Layer text', 'innox' ),
				'desc'  => esc_html__( 'Text to appear in slide layer', 'innox' ),
			),
			'layer_text_color'     => array(
				'type'    => 'select',
				'value'   => '',
				'label'   => esc_html__( 'Layer text color', 'innox' ),
				'desc'    => esc_html__( 'Select a color for your text in layer', 'innox' ),
				'choices' => array(
					''           => esc_html__( 'Inherited', 'innox' ),
					'highlight'  => esc_html__( 'Accent Color 1', 'innox' ),
					'highlight2' => esc_html__( 'Accent Color 2', 'innox' ),
					'highlight3' => esc_html__( 'Accent Color 3', 'innox' ),
					'highlight4' => esc_html__( 'Accent Color 4', 'innox' ),
					'grey'       => esc_html__( 'Dark grey theme color', 'innox' ),
					'black'      => esc_html__( 'Dark theme color', 'innox' ),

				),
			),
			'layer_text_weight'    => array(
				'type'    => 'select',
				'value'   => '',
				'label'   => esc_html__( 'Layer text weight', 'innox' ),
				'desc'    => esc_html__( 'Select a weight for your text in layer', 'innox' ),
				'choices' => array(
					''     => esc_html__( 'Normal', 'innox' ),
					'bold' => esc_html__( 'Bold', 'innox' ),
					'thin' => esc_html__( 'Thin', 'innox' ),

				),
			),
			'layer_text_transform' => array(
				'type'    => 'select',
				'value'   => '',
				'label'   => esc_html__( 'Layer text transform', 'innox' ),
				'desc'    => esc_html__( 'Select a text transformation for your layer', 'innox' ),
				'choices' => array(
					''                => esc_html__( 'None', 'innox' ),
					'text-lowercase'  => esc_html__( 'Lowercase', 'innox' ),
					'text-uppercase'  => esc_html__( 'Uppercase', 'innox' ),
					'text-capitalize' => esc_html__( 'Capitalize', 'innox' ),

				),
			),
		),
		'template'    => esc_html__( 'Slider Layer', 'innox' ),

		'limit'           => 5, // limit the number of boxes that can be added
		'add-button-text' => esc_html__( 'Add', 'innox' ),
	),
	'slide_buttons'     => array(
		'type'        => 'addable-box',
		'value'       => '',
		'label'       => esc_html__( 'Slide Buttons', 'innox' ),
		'desc'        => esc_html__( 'Choose a button, link for it and text inside it', 'innox' ),
		'template'    => esc_html__( 'Button', 'innox' ),
		'box-options' => array(
			$button_options
		),
		'limit'           => 5, // limit the number of boxes that can be added
		'add-button-text' => esc_html__( 'Add', 'innox' ),
	),
	'slide_button_animation' => array(
		'type'    => 'select',
		'value'   => 'fadeIn',
		'label'   => esc_html__( 'Buttons animation type', 'innox' ),
		'desc'    => esc_html__( 'Select one of predefined animations', 'innox' ),
		'choices' => array(
			''               => esc_html__( 'Default', 'innox' ),
			'slideDown'      => esc_html__( 'slideDown', 'innox' ),
			'scaleAppear'    => esc_html__( 'scaleAppear', 'innox' ),
			'fadeInLeft'     => esc_html__( 'fadeInLeft', 'innox' ),
			'fadeInUp'       => esc_html__( 'fadeInUp', 'innox' ),
			'fadeInRight'    => esc_html__( 'fadeInRight', 'innox' ),
			'fadeInDown'     => esc_html__( 'fadeInDown', 'innox' ),
			'fadeIn'         => esc_html__( 'fadeIn', 'innox' ),
			'slideRight'     => esc_html__( 'slideRight', 'innox' ),
			'slideUp'        => esc_html__( 'slideUp', 'innox' ),
			'slideLeft'      => esc_html__( 'slideLeft', 'innox' ),
			'expandUp'       => esc_html__( 'expandUp', 'innox' ),
			'slideExpandUp'  => esc_html__( 'slideExpandUp', 'innox' ),
			'expandOpen'     => esc_html__( 'expandOpen', 'innox' ),
			'bigEntrance'    => esc_html__( 'bigEntrance', 'innox' ),
			'hatch'          => esc_html__( 'hatch', 'innox' ),
			'tossing'        => esc_html__( 'tossing', 'innox' ),
			'pulse'          => esc_html__( 'pulse', 'innox' ),
			'floating'       => esc_html__( 'floating', 'innox' ),
			'bounce'         => esc_html__( 'bounce', 'innox' ),
			'pullUp'         => esc_html__( 'pullUp', 'innox' ),
			'pullDown'       => esc_html__( 'pullDown', 'innox' ),
			'stretchLeft'    => esc_html__( 'stretchLeft', 'innox' ),
			'stretchRight'   => esc_html__( 'stretchRight', 'innox' ),
			'fadeInUpBig'    => esc_html__( 'fadeInUpBig', 'innox' ),
			'fadeInDownBig'  => esc_html__( 'fadeInDownBig', 'innox' ),
			'fadeInLeftBig'  => esc_html__( 'fadeInLeftBig', 'innox' ),
			'fadeInRightBig' => esc_html__( 'fadeInRightBig', 'innox' ),
			'slideInDown'    => esc_html__( 'slideInDown', 'innox' ),
			'slideInLeft'    => esc_html__( 'slideInLeft', 'innox' ),
			'slideInRight'   => esc_html__( 'slideInRight', 'innox' ),
			'moveFromLeft'   => esc_html__( 'moveFromLeft', 'innox' ),
			'moveFromRight'  => esc_html__( 'moveFromRight', 'innox' ),
			'moveFromBottom' => esc_html__( 'moveFromBottom', 'innox' ),
		),
	),
);