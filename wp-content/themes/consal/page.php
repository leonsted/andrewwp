<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */

global $consal_opt;

get_header();

if(isset($consal_opt)){
	$pagelayout = 'nosidebar';
} else {
	$pagelayout = 'sidebar';
}
if(isset($consal_opt['page_layout']) && $consal_opt['page_layout']!=''){
	$pagelayout = $consal_opt['page_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$pagelayout = $_GET['layout'];
}
$pagesidebar = 'right';
if(isset($consal_opt['sidebarsepage_pos']) && $consal_opt['sidebarsepage_pos']!=''){
	$pagesidebar = $consal_opt['sidebarsepage_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$pagesidebar = $_GET['sidebar'];
}
switch($pagelayout) {
	case 'sidebar':
		$pageclass = 'page-sidebar';
		$pagecolclass = 9;
		break;
	case 'fullwidth':
		$pageclass = 'page-fullwidth';
		$pagecolclass = 12;
		$pagesidebar = 'none';
		break;
	default:
		$pageclass = 'page-nosidebar';
		$pagecolclass = 12;
		$pagesidebar = 'none';
} consal_pages_breadcrumb(); ?> 
 
<div class="page-content default-page section_padding">
	<div id="primary" class="content-area">
		<div class="container">
			<div class="row">
				<?php if($pagesidebar=='left') : ?>
					<?php get_sidebar('page'); ?>
				<?php endif; ?>	
				<div class="blog-grid-area col-xs-12 <?php echo 'col-md-'.$pagecolclass; ?>">
					<div id="main" class="site-main">
						<?php while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/content', 'page' );
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						endwhile; // End of the loop.
						?>			
					</div>	
				</div>
				<?php if( $pagesidebar=='right') : ?>
					<?php get_sidebar('page'); ?>
				<?php endif; ?>
			</div>
		</div>
	</div><!-- #main -->
</div><!-- #primary -->	
<?php
get_footer();
