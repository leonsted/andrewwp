<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * Framework options
 *
 * @var array $options Fill this array with options to generate framework settings form in WordPress customizer
 */

//find fw_ext
$shortcodes_extension = fw()->extensions->get( 'shortcodes' );
$header_social_icons  = array();
if ( ! empty( $shortcodes_extension ) ) {
	$header_social_icons = $shortcodes_extension->get_shortcode( 'icons_social' )->get_options();
}

$slider_extension = fw()->extensions->get( 'slider' );
$choices = '';
if ( ! empty ( $slider_extension ) ) {
	$choices = $slider_extension->get_populated_sliders_choices();
}

//adding empty value to disable slider
$choices['0'] = esc_html__( 'No Slider', 'innox' );

//header options

$header_teasers_common = array(
	'topline_combined_teasers' => array(
		'type'  => 'addable-box',
		'label' => esc_html__('Top Line Teasers', 'innox'),
		'desc'  => esc_html__('Chose icon and enter teaser text', 'innox'),
		'template'    => '{{=teaser_text}}',
		'box-options' => array(
			'teaser_icon' => array(
				'type' => 'icon-v2',
				'label' => esc_html__( 'Icon', 'innox' ),
			),
			'teaser_text' => array(
				'type' => 'text',
				'label' => esc_html__( 'Teaser text' , 'innox' ),
			),
			'teaser_text_link' => array(
				'type' => 'text',
				'label' => esc_html__( 'Teaser link' , 'innox' ),
			),
		),
	),
);

$header_button = array(
	'header_button_label' => array(
		'type'  => 'text',
		'value' => esc_html__( 'Call back', 'innox' ),
		'label' => esc_html__( 'Header button label', 'innox' ),
	),
	'header_button_link' => array(
		'type'  => 'text',
		'value' => esc_url( '#' ),
		'label' => esc_html__( 'Header button link', 'innox' ),
	)
);

$options = array(
	'logo_section'    => array(
		'title'   => esc_html__( 'Site Logo', 'innox' ),
		'options' => array(
			'logo_image'             => array(
				'type'        => 'upload',
				'value'       => array(),
				'attr'        => array( 'class' => 'logo_image-class', 'data-logo_image' => 'logo_image' ),
				'label'       => esc_html__( 'Logo Image', 'innox' ),
				'desc'        => esc_html__( 'Logo Image that appears in sections with light background', 'innox' ),
				'help'        => esc_html__( 'Choose image to display as a site logo', 'innox' ),
				'images_only' => true,
				'files_ext'   => array( 'png', 'jpg', 'jpeg', 'gif' ),
			),
			'logo_image_dark'             => array(
				'type'        => 'upload',
				'value'       => array(),
				'attr'        => array( 'class' => 'logo_image-class', 'data-logo_image' => 'logo_image' ),
				'label'       => esc_html__( 'Logo Image Dark', 'innox' ),
				'desc'        => esc_html__( 'Logo Image that appears in sections with dark background', 'innox' ),
				'help'        => esc_html__( 'Choose image to display as a site logo', 'innox' ),
				'images_only' => true,
				'files_ext'   => array( 'png', 'jpg', 'jpeg', 'gif' ),
			),
			'logo_text'              => array(
				'type'  => 'text',
				'value' => 'Innox',
				'attr'  => array( 'class' => 'logo_text-class', 'data-logo_text' => 'logo_text' ),
				'label' => esc_html__( 'Logo Text', 'innox' ),
				'desc'  => esc_html__( 'Text that appears near logo image', 'innox' ),
				'help'  => esc_html__( 'Type your text to show it in logo', 'innox' ),
			),
			'footer_logo_image'             => array(
				'type'        => 'upload',
				'value'       => array(),
				'attr'        => array( 'class' => 'logo_image-class', 'data-logo_image' => 'logo_image' ),
				'label'       => esc_html__( 'Logo image that appears in footer', 'innox' ),
				'desc'        => esc_html__( 'Select your logo', 'innox' ),
				'help'        => esc_html__( 'Choose image to display as a site logo in footer section. If not set main logo image used', 'innox' ),
				'images_only' => true,
				'files_ext'   => array( 'png', 'jpg', 'jpeg', 'gif' ),
			),
		),
	),
	'layout'          => array(
		'title'   => esc_html__( 'Theme Layout', 'innox' ),
		'options' => array(
			'layout' => array(
				'type'    => 'multi-picker',
				'value'   => 'wide',
				'attr'    => array( 'class' => 'theme-layout-class', 'data-theme-layout' => 'layout' ),
				'label'   => esc_html__( 'Theme layout', 'innox' ),
				'desc'    => esc_html__( 'Wide or Boxed layout', 'innox' ),
				'picker'  => array(
					'boxed' => array(
						'type'         => 'switch',
						'value'        => '',
						'label'        => false,
						'desc'         => false,
						'left-choice'  => array(
							'value' => '',
							'label' => esc_html__( 'Wide', 'innox' ),
						),
						'right-choice' => array(
							'value' => 'boxed_options',
							'label' => esc_html__( 'Boxed', 'innox' ),
						),
					),
				),
				'choices' => array(
					'boxed_options' => array(
						'body_background_image' => array(
							'type'        => 'upload',
							'value'       => '',
							'label'       => esc_html__( 'Body background image', 'innox' ),
							'help'        => esc_html__( 'Choose body background image if needed.', 'innox' ),
							'images_only' => true,
						),
						'body_cover'            => array(
							'type'         => 'switch',
							'value'        => '',
							'label'        => esc_html__( 'Parallax background', 'innox' ),
							'desc'         => esc_html__( 'Enable full width background for body', 'innox' ),
							'left-choice'  => array(
								'value' => '',
								'label' => esc_html__( 'No', 'innox' ),
							),
							'right-choice' => array(
								'value' => 'yes',
								'label' => esc_html__( 'Yes', 'innox' ),
							),
						),
					),
				),

			),
		),
	),
	'color_scheme_number'     => array(
		'title'   => esc_html__( 'Theme Color Scheme', 'innox' ),
		'options' => array(
			'color_scheme_number' => array(
				'type'    => 'image-picker',
				'value'   => '',
				'label'   => esc_html__( 'Color scheme', 'innox' ),
				'desc'    => esc_html__( 'Select one of predefined theme main colors', 'innox' ),
				'choices' => array(
					'' => get_template_directory_uri() . '/img/theme-options/color_scheme1.png',
					'2' => get_template_directory_uri() . '/img/theme-options/color_scheme2.png',
					'3' => get_template_directory_uri() . '/img/theme-options/color_scheme3.png',
				),
				'blank'   => false, // (optional) if true, image can be deselected
			),

		),
	),
	'blog_posts' => array(
		'title'   => esc_html__( 'Theme Blog', 'innox' ),
		'options' => array(
			'blog_slider_switch' => array(
				'type'    => 'multi-picker',
				'label'   => false,
				'desc'    => false,
				'picker'  => array(
					'blog_slider_enabled' => array(
						'type'         => 'switch',
						'value'        => '',
						'label'        => esc_html__( 'Post slider', 'innox' ),
						'desc'         => esc_html__( 'Enable slider on blog page', 'innox' ),
						'left-choice'  => array(
							'value' => '',
							'label' => esc_html__( 'No', 'innox' ),
						),
						'right-choice' => array(
							'value' => 'yes',
							'label' => esc_html__( 'Yes', 'innox' ),
						),
					),
				),
				'choices' => array(
					'yes' => array(
						'slider_id' => array(
							'type'    => 'select',
							'value'   => '',
							'label'   => esc_html__( 'Select Slider', 'innox' ),
							'choices' => $choices
						),
					),
				),
			),

			'blog_layout' => array(
				'type'        => 'select',
				'value'       => 'regular',
				'label'       => esc_html__( 'Blog Layout', 'innox' ),
				'desc'        => esc_html__( 'Select blog feed layout', 'innox' ),
				'choices'     => array(
					'regular'        => esc_html__( 'Regular', 'innox' ),
					'grid'           => esc_html__( 'Grid', 'innox' ),
				),
			),

			'posts_side_image' => array(
				'type'         => 'switch',
				'value'        => 'post-layout-standard',
				'label'        => esc_html__( 'Side Image Layout', 'innox' ),
				'desc'         => esc_html__( 'Enable blog post layout with side image. Works only with regular layout', 'innox' ),
				'left-choice'  => array(
					'value' => 'post-layout-standard',
					'label' => esc_html__( 'No', 'innox' ),
				),
				'right-choice' => array(
					'value' => 'post-layout-side',
					'label' => esc_html__( 'Yes', 'innox' ),
				),
			),
		)
	),

	'headers'     => array(
		'title'   => esc_html__( 'Theme Header', 'innox' ),
		'options' => array(
			'header' => array(
				'type'         => 'multi-picker',
				'label'        => false,
				'desc'         => false,
				'picker'       => array(
					'header_var' => array(
						'type'    => 'image-picker',
						'value'   => '1',
						'attr'    => array(
							'class'    => 'header-thumbnail',
							'data-foo' => 'header',
						),
						'label'   => esc_html__( 'Template Header', 'innox' ),
						'desc'    => esc_html__( 'Select one of predefined theme headers', 'innox' ),
						'help'    => esc_html__( 'You can select one of predefined theme headers', 'innox' ),
						'choices' => array(
							'1' => INNOX_THEME_URI . '/img/theme-options/header1.png',
							'2' => INNOX_THEME_URI . '/img/theme-options/header2.png',
							'3' => INNOX_THEME_URI . '/img/theme-options/header3.png',
							'4' => INNOX_THEME_URI . '/img/theme-options/header4.png',
							'5' => INNOX_THEME_URI . '/img/theme-options/header5.png',
							'6' => INNOX_THEME_URI . '/img/theme-options/header6.png',
							'7' => INNOX_THEME_URI . '/img/theme-options/header7.png',
							'21' => INNOX_THEME_URI . '/img/theme-options/header21.png',
							'22' => INNOX_THEME_URI . '/img/theme-options/header22.png',
							'23' => INNOX_THEME_URI . '/img/theme-options/header23.png',
							'24' => INNOX_THEME_URI . '/img/theme-options/header24.png',
						),
						'blank'   => false, // (optional) if true, image can be deselected
					),
				),
				'choices'      => array(
					'1'     => array(),
					'2'     => array_merge(
						array(
							'header_slider' => array(
								'type'    => 'multi-picker',
								'label'   => false,
								'desc'    => false,
								'value'   => false,
								'picker'  => array(
									'slider_before_header' => array(
										'type'      => 'switch',
										'label'     => esc_html__( 'Slider Before Header', 'innox' ),
										'desc'      => esc_html__( 'If slider exists on page put it before header', 'innox' ),
										'value'     => '',
										'left-choice'   => array(
											'value' => '',
											'label' => esc_html__( 'No', 'innox' )
										),
										'right-choice'   => array(
											'value' => 'before_header',
											'label' => esc_html__( 'Yes', 'innox' )
										)
									),
								),
								'choices' => array(
									'before_header'   => array(
										'toplogo_overlap' => array(
											'type'      => 'switch',
											'label'     => esc_html__( 'Top Logo Slider Overlap', 'innox' ),
											'desc'      => esc_html__( 'Make top logo section to overlap slider section', 'innox' ),
											'value'     => '',
											'left-choice'   => array(
												'value' => false,
												'label' => esc_html__( 'No', 'innox' )
											),
											'right-choice'   => array(
												'value' => true,
												'label' => esc_html__( 'Yes', 'innox' )
											)
										),
									),
									''   => array(),
								),
							),
						),
						$header_teasers_common
					),
					'3'     => array(),
					'4'     => $header_teasers_common,
					'5'     => array(),
					'6'     => $header_teasers_common,
					'7'     => $header_teasers_common,
					'21'     => $header_teasers_common,
					'22'     => $header_teasers_common,
					'23'     => $header_teasers_common,
					'24'     => $header_teasers_common,
				),
				'show_borders' => true,
			),

			$header_social_icons,
		),
	),
	'breadcrumbs'     => array(
		'title'   => esc_html__( 'Theme Title Section', 'innox' ),
		'options' => array(

			'breadcrumbs' => array(
				'type'    => 'image-picker',
				'value'   => '1',
				'attr'    => array(
					'class'    => 'breadcrumbs-thumbnail',
					'data-foo' => 'breadcrumbs',
				),
				'label'   => esc_html__( 'Page title sections with optional breadcrumbs', 'innox' ),
				'desc'    => esc_html__( 'Select one of predefined page title sections. Install Unyson Breadcrumbs extension to display breadcrumbs', 'innox' ),
				'help'    => esc_html__( 'You can select one of predefined theme title sections', 'innox' ),
				'choices' => array(
					'1' => INNOX_THEME_URI . '/img/theme-options/breadcrumbs1.png',
					'2' => INNOX_THEME_URI . '/img/theme-options/breadcrumbs2.png',
					'3' => INNOX_THEME_URI . '/img/theme-options/breadcrumbs3.png',
					'4' => INNOX_THEME_URI . '/img/theme-options/breadcrumbs4.png',
					'5' => INNOX_THEME_URI . '/img/theme-options/breadcrumbs5.png',
					'6' => INNOX_THEME_URI . '/img/theme-options/breadcrumbs6.png',
				),
				'blank'   => false, // (optional) if true, image can be deselected
			),

			'breadcrumbs_background_image' => array(
				'type'        => 'background-image',
				'label'       => esc_html__( 'Background Image', 'innox' ),
			),

		),
	),

	'footer'          => array(
		'title'   => esc_html__( 'Theme Footer', 'innox' ),
		'options' => array(

			'footer' => array(
				'type'    => 'image-picker',
				'value'   => '1',
				'attr'    => array(
					'class'    => 'footer-thumbnail',
					'data-foo' => 'footer',
				),
				'label'   => esc_html__( 'Page footer', 'innox' ),
				'desc'    => esc_html__( 'Select one of predefined page footers.', 'innox' ),
				'help'    => esc_html__( 'You can select one of predefined theme footers', 'innox' ),
				'choices' => array(
					'1' => INNOX_THEME_URI . '/img/theme-options/footer1.png',
					'2' => INNOX_THEME_URI . '/img/theme-options/footer2.png',
				),
				'blank'   => false, // (optional) if true, image can be deselected
			),
		),
	),

	'copyrights'      => array(
		'title'   => esc_html__( 'Theme Copyrights', 'innox' ),
		'options' => array(

			'copyrights'      => array(
				'type'    => 'image-picker',
				'value'   => '1',
				'attr'    => array(
					'class'    => 'copyrights-thumbnail',
					'data-foo' => 'copyrights',
				),
				'label'   => esc_html__( 'Page copyrights', 'innox' ),
				'desc'    => esc_html__( 'Select one of predefined page copyrights.', 'innox' ),
				'help'    => esc_html__( 'You can select one of predefined theme copyrights', 'innox' ),
				'choices' => array(
					'1' => INNOX_THEME_URI . '/img/theme-options/copyrights1.png',
					'2' => INNOX_THEME_URI . '/img/theme-options/copyrights2.png',
					'3' => INNOX_THEME_URI . '/img/theme-options/copyrights3.png',
				),
				'blank'   => false, // (optional) if true, image can be deselected
			),
			'copyrights_text' => array(
				'type'  => 'textarea',
				'value' => '&copy; Copyright 2018. All Rights Reserved',
				'label' => esc_html__( 'Copyrights text', 'innox' ),
				'desc'  => esc_html__( 'Please type your copyrights text', 'innox' ),
			),
			'copyright_social' => array(
				'type'            => 'addable-box',
				'value'           => '',
				'label'           => esc_html__( 'Social Buttons', 'innox' ),
				'desc'            => esc_html__( 'Optional social buttons', 'innox' ),
				'template'        => '{{=icon}}',
				'box-options'     => array(
					'icon'       => array(
						'type'  => 'icon',
						'label' => esc_html__( 'Social Icon', 'innox' ),
						'set'   => 'social-icons',
					),
					'icon_class' => array(
						'type'        => 'select',
						'value'       => '',
						'label'       => esc_html__( 'Icon type', 'innox' ),
						'desc'        => esc_html__( 'Select one of predefined social button types', 'innox' ),
						'choices'     => array(
							''                                    => esc_html__( 'Default', 'innox' ),
							'border-icon'                         => esc_html__( 'Simple Bordered Icon', 'innox' ),
							'border-icon rounded-icon'            => esc_html__( 'Rounded Bordered Icon', 'innox' ),
							'bg-icon'                             => esc_html__( 'Simple Background Icon', 'innox' ),
							'bg-icon rounded-icon'                => esc_html__( 'Rounded Background Icon', 'innox' ),
							'color-icon bg-icon'                  => esc_html__( 'Color Light Background Icon', 'innox' ),
							'color-icon bg-icon rounded-icon'     => esc_html__( 'Color Light Background Rounded Icon', 'innox' ),
							'color-icon'                          => esc_html__( 'Color Icon', 'innox' ),
							'color-icon border-icon'              => esc_html__( 'Color Bordered Icon', 'innox' ),
							'color-icon border-icon rounded-icon' => esc_html__( 'Rounded Color Bordered Icon', 'innox' ),
							'color-bg-icon'                       => esc_html__( 'Color Background Icon', 'innox' ),
							'color-bg-icon rounded-icon'          => esc_html__( 'Rounded Color Background Icon', 'innox' ),

						),
						/**
						 * Allow save not existing choices
						 * Useful when you use the select to populate it dynamically from js
						 */
						'no-validate' => false,
					),
					'icon_url'   => array(
						'type'  => 'text',
						'value' => '',
						'label' => esc_html__( 'Icon Link', 'innox' ),
						'desc'  => esc_html__( 'Provide a URL to your icon', 'innox' ),
					)
				),
				'limit'           => 0, // limit the number of boxes that can be added
				'add-button-text' => esc_html__( 'Add', 'innox' ),
				'sortable'        => true,
			),
		),
	),
	'not_found_page'    => array(
		'title'   => esc_html__( 'Theme 404 page', 'innox' ),
		'options' => array(
			'404_background_image'             => array(
				'type'        => 'upload',
				'value'       => array(),
				'attr'        => array( 'class' => 'logo_image-class', 'data-logo_image' => 'logo_image' ),
				'label'       => esc_html__( 'Background Image', 'innox' ),
				'images_only' => true,
				'files_ext'   => array( 'png', 'jpg', 'jpeg', 'gif' ),
			),
		),
	),
	'fonts_panel'     => array(
		'title'   => esc_html__( 'Theme Fonts', 'innox' ),
		'options' => array(
			'body_fonts_section' => array(
				'title'   => esc_html__( 'Font for body', 'innox' ),
				'options' => array(
					'body_font_picker_switch' => array(
						'type'    => 'multi-picker',
						'label'   => false,
						'desc'    => false,
						'picker'  => array(
							'main_font_enabled' => array(
								'type'         => 'switch',
								'value'        => '',
								'label'        => esc_html__( 'Enable', 'innox' ),
								'desc'         => esc_html__( 'Enable custom body font', 'innox' ),
								'left-choice'  => array(
									'value' => '',
									'label' => esc_html__( 'Disabled', 'innox' ),
								),
								'right-choice' => array(
									'value' => 'main_font_options',
									'label' => esc_html__( 'Enabled', 'innox' ),
								),
							),
						),
						'choices' => array(
							'main_font_options' => array(
								'main_font' => array(
									'type'       => 'typography-v2',
									'value'      => array(
										'family'         => 'Roboto',
										// For standard fonts, instead of subset and variation you should set 'style' and 'weight'.
										// 'style' => 'italic',
										// 'weight' => 700,
										'subset'         => 'latin-ext',
										'variation'      => 'regular',
										'size'           => 14,
										'line-height'    => 24,
										'letter-spacing' => 0,
										'color'          => '#0000ff'
									),
									'components' => array(
										'family'         => true,
										// 'style', 'weight', 'subset', 'variation' will appear and disappear along with 'family'
										'size'           => true,
										'line-height'    => true,
										'letter-spacing' => true,
										'color'          => false
									),
									'attr'       => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
									'label'      => esc_html__( 'Custom font', 'innox' ),
									'desc'       => esc_html__( 'Select custom font for headings', 'innox' ),
									'help'       => esc_html__( 'You should enable using custom heading fonts above at first', 'innox' ),
								),
							),
						),
					),
				),
			),

			'headings_fonts_section' => array(
				'title'   => esc_html__( 'Font for headings', 'innox' ),
				'options' => array(
					'h_font_picker_switch' => array(
						'type'    => 'multi-picker',
						'label'   => false,
						'desc'    => false,
						'picker'  => array(
							'h_font_enabled' => array(
								'type'         => 'switch',
								'value'        => '',
								'label'        => esc_html__( 'Enable', 'innox' ),
								'desc'         => esc_html__( 'Enable custom heading font', 'innox' ),
								'left-choice'  => array(
									'value' => '',
									'label' => esc_html__( 'Disabled', 'innox' ),
								),
								'right-choice' => array(
									'value' => 'h_font_options',
									'label' => esc_html__( 'Enabled', 'innox' ),
								),
							),
						),
						'choices' => array(
							'h_font_options' => array(
								'h_font' => array(
									'type'       => 'typography-v2',
									'value'      => array(
										'family'         => 'Roboto',
										// For standard fonts, instead of subset and variation you should set 'style' and 'weight'.
										// 'style' => 'italic',
										// 'weight' => 700,
										'subset'         => 'latin-ext',
										'variation'      => 'regular',
										'size'           => 28,
										'line-height'    => '100%',
										'letter-spacing' => 0,
										'color'          => '#0000ff'
									),
									'components' => array(
										'family'         => true,
										// 'style', 'weight', 'subset', 'variation' will appear and disappear along with 'family'
										'size'           => false,
										'line-height'    => false,
										'letter-spacing' => true,
										'color'          => false
									),
									'attr'       => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
									'label'      => esc_html__( 'Custom font', 'innox' ),
									'desc'       => esc_html__( 'Select custom font for headings', 'innox' ),
									'help'       => esc_html__( 'You should enable using custom heading fonts above at first', 'innox' ),
								),
							),
						),
					),
				),
			),

		),
	),
	'preloader_panel' => array(
		'title' => esc_html__( 'Theme Preloader', 'innox' ),

		'options' => array(
			'preloader_enabled' => array(
				'type'         => 'switch',
				'value'        => '1',
				'label'        => esc_html__( 'Enable Preloader', 'innox' ),
				'left-choice'  => array(
					'value' => '1',
					'label' => esc_html__( 'Enabled', 'innox' ),
				),
				'right-choice' => array(
					'value' => '0',
					'label' => esc_html__( 'Disabled', 'innox' ),
				),
			),

			'preloader_image' => array(
				'type'        => 'upload',
				'value'       => '',
				'label'       => esc_html__( 'Custom preloader image', 'innox' ),
				'help'        => esc_html__( 'GIF image recommended. Recommended maximum preloader width 150px, maximum preloader height 150px.', 'innox' ),
				'images_only' => true,
			),


		),
	),
	'share_buttons'   => array(
		'title' => esc_html__( 'Theme Share Buttons', 'innox' ),

		'options' => array(
			'share_facebook'    => array(
				'type'         => 'switch',
				'value'        => '1',
				'label'        => esc_html__( 'Enable Facebook Share Button', 'innox' ),
				'left-choice'  => array(
					'value' => '1',
					'label' => esc_html__( 'Enabled', 'innox' ),
				),
				'right-choice' => array(
					'value' => '0',
					'label' => esc_html__( 'Disabled', 'innox' ),
				),
			),
			'share_twitter'     => array(
				'type'         => 'switch',
				'value'        => '1',
				'label'        => esc_html__( 'Enable Twitter Share Button', 'innox' ),
				'left-choice'  => array(
					'value' => '1',
					'label' => esc_html__( 'Enabled', 'innox' ),
				),
				'right-choice' => array(
					'value' => '0',
					'label' => esc_html__( 'Disabled', 'innox' ),
				),
			),
			'share_google_plus' => array(
				'type'         => 'switch',
				'value'        => '1',
				'label'        => esc_html__( 'Enable Google+ Share Button', 'innox' ),
				'left-choice'  => array(
					'value' => '1',
					'label' => esc_html__( 'Enabled', 'innox' ),
				),
				'right-choice' => array(
					'value' => '0',
					'label' => esc_html__( 'Disabled', 'innox' ),
				),
			),
			'share_pinterest'   => array(
				'type'         => 'switch',
				'value'        => '1',
				'label'        => esc_html__( 'Enable Pinterest Share Button', 'innox' ),
				'left-choice'  => array(
					'value' => '1',
					'label' => esc_html__( 'Enabled', 'innox' ),
				),
				'right-choice' => array(
					'value' => '0',
					'label' => esc_html__( 'Disabled', 'innox' ),
				),
			),
			'share_linkedin'    => array(
				'type'         => 'switch',
				'value'        => '1',
				'label'        => esc_html__( 'Enable LinkedIn Share Button', 'innox' ),
				'left-choice'  => array(
					'value' => '1',
					'label' => esc_html__( 'Enabled', 'innox' ),
				),
				'right-choice' => array(
					'value' => '0',
					'label' => esc_html__( 'Disabled', 'innox' ),
				),
			),
			'share_tumblr'      => array(
				'type'         => 'switch',
				'value'        => '1',
				'label'        => esc_html__( 'Enable Tumblr Share Button', 'innox' ),
				'left-choice'  => array(
					'value' => '1',
					'label' => esc_html__( 'Enabled', 'innox' ),
				),
				'right-choice' => array(
					'value' => '0',
					'label' => esc_html__( 'Disabled', 'innox' ),
				),
			),
			'share_reddit'      => array(
				'type'         => 'switch',
				'value'        => '1',
				'label'        => esc_html__( 'Enable Reddit Share Button', 'innox' ),
				'left-choice'  => array(
					'value' => '1',
					'label' => esc_html__( 'Enabled', 'innox' ),
				),
				'right-choice' => array(
					'value' => '0',
					'label' => esc_html__( 'Disabled', 'innox' ),
				),
			),

		),
	),

);