<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'icon'       => array(
		'type'  => 'icon-v2',
		'label' => esc_html__( 'Icon', 'innox' ),
	),
	'icon_style' => array(
		'type'    => 'select',
		'value'   => 'default_icon',
		'label'   => esc_html__( 'Icon Color', 'innox' ),
		'desc'    => esc_html__( 'Select one of predefined colors.', 'innox' ),
		'choices' => array(
			'default_icon' => esc_html__( 'Default Font Color', 'innox' ),
			'black'        => esc_html__( 'Dark Color', 'innox' ),
			'highlight'    => esc_html__( 'Accent Color', 'innox' ),
		),
	),
	'icon_size'       => array(
		'label'   => esc_html__( 'Icon Size', 'innox' ),
		'desc'    => esc_html__( 'Choose icon size', 'innox' ),
		'type'    => 'select',
		'choices' => array(
			''         => esc_html__( 'Regular font size', 'innox' ),
			'size_small' => esc_html__( 'Small Icon Size (28px)', 'innox' ),
			'size_normal'  => esc_html__( 'Normal Icon Size (42px)', 'innox' ),
			'size_big'  => esc_html__( 'Big Icon Size (64px)', 'innox' ),
		)
	),
	'content'       => array(
		'type'  => 'text',
		'label' => esc_html__('Icon text', 'innox'),
	),
	'link'       => array(
		'type'  => 'text',
		'label' => esc_html__('Icon text link', 'innox'),
	)
);