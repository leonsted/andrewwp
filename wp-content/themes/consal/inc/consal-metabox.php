<?php

/**
 * Load Font Awesome Icon Picker Plugin
 */
require_once get_template_directory() . '/inc/cmb2-fontawesome-picker/cmb2-fontawesome-picker.php';




add_action( 'cmb2_admin_init', 'consal_metabox' );

function consal_metabox() {

	$prefix = 'consal_';
	
    // metabox support for service
	$cmb2_service = new_cmb2_box( array(
		'id'           => $prefix . 'our_service_area',
		'title'        => esc_html__( 'Service Settings', 'consal' ),
		'object_types' => array( 'service'), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
	) );

	$cmb2_service->add_field( array(
	    'name'    =>  esc_html__( 'Service Icon', 'consal' ),
	    'id'      =>  $prefix . 'es_s_icon',
	    'type'    =>  'fontawesome_icon',
	    'default' =>  esc_html__( 'fa fa-recycle', 'consal' ),
	) );
	
    // metabox support for portfolio
	$cmb2_portfolio = new_cmb2_box( array(
		'id'           => $prefix . 'portfolio_area',
		'title'        => esc_html__( 'Portfolio Settings', 'consal' ),
		'object_types' => array( 'portfolio'), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
	) );

	$cmb2_portfolio->add_field( array(
	    'name'    =>  esc_html__( 'Client Name', 'consal' ),
	    'id'      =>  $prefix . 'client_name',
	    'type'    =>  'text',
	) );
	
    // metabox support for Team Members
	$cmb2_advisors = new_cmb2_box( array(
		'id'           => $prefix . 'meat_our_advisors',
		'title'        => esc_html__( 'Member Settings', 'consal' ),
		'object_types' => array( 'team_members'), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
	) );

	$cmb2_advisors->add_field( array(
	    'name'    =>  esc_html__( 'Name', 'consal' ),
	    'id'      =>  $prefix . 'member_name',
	    'type'    => 'text',
	    'default' => esc_html__( '', 'consal' ),
		'desc' 	  => esc_html__( 'Enter member name here', 'consal' )
	));	
	
	$cmb2_advisors->add_field( array(
	    'name'    => esc_html__( 'Position', 'consal' ),
	    'id'      =>  $prefix . 'member_position',
	    'type'    => 'text',
	    'default' => esc_html__( '', 'consal' ),
		'desc' => esc_html__( 'Enter member position here', 'consal' )
	));		
	
	$cmb2_advisors->add_field( array(
	    'name'    => esc_html__( 'Facebook Link', 'consal' ),
	    'id'      =>  $prefix.'fb_link',
	    'type'    => 'text_url',
		'desc' => esc_html__( 'Use http, https before link', 'consal' )
	));		
	
	$cmb2_advisors->add_field( array(
	    'name'    => esc_html__( 'Twitter Link', 'consal' ),
	    'id'      =>  $prefix.'tw_link',
	    'type'    => 'text_url',
		'desc' => esc_html__( 'Use http, https before link', 'consal' )
	));		
	
	$cmb2_advisors->add_field( array(
	    'name'    => esc_html__( 'Pinterest Link', 'consal' ),
	    'id'      =>  $prefix.'pin_link',
	    'type'    => 'text_url',
		'desc' => esc_html__( 'Use http, https before link', 'consal' )
	));		
	
	$cmb2_advisors->add_field( array(
	    'name'    => esc_html__( 'Google Link', 'consal' ),
	    'id'      =>  $prefix.'google_link',
	    'type'    => 'text_url',
		'desc' => esc_html__( 'Use http, https before link', 'consal' )
	));	
	
	$cmb2_advisors->add_field( array(
	    'name'    => esc_html__( 'Linkedin Link', 'consal' ),
	    'id'      =>  $prefix.'ldin_link',
	    'type'    => 'text_url',
		'desc' => esc_html__( 'Use http, https before link', 'consal' )
	));
	
    // metabox support for testimonials
	$cmb2_consal_testimonial = new_cmb2_box( array(
		'id'           => $prefix . 'testimonials_area',
		'title'        => esc_html__( 'Testimonials Settings', 'consal' ),
		'object_types' => array( 'consal_testimonial'), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
	) );

	$cmb2_consal_testimonial->add_field( array(
	    'name'    =>  esc_html__( 'Designation', 'consal' ),
	    'id'      =>  $prefix . 'test_designation',
	    'type'    =>  'text',
	    'default' =>  esc_html__( '', 'consal' ),
	) );
}

