<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */
 
global $consal_opt;
get_header();
consal_pages_breadcrumb();
?>
    <!--================================
        3. START PROJECT DETAIL
    =================================-->
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/portfolio', 'single' );					
		endwhile; // End of the loop.
		?>
		</main><!-- #main -->
	</div><!-- #primary -->
    <!--================================
        3.END PROJECT DETAIL
    =================================-->
    <!--================================
        4.START LATEST PROJECTS
    =================================-->
	<?php if ( function_exists ('consal_latestproject_shortcode')) {
		echo do_shortcode('[latestproject]'); 
	} ?>
    <!--================================
        4.END LATEST PROJECTS
    =================================-->
    
<?php get_footer();
