<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */
global $consal_opt;
?>

	</div><!-- #content -->

    <!--================================
    12.START FOOTER
    =================================-->
    <footer class="footer">
		<?php consal_footer_sidebar(); ?>

        <div class="tiny_footer">
          <div class="container">
            <div class="row">
                <div class="col-md-12">
                        <div class="footer_text text-center">
							<p><?php consal_copyright(); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--================================
        12.END FOOTER
    =================================-->
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>
