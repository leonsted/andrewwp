<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'message' => array(
		'label' => esc_html__( 'Message', 'innox' ),
		'desc'  => esc_html__( 'Notification message', 'innox' ),
		'type'  => 'text',
		'value' => esc_html__( 'Message!', 'innox' ),
	),
	'type'    => array(
		'label'   => esc_html__( 'Type', 'innox' ),
		'desc'    => esc_html__( 'Notification type', 'innox' ),
		'type'    => 'select',
		'choices' => array(
			'success' => esc_html__( 'Congratulations', 'innox' ),
			'info'    => esc_html__( 'Information', 'innox' ),
			'warning' => esc_html__( 'Alert', 'innox' ),
			'danger'  => esc_html__( 'Error', 'innox' ),
		)
	),
	'icon'       => array(
		'type'  => 'icon',
		'label' => esc_html__( 'Icon', 'innox' ),
		'set'   => 'rt-icons-2',
	),
);