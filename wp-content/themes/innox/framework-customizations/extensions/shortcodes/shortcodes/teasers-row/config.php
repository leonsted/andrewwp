<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Teasers in Row', 'innox' ),
		'description' => esc_html__( 'Several teasers in row', 'innox' ),
		'tab'         => esc_html__( 'Content Elements', 'innox' ),
	)
);