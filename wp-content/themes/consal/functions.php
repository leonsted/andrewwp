<?php
/**
 * Consal - Agency & Corporate WordPress Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */

if ( ! function_exists( 'consal_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function consal_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Consal - Agency & Corporate WordPress Theme, use a find and replace
	 * to change 'consal' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'consal', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails', array( 'post', 'portfolio', 'team_members', 'consal_testimonial') );
	add_image_size( 'consal-category-thumb', 358, 265, true );
	add_image_size( 'consal-category-full', 1144, 510, true );
	add_image_size( 'consal-post-thumb', 848, 448, true );
	add_image_size( 'consal-homepost-thumb', 360, 230, true );
	add_image_size( 'consal-widget-latest-thumb', 80, 65, true );
	add_image_size( 'consal-portfolio-thumb', 375, 360, true );
	add_image_size( 'consal-team-members-thumb', 263, 285, true );
	add_image_size( 'consal-testimonial-thumb', 110, 110, true );
	add_image_size( 'consal-press-thumb', 360, 240, true );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary Menu', 'consal' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'consal_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'consal_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function consal_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'consal_content_width', 640 );
}
add_action( 'after_setup_theme', 'consal_content_width', 0 );

/**
 * Enqueue google fonts.
 */
function consal_fonts_url(){
	$fonts_url = '';
	 
	/* Translators: If there are characters in your language that are not
	* supported by Raleway, translate this to 'off'. Do not translate
	* into your own language.
	*/
	$raleway = _x( 'on', 'Raleway font: on or off', 'consal' );
	 
	/* Translators: If there are characters in your language that are not
	* supported by Open Sans, translate this to 'off'. Do not translate
	* into your own language.
	*/
	 
	if ( 'off' !== $raleway ) {
	$font_families = array();
	 
	if ( 'off' !== $raleway ) {
	$font_families[] = 'Raleway:400,500,700';
	}
	 
	$query_args = array(
	'family' => urlencode( implode( '|', $font_families ) ),
	'subset' => urlencode( 'latin,latin-ext' ),	
	);
	 
	$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}
	 
	return esc_url_raw( $fonts_url );
}

/**
 * Consal Partner Slider
 */
function consal_partner_script(){
	global $consal_opt;
	ob_start(); ?>
	var consal_partnernumber = <?php if(isset($consal_opt['partnernumber'])) { echo esc_js($consal_opt['partnernumber']); } else { echo '6'; } ?>,
	consal_partnerscroll = <?php echo esc_js($consal_opt['partnerscroll'])==1 ? 'true': 'false'; ?>;
	<?php
	return ob_get_clean();
}



/**
 * Sample implementation of the Custom Header feature
 *
 * You can add an optional custom header image to header.php like so ...
 *
	<?php the_header_image_tag(); ?>
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses consal_header_style()
 */
function consal_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'consal_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => '000000',
		'width'                  => 1000,
		'height'                 => 250,
		'flex-height'            => true,
		'wp-head-callback'       => 'consal_header_style',
	) ) );
}
add_action( 'after_setup_theme', 'consal_custom_header_setup' );

if ( ! function_exists( 'consal_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog.
 *
 * @see consal_custom_header_setup().
 */
function consal_header_style() {
	$header_text_color = get_header_textcolor();

	/*
	 * If no custom options for text are set, let's bail.
	 * get_header_textcolor() options: Any hex value, 'blank' to hide text. Default: add_theme_support( 'custom-header' ).
	 */
	if ( get_theme_support( 'custom-header', 'default-text-color' ) === $header_text_color ) {
		return;
	}
	
}
endif;
// If we get this far, we have custom styles. Let's do this.
function consal_header_style_script(){
	
	ob_start();
	// Has the text been hidden?
	if ( ! display_header_text() ) :
	?>
	.site-title,
	.site-description {
		position: absolute;
		clip: rect(1px, 1px, 1px, 1px);
	}
	<?php
		// If the user has set a custom color for the text use that.
		else :
	?>
	.site-title a,
	.site-description {
		color: #<?php
		$header_text_color = get_header_textcolor();
		echo esc_attr( $header_text_color ); ?>;
	}
	<?php endif; ?>
	<?php return ob_get_clean();
}

/**
 * Enqueue scripts and styles.
 */
function consal_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'consal-fonts', consal_fonts_url(), array(), null );
	// Owl Carousel CSS
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css' );
	
	// font-awesome.min
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/inc/cmb2-fontawesome-picker/assets/css/font-awesome.min.css' );
	
	// Bootstrap CSS
	wp_enqueue_style( 'bootstrap-min', get_template_directory_uri() . '/css/bootstrap.min.css' );

	// Venobox CSS
	wp_enqueue_style( 'venobox', get_template_directory_uri() . '/css/venobox.css' );
	
	wp_enqueue_style( 'consal-style', get_stylesheet_uri() );
	
	// Responsive CSS 
	wp_enqueue_style( 'consal-responsive', get_template_directory_uri() . '/css/responsive.css' );	
	
	// Bootstrap Script
	wp_enqueue_script( 'bootstrap-min', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.7', true );
	
	// Easing Script
	wp_enqueue_script( 'easing1', get_template_directory_uri() . '/js/jquery.easing.1.3.js', array(), '1.3', true );
	
	// Owl Carousel Script
	wp_enqueue_script( 'owl-carousel-min', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '20151215', true );
	// Venobox
	wp_enqueue_script( 'venobox-min', get_template_directory_uri() . '/js/venobox.min.js', array(), '1.7.1', true );
	
	// Isotope
	wp_enqueue_script( 'isotope', get_template_directory_uri() . '/js/isotope.js', array(), '3.0.3', true );
	
	// Packery
	wp_enqueue_script( 'packery', get_template_directory_uri() . '/js/packery.js', array(), '2.0.0', true );
	
	// Consal Main Jquery
	wp_enqueue_script( 'consal-main', get_template_directory_uri() . '/js/main.js', array(), '1.0.0', true );

	wp_enqueue_script( 'consal-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_add_inline_script('consal-main', consal_partner_script(),'before');
	wp_add_inline_style( 'consal-style', consal_header_style_script(),'wp_head' );
	
}
add_action( 'wp_enqueue_scripts', 'consal_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Register Sidebar
 */
require_once get_template_directory() . '/inc/widgets/register-widget.php';

/**
 * Load global function file.
 */
require_once get_template_directory() . '/inc/global-function.php';
 
/**
 * Load TGM plugin file.
 */
require get_template_directory() . '/inc/tgm-plugin/class-tgm-plugin-activation.php';
require get_template_directory() . '/inc/tgm-plugin/required-plugins.php';

/**
 * Load Redux Framework file.
 */
if ( file_exists( get_template_directory().'/inc/consal-config.php' ) ) {
  require_once( get_template_directory().'/inc/consal-config.php' );
}
/**
 * Custom Metabox 2
 */
 if ( file_exists( get_template_directory().'/inc/consal-metabox.php' ) ) {
  require_once( get_template_directory().'/inc/consal-metabox.php' );
}

// Replaces the excerpt "more" text by a link
function consal_new_excerpt_more($more) {
	return '';
}
add_filter('excerpt_more', 'consal_new_excerpt_more');

//Change excerpt length
function consal_change_excerpt_length( $length ) {
	global $consal_opt;
	
	if(isset($consal_opt['excerpt_length'])){
		return $consal_opt['excerpt_length'];
	}
	return 22;
}
add_filter( 'excerpt_length', 'consal_change_excerpt_length', 999 );

remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);



/**
 *  comment list modify 
 */
function consal_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
     
	<li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
		<div class="media" id="comment-8">
			<div class="pull-left"> 
				<?php echo get_avatar( $comment, 90 ); ?>
			</div>
			<div class="media-body"> 
				<header class="media_top"> 
					<div class="heading_left pull-left">
						<a href="<?php comment_author_url(); ?>"><h4 class="media-heading"><?php comment_author(); ?></h4></a>
						<span><?php echo esc_html(get_the_date()); ?></span> <span><?php echo esc_html(get_comment_time()); ?></span>
					</div>
					<?php if ($comment->comment_approved == '0') : ?>
						<p><em><?php esc_html_e('Your comment is awaiting moderation.','consal'); ?></em></p>
					<?php endif; ?>
						<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?>						
				</header>
				<section class="comment-content comment">
					<?php comment_text() ?>
				</section>
			</div> 
		</div>	
		
<?php }

// comment box title change
add_filter( 'comment_form_defaults', 'consal_remove_comment_form_allowed_tags' );
function consal_remove_comment_form_allowed_tags( $defaults ) {

	$defaults['comment_notes_after'] = '';
	$defaults['comment_notes_before'] = '';
	return $defaults;

}

// comment form modify
function consal_modify_comment_form_fields($fields){
	$commenter = wp_get_current_commenter();
	$req	   = get_option( 'require_name_email' );

	$fields['author'] = '<div class="comment-input"><div class="row"><div class="col-md-4"><input type="text" name="author" id="author" value="'. esc_attr( $commenter['comment_author'] ) .'" placeholder="'. esc_attr__("Your Name *", "consal").'" size="22" tabindex="1"'. ( $req ? 'aria-required="true"' : '' ).' class="input-name" /></div>';

	$fields['email'] = '<div class="col-md-4"><input type="text" name="email" id="email" value="'. esc_attr( $commenter['comment_author_email'] ) .'" placeholder="'.esc_attr__("Your Email *", "consal").'" size="22" tabindex="2"'. ( $req ? 'aria-required="true"' : '' ).' class="input-email"  /></div>';
	
	$fields['url'] = '<div class="col-md-4"><input type="text" name="url" id="url" value="'. esc_attr( $commenter['comment_author_url'] ) .'" placeholder="'. esc_attr__("Website", "consal").'" size="22" tabindex="2"'. ( $req ? 'aria-required="false"' : '' ).' class="input-url"  /></div></div></div>';

	return $fields;
}
add_filter('comment_form_default_fields','consal_modify_comment_form_fields'); 

function consal_comment_reform ($arg) {

$arg['title_reply'] = esc_html__('Leave a Reply','consal');
$arg['comment_field'] = '<div class="row"><div class="col-md-12"><textarea id="comment" class="comment_field" name="comment" cols="77" rows="8" placeholder="'. esc_html__("Write your Comment", "consal").'" aria-required="true"></textarea></div></div>';


return $arg;

}
add_filter('comment_form_defaults','consal_comment_reform');

//Google Map Api Key
global $consal_opt;
if(isset($consal_opt['map_apy_key']) && !empty($consal_opt['map_apy_key'])){
	add_action('wp_enqueue_scripts', 'consal_googlemap_api_enqueue');
	add_action('wp_footer', 'consal_googlemap', 31);
}

// Breadcrumb navxt home title change
add_filter('bcn_breadcrumb_title', function($title, $type, $id) {
	if ($type[0] === 'home') {
		$title = get_the_title(get_option('page_on_front'));
	}
	return $title;
}, 42, 3);