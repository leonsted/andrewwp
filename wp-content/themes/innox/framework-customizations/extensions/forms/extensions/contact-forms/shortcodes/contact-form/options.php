<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

//find fw_ext
$shortcodes_extension = fw()->extensions->get( 'shortcodes' );
$button_options = array();
if ( ! empty( $shortcodes_extension ) ) {
	$button_options = $shortcodes_extension->get_shortcode( 'button' )->get_options();
}

$options = array(
	'main' => array(
		'type'    => 'box',
		'title'   => '',
		'options' => array(
			'id'       => array(
				'type' => 'unique',
			),
			'builder'  => array(
				'type'    => 'tab',
				'title'   => esc_html__( 'Form Fields', 'innox' ),
				'options' => array(
					'form' => array(
						'label'        => false,
						'type'         => 'form-builder',
						'value'        => array(
							'json' => apply_filters( 'fw:ext:forms:builder:load-item:form-header-title', true )
								? json_encode( array(
									array(
										'type'      => 'form-header-title',
										'shortcode' => 'form_header_title',
										'width'     => '',
										'options'   => array(
											'title'    => '',
											'subtitle' => '',
										)
									)
								) )
								: '[]'
						),
						'fixed_header' => true,
					),
				),
			),
			'settings' => array(
				'type'    => 'tab',
				'title'   => esc_html__( 'Settings', 'innox' ),
				'options' => array(
					'settings-options' => array(
						'title'   => esc_html__( 'Contact Form Options', 'innox' ),
						'type'    => 'tab',
						'options' => array(
							'background_color'    => array(
								'type'    => 'select',
								'value'   => 'ls',
								'label'   => esc_html__( 'Form Background color', 'innox' ),
								'desc'    => esc_html__( 'Select background color', 'innox' ),
								'help'    => esc_html__( 'Select one of predefined background colors', 'innox' ),
								'choices' => array(
									''                              => esc_html__( 'No background', 'innox' ),
									'with_padding muted_background' => esc_html__( 'Muted', 'innox' ),
									'with_padding with_border'      => esc_html__( 'With Border', 'innox' ),
									'with_padding ls'               => esc_html__( 'Light', 'innox' ),
									'with_padding ls ms'            => esc_html__( 'Light Grey', 'innox' ),
									'with_padding ds'               => esc_html__( 'Dark Grey', 'innox' ),
									'with_padding ds ms'            => esc_html__( 'Dark', 'innox' ),
									'with_padding cs'               => esc_html__( 'Main color', 'innox' ),
								),
							),
							'columns_padding'     => array(
								'type'    => 'select',
								'value'   => 'columns_padding_15',
								'label'   => esc_html__( 'Column paddings in form', 'innox' ),
								'desc'    => esc_html__( 'Choose columns horizontal paddings value', 'innox' ),
								'choices' => array(
									'columns_padding_15' => esc_html__( '15 px - default', 'innox' ),
									'columns_padding_0'  => esc_html__( '0', 'innox' ),
									'columns_padding_1'  => esc_html__( '1 px', 'innox' ),
									'columns_padding_2'  => esc_html__( '2 px', 'innox' ),
									'columns_padding_5'  => esc_html__( '5 px', 'innox' ),
									'columns_padding_10'  => esc_html__( '10 px', 'innox' ),
								),
							),
							'input_text_center'     => array(
								'type'  => 'switch',
								'value' => '',
								'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
								'label' => esc_html__('Input Text Center', 'innox'),
								'desc'  => esc_html__('Center Text in Form Fields', 'innox'),
								'left-choice' => array(
									'value' => '',
									'label' => esc_html__('No', 'innox'),
								),
								'right-choice' => array(
									'value' => 'input-text-center',
									'label' => esc_html__('Yes', 'innox'),
								),
							),
							'form_email_settings' => array(
								'type'    => 'group',
								'options' => array(
									'email_to' => array(
										'type'  => 'text',
										'label' => esc_html__( 'Email To', 'innox' ),
										'help'  => esc_html__( 'We recommend you to use an email that you verify often', 'innox' ),
										'desc'  => esc_html__( 'The form will be sent to this email address.', 'innox' ),
									),
								),
							),
							'form_text_settings'  => array(
								'type'    => 'group',
								'options' => array(
									'subject-group'       => array(
										'type'    => 'group',
										'options' => array(
											'subject_message' => array(
												'type'  => 'text',
												'label' => esc_html__( 'Subject Message', 'innox' ),
												'desc'  => esc_html__( 'This text will be used as subject message for the email', 'innox' ),
												'value' => esc_html__( 'Contact Form', 'innox' ),
											),
										)
									),
									'submit-button-group' => array(
										'type'    => 'group',
										'options' => array(
											'submit_button_text' => array(
												'type'  => 'text',
												'label' => esc_html__( 'Submit Button', 'innox' ),
												'desc'  => esc_html__( 'This text will appear in submit button', 'innox' ),
												'value' => esc_html__( 'Send Message', 'innox' ),
											),
											'submit_button_type' => array(
												'type'    => 'select',
												'value'   => 'theme_button color1',
												'label'   => esc_html__( 'Button Type', 'innox' ),
												'desc'    => esc_html__( 'Choose a type for your button', 'innox' ),
												'choices' => array(
													'simple_link'          => esc_html__( 'Just link', 'innox' ),
													'more-link'          => esc_html__( 'Read More link', 'innox' ),
													'theme_button'         => esc_html__( 'Default', 'innox' ),
													'theme_button inverse' => esc_html__( 'Inverse', 'innox' ),
													'theme_button color1'  => esc_html__( 'Accent Color 1', 'innox' )
												),
											),
											'reset_button_text'  => array(
												'type'  => 'text',
												'label' => esc_html__( 'Reset Button', 'innox' ),
												'desc'  => esc_html__( 'This text will appear in reset button. Leave blank if reset button not needed', 'innox' ),
												'value' => esc_html__( 'Clear', 'innox' ),
											),
											'custom_buttons'     => array(
												'type'        => 'addable-box',
												'value'       => '',
												'label'       => esc_html__( 'Custom button', 'innox' ),
												'desc'        => esc_html__( 'Add Custom Buttons', 'innox' ),
												'box-options' => array(
													$button_options
												),
												'template'    => esc_html__( 'Button', 'innox' ),
												'limit'           => 5, // limit the number of boxes that can be added
												'add-button-text' => esc_html__( 'Add', 'innox' ),
											),
										)
									),
									'success-group'       => array(
										'type'    => 'group',
										'options' => array(
											'success_message' => array(
												'type'  => 'text',
												'label' => esc_html__( 'Success Message', 'innox' ),
												'desc'  => esc_html__( 'This text will be displayed when the form will successfully send', 'innox' ),
												'value' => esc_html__( 'Message sent!', 'innox' ),
											),
										)
									),
									'failure_message'     => array(
										'type'  => 'text',
										'label' => esc_html__( 'Failure Message', 'innox' ),
										'desc'  => esc_html__( 'This text will be displayed when the form will fail to be sent', 'innox' ),
										'value' => esc_html__( 'Oops something went wrong.', 'innox' ),
									),
								),
							),
						)
					),
					'mailer-options'   => array(
						'title'   => esc_html__( 'Mailer Options', 'innox' ),
						'type'    => 'tab',
						'options' => array(
							'mailer' => array(
								'label' => false,
								'type'  => 'mailer'
							)
						)
					)
				),
			),
		),
	)
);