<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$regular_button_options = array(
	'button_icon' => array(
		'type'         => 'multi-picker',
		'label'        => false,
		'desc'         => false,
		'picker'       => array(
			'use_icon' => array(
				'type'  => 'switch',
				'value' => '',
				'label' => esc_html__('Add icon to button', 'innox'),
				'left-choice' => array(
					'value' => '',
					'label' => esc_html__('No', 'innox'),
				),
				'right-choice' => array(
					'value' => 'icon',
					'label' => esc_html__('Yes', 'innox'),
				),
			),

		),
		'choices'      => array(
			''       => array(),
			'icon'   => array(
				'icon_source'       => array(
					'type'  => 'icon-v2',
					'label' => esc_html__( 'Choose Icon', 'innox' ),
				),
				'icon_side' => array(
					'type'  => 'switch',
					'value' => '',
					'label' => esc_html__('Icon Side.', 'innox'),
					'desc' => esc_html__('Add icon before or after button Label', 'innox'),
					'left-choice' => array(
						'value' => 'left',
						'label' => esc_html__('Left', 'innox'),
					),
					'right-choice' => array(
						'value' => 'right',
						'label' => esc_html__('Right', 'innox'),
					),
				),
				'styled_icon' => array(
					'label'     => esc_html__( 'Style Icon', 'innox' ),
					'value'     => false,
					'type'      => 'switch',
				),
			),
		),
	),
	'min_width' => array(
		'type'  => 'switch',
		'label' => esc_html__( 'Min width button', 'innox' ),
		'desc'  => esc_html__( 'Switch to set min width for button. (170px for regular button and 230px for big button)', 'innox' ),
	),
	'small_button' => array(
		'type'  => 'switch',
		'label' => esc_html__( 'Small button', 'innox' )
	)
);

$complex_button_options = array(
	'icon_left'       => array(
		'type'  => 'icon-v2',
		'label' => esc_html__( 'Left Icon', 'innox' ),
	),
	'icon_right'       => array(
		'type'  => 'icon-v2',
		'label' => esc_html__( 'Right Icon', 'innox' ),
	),
);

$options = array(
	'label'       => array(
		'label' => esc_html__( 'Button Label', 'innox' ),
		'desc'  => esc_html__( 'This is the text that appears on your button', 'innox' ),
		'type'  => 'text',
		'value' => esc_html__( 'Submit', 'innox' ),
	),
	'link'        => array(
		'label' => esc_html__( 'Button Link', 'innox' ),
		'desc'  => esc_html__( 'Where should your button link to', 'innox' ),
		'type'  => 'text',
		'value' => '#'
	),
	'target'      => array(
		'type'         => 'switch',
		'label'        => esc_html__( 'Open Link in New Window', 'innox' ),
		'desc'         => esc_html__( 'Select here if you want to open the linked page in a new window', 'innox' ),
		'right-choice' => array(
			'value' => '_blank',
			'label' => esc_html__( 'Yes', 'innox' ),
		),
		'left-choice'  => array(
			'value' => '_self',
			'label' => esc_html__( 'No', 'innox' ),
		),
	),

	'two_lines_label' => array(
		'type'  => 'switch',
		'label' => esc_html__( 'Two Lines Label', 'innox' )
	),
	'button_types' => array(
		'type'         => 'multi-picker',
		'label'        => false,
		'desc'         => false,
		'picker'       => array(
			'button_type' => array(
				'type'    => 'select',
				'value'   => 'theme_button color1',
				'label'   => esc_html__( 'Button Type', 'innox' ),
				'desc'    => esc_html__( 'Choose a type for your button', 'innox' ),
				'choices' => array(
					'simple_link'          => esc_html__( 'Just link', 'innox' ),
					'more-link'          => esc_html__( 'Read More link', 'innox' ),
					'theme_button'         => esc_html__( 'Default', 'innox' ),
					'theme_button inverse' => esc_html__( 'Inverse', 'innox' ),
					'theme_button color1'  => esc_html__( 'Accent Color 1', 'innox' ),
				),
			),
		),
		'choices'      => array(
			'more-link'                             => array(),
			'simple_link'                           => $regular_button_options,
			'theme_button'                          => $regular_button_options,
			'theme_button inverse'                  => $regular_button_options,
			'theme_button color1'                   => $regular_button_options
		),
		'show_borders' => true,
	),
);