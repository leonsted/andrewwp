<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
//get social icons to add in item:
$icon = fw_ext( 'shortcodes' )->get_shortcode( 'icon' );
//get social icons to add in item:
$icons_social = fw_ext( 'shortcodes' )->get_shortcode( 'icons_social' );

$options = array(
	'title'      => array(
		'type'  => 'text',
		'label' => esc_html__( 'Title of the Box', 'innox' ),
	),
	'title_tag'  => array(
		'type'    => 'select',
		'value'   => 'h3',
		'label'   => esc_html__( 'Title Tag', 'innox' ),
		'choices' => array(
			'h2' => esc_html__( 'H2', 'innox' ),
			'h3' => esc_html__( 'H3', 'innox' ),
			'h4' => esc_html__( 'H4', 'innox' ),
		)
	),
	'content'    => array(
		'type'          => 'wp-editor',
		'label'         => esc_html__( 'Item text', 'innox' ),
		'desc'          => esc_html__( 'Enter desired item content', 'innox' ),
		'size'          => 'small', // small, large
		'editor_height' => 400,
	),
	'item_style' => array(
		'type'    => 'select',
		'label'   => esc_html__( 'Item Box Style', 'innox' ),
		'choices' => array(
			''                                => esc_html__( 'Default (no border or background)', 'innox' ),
			'content-padding with_border'     => esc_html__( 'Bordered', 'innox' ),
			'content-padding with_background' => esc_html__( 'Muted Background', 'innox' ),
			'content-padding with_border with_background' => esc_html__( 'Bordered Muted Background', 'innox' ),
			'content-padding ls ms'           => esc_html__( 'Grey background', 'innox' ),
			'content-padding ds'              => esc_html__( 'Darkgrey background', 'innox' ),
			'content-padding ds ms'           => esc_html__( 'Dark background', 'innox' ),
			'content-padding cs'              => esc_html__( 'Main color background', 'innox' ),
			'content-padding big-padding with_border'        => esc_html__( 'Bordered with Padding', 'innox' ),
			'content-padding big-padding with_background'    => esc_html__( 'Muted Background with Padding', 'innox' ),
			'content-padding big-padding with_border with_background'    => esc_html__( 'Bordered Muted Background with Padding', 'innox' ),
			'content-padding big-padding ls ms'              => esc_html__( 'Grey background with Padding', 'innox' ),
			'content-padding big-padding ds'                 => esc_html__( 'Darkgrey background with Padding', 'innox' ),
			'content-padding big-padding ds ms'              => esc_html__( 'Dark background with Padding', 'innox' ),
			'content-padding big-padding cs'                 => esc_html__( 'Main color background with Padding', 'innox' ),
		)
	),
	'link'       => array(
		'type'  => 'text',
		'value' => '',
		'label' => esc_html__( 'Item link', 'innox' ),
		'desc'  => esc_html__( 'Link on title and optional button', 'innox' ),
	),
	'item_image' => array(
		'type'        => 'upload',
		'value'       => '',
		'label'       => esc_html__( 'Item Image', 'innox' ),
		'image'       => esc_html__( 'Image for your item. Not all item layouts show image', 'innox' ),
		'help'        => esc_html__( 'Image for your item. Image can appear as an element, or background or even can be hidden depends from chosen item type', 'innox' ),
		'images_only' => true,
	),
	'text_align' => array(
		'type'    => 'select',
		'label'   => esc_html__( 'Text Alignment', 'innox' ),
		'choices' => array(
			'text-left'   => esc_html__( 'Left', 'innox' ),
			'text-center' => esc_html__( 'Center', 'innox' ),
			'text-right'  => esc_html__( 'Right', 'innox' ),
		)
	),
	'icons'      => array(
		'type'            => 'addable-box',
		'value'           => '',
		'label'           => esc_html__( 'Additional info', 'innox' ),
		'desc'            => esc_html__( 'Add icons with title and text', 'innox' ),
		'box-options'     => $icon->get_options(),
		'add-button-text' => esc_html__( 'Add New', 'innox' ),
		'template'        => '{{=title}}',
		'sortable'        => true,
	),
	$icons_social->get_options(),

);