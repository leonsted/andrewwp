<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */
?>

<?php if ( ! is_active_sidebar( 'sidebar-page' ) ) {
	return;
}
$pagesidebar = 'right';
if(isset($consal_opt['sidebarpage_pos']) && $consal_opt['sidebarpage_pos']!=''){
	$pagesidebar = $consal_opt['sidebarpage_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$pagesidebar = $_GET['sidebar'];
}
?>
<?php if ( is_active_sidebar( 'sidebar-page' ) ) : ?>
	<div id="secondary" class="col-xs-12 col-md-3">
		<div class="page-sidebar <?php echo esc_attr($pagesidebar);?>">
			<?php dynamic_sidebar( 'sidebar-page' ); ?>
		</div>
	</div><!-- #secondary -->
<?php endif; ?>