<form role="search" method="get" class="search-form blog_search" action="<?php echo esc_url( home_url('/') ); ?>">
	<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'consal' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'consal' ) ?>" />
	<input type="submit" class="consal-search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'consal' ) ?>" />
</form>