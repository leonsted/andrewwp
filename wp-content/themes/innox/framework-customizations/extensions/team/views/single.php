<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * The template for displaying single service
 *
 */

get_header();
$pID = get_the_ID();

//no columns on single service page
$column_classes = fw_ext_extension_get_columns_classes( true );

//getting taxonomy name
$ext_team_settings = fw()->extensions->get( 'team' )->get_settings();
$taxonomy_name = $ext_team_settings['taxonomy_name'];

$atts = fw_get_db_post_option(get_the_ID());

$shortcodes_extension = fw()->extensions->get( 'shortcodes' );

$unique_id = uniqid();
?>
    <div id="content" class="<?php echo esc_attr( $column_classes['main_column_class'] ); ?> topmargin_0 bottommargin_0">
		<?php
		// Start the Loop.
		while ( have_posts() ) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="row columns_padding_30 columns_margin_bottom_40">
                    <?php if ( has_post_thumbnail() || ! empty( $atts['skills'] ) ) : ?>
                        <div class="col-xs-12 col-sm-5 col-md-4">
	                        <?php the_post_thumbnail( 'solarify-square-width', array( 'class' => 'bottommargin_50' ) ); ?>

                            <?php if ( ! empty( $atts['skills'] ) ) :
                                foreach($atts['skills'] as $skill) :
                                    echo fw_ext( 'shortcodes' )->get_shortcode( 'progress_bar' )->render( $skill );
                                endforeach;
                            endif; //skills check ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-xs-12 col-sm-7 col-md-8">

                        <header class="entry-header">
                            <div class="content-justify bottommargin_15">
                                <div>
        	                        <?php the_title( '<h1 class="entry-title highlight">', '</h1>' ); ?>
                                    <?php if ( ! empty( $atts['member_position'] ) ) : ?>
                                        <p><?php echo wp_kses_post( $atts['member_position'] ); ?></p>
                                    <?php endif; //position ?>
                                </div>
                                <?php if ( !empty( $atts['social_icons'] ) ) : ?>
                                <div class="member-social">
                                    <?php
                                    //get icons-social shortcode to render icons in team member item
                                    $shortcodes_extension = fw()->extensions->get( 'shortcodes' );
                                    if ( ! empty( $shortcodes_extension ) ) {
                                        echo fw_ext( 'shortcodes' )->get_shortcode( 'icons_social' )->render( array( 'social_icons' => $atts['social_icons'] ) );
                                    }
                                    ?>
                                </div><!-- eof social icons -->
                                <?php endif; ?>
                            </div>

	                        <?php if ( ! empty( $atts['icons'] ) ) : ?>
                                <div class="inline-content entry-meta">
			                        <?php foreach ( $atts['icons'] as $icon ) : ?>
                                        <span class="grey darklinks">
                                            <?php innox_get_icon_v2( $icon['icon'], 'highlight' ) ?>
                                            <?php if ( $icon['link'] ) : ?>
                                                <a href="<?php echo esc_url( $icon['link'] ) ?>">
                                            <?php endif;
                                            echo wp_kses_post( $icon['content'] );
                                            if ( $icon['link'] ) : ?>
                                                </a>
                                            <?php endif; ?>
                                        </span>
			                        <?php endforeach; ?>
                                </div><!-- eof social icons -->
	                        <?php endif; //social icons ?>
                        </header>

                        <div class="entry-content">
                            <?php the_content(); ?>
                        </div>

	                    <?php if ( ! empty( json_decode($atts['form']['json'])[1] ) ) { ?>
                            <?php echo fw_ext( 'shortcodes' )->get_shortcode( 'contact_form' )->render( $atts ); ?>

				        <?php } //form check ?>

                    </div>
                </div>



            </article><!-- #post-## -->
		<?php endwhile; ?>
    </div><!--eof #content -->


<?php if ( $column_classes['sidebar_class'] ): ?>
    <!-- main aside sidebar -->
    <aside class="<?php echo esc_attr( $column_classes['sidebar_class'] ); ?>">
		<?php get_sidebar(); ?>
    </aside>
    <!-- eof main aside sidebar -->
	<?php
endif;
get_footer();