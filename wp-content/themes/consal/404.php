<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Consal_-_Agency_&_Corporate_WordPress_Theme
 */

get_header();
global $consal_opt;
consal_pages_breadcrumb();
 ?>
    <section class="page_not_found section_padding">
       <div class="container">
           <div class="row">
                <div class="col-md-12">
                    <div class="huge_text"><p><?php esc_html_e( '404', 'consal' ); ?></p></div>
                    <p class="alas_message">
					           <?php consal_not_found_title() ?>
				            </p>
                    <p class="directory_text">
          						<?php consal_not_found_content(); ?>
          					</p>
                    <!-- back to home btn -->
                    <a href="<?php echo esc_url( home_url('/') ); ?>" class="consal_btn btn_colored"><?php esc_html_e('Back to Home', 'consal'); ?></a>
                </div>
           </div>
       </div>
    </section>

<?php
get_footer();
